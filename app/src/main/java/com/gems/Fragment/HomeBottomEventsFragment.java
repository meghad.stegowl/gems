package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.HomeBottomUpcomingEventsAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Customview.RecyclerViewPositionHelper;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.UpcomingEvent;
import com.gems.Model.UpcomingEventData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class HomeBottomEventsFragment extends Fragment {

    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    RecyclerView.LayoutManager layoutManager;
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    private ArrayList<UpcomingEventData> newupcomingEventData = new ArrayList<>();
    RecyclerView rv_events;
    private HomeBottomUpcomingEventsAdapter homeBottomUpcomingEventsAdapter;
    private boolean isLoading = false;
    public int page = 1, totalPage;
    private AppCompatDialog mProgressDialog;
    private TextView tv_nodata_found,tv_upcoming_events;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;
    FragmentManager fragmentManager;

    public HomeBottomEventsFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_upcoming_events, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);

        init();
        listener();
        return rootView;
    }

    private void init() {
        rv_events = rootView.findViewById(R.id.rv_events);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);
        tv_upcoming_events = rootView.findViewById(R.id.tv_upcoming_events);
        tv_upcoming_events.setText("All Events");
        layoutManager = new LinearLayoutManager(context);
        rv_events.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_events);

        upcomingEventData = new ArrayList<>();
        homeBottomUpcomingEventsAdapter = new HomeBottomUpcomingEventsAdapter( context, upcomingEventData,fragmentManager,rl_fragments_header);
        rv_events.setAdapter(homeBottomUpcomingEventsAdapter);
        homeBottomUpcomingEventsAdapter.notifyDataSetChanged();
        getEvents(page);

    }

    private void listener() {
        rv_events.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) rv_events.getLayoutManager();
                    int total = layoutManager.getItemCount();
                    int currentLastItem = recyclerViewPositionHelper.findLastCompletelyVisibleItemPosition() + 1;
//                    if (currentLastItem == total) {
//                        if (page <= totalPage) {
//                            isLoading = true;
//                            page++;
//                            getEvents(page);
//                        }
//                    }
                    if (page <= totalPage - 10) {
                        isLoading = true;
                        page++;
                        getEvents(page);
                    }
                }
            }
        });
    }

    private void getEvents(int page) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UpcomingEvent> getcall = apiInterface.getEventData("EventsList",token, u_id,100,page);
            getcall.enqueue(new Callback<UpcomingEvent>() {
                @Override
                public void onResponse(Call<UpcomingEvent> call, final Response<UpcomingEvent> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        newupcomingEventData = new ArrayList<>();
                        newupcomingEventData = response.body().getData();
                        if (status == 200) {
                            tv_nodata_found.setVisibility(View.GONE);
                            rv_events.setVisibility(View.VISIBLE);
                            homeBottomUpcomingEventsAdapter.add(newupcomingEventData);
                            totalPage = response.body().getLastPage();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            tv_nodata_found.setVisibility(View.VISIBLE);
                            rv_events.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UpcomingEvent> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
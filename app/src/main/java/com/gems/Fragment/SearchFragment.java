//package com.gems.Fragment;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatDialog;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.gems.Adapter.SearchEventsAdapter;
//import com.gems.Adapter.SearchVenueAdapter;
//import com.gems.Customview.CustomHeader;
//import com.gems.Helper.CheckNetwork;
//import com.gems.Helper.Const;
//import com.gems.Helper.Prefs;
//import com.gems.Model.Search;
//import com.gems.Model.SearchVenue;
//import com.gems.Model.UpcomingEventData;
//import com.gems.R;
//import com.gems.Rest.ApiClient;
//import com.gems.Rest.ApiInterface;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
//import okhttp3.MediaType;
//import okhttp3.RequestBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class SearchFragment extends Fragment {
//
//    FragmentManager fragmentManager;
//    private AppCompatDialog mProgressDialog;
//    private Context context;
//    private RelativeLayout rl_fragments_header;
//    boolean isOuter;
//    private View rootView;
//    private RecyclerView rv_venues, rv_events;
//    private RecyclerView.LayoutManager layoutManager,layoutManager1;
//    SearchVenueAdapter searchVenueAdapter;
//    SearchEventsAdapter searchEventsAdapter;
//
//    private ArrayList<SearchVenue> searchVenues = new ArrayList<>();
//    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
//    private TextView tv_no_venues, tv_no_events;
//    private EditText et_search;
//    Button btn_result;
//
//
//    @SuppressLint("ValidFragment")
//    public SearchFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager) {
//        this.context = context;
//        this.rl_fragments_header = rl_fragments_header;
//        this.isOuter = isOuter;
//        this.fragmentManager = fragmentManager;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_search, container, false);
//        if (isOuter == true) {
//            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
//        } else {
//            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
//        }
//        rl_fragments_header.setVisibility(View.VISIBLE);
//        init();
//        return rootView;
//    }
//
//    private void init() {
//        tv_no_venues = rootView.findViewById(R.id.tv_no_venues);
//        tv_no_events = rootView.findViewById(R.id.tv_no_events);
//        rv_venues = rootView.findViewById(R.id.rv_venues);
//        rv_events = rootView.findViewById(R.id.rv_events);
//        et_search = rootView.findViewById(R.id.et_search);
//        btn_result = rootView.findViewById(R.id.btn_result);
//        layoutManager = new LinearLayoutManager(context);
//        layoutManager1 = new LinearLayoutManager(context);
//        Lisner();
//    }
//
//    private void Lisner() {
//        tv_no_venues.setVisibility(View.VISIBLE);
//        tv_no_events.setVisibility(View.VISIBLE);
//        btn_result.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                tv_no_venues.setVisibility(View.GONE);
//                tv_no_events.setVisibility(View.GONE);
//                if (et_search.getText().length() == 0) {
//                    et_search.requestFocus();
//                    et_search.setError("Please enter the text.");
//                } else {
//                    getVenueData();
//                }
//            }
//        });
//    }
//
//    private void getVenueData() {
//        if (CheckNetwork.isInternetAvailable(context)) {
//            mProgressDialog = new AppCompatDialog(context);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            mProgressDialog.setContentView(R.layout.progress_dialog);
//            mProgressDialog.show();
//
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("search", et_search.getText().toString());
//            } catch (
//                    JSONException e) {
//                e.printStackTrace();
//            }
//            String jsonString = jsonObject.toString();
//            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);
//            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
//            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
//
//            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//            Call<Search> getcall = apiInterface.getSearch(token, u_id, requestBody);
//            getcall.enqueue(new Callback<Search>() {
//                @Override
//                public void onResponse(Call<Search> call, final Response<Search> response) {
//                    if (response.isSuccessful()) {
//                        mProgressDialog.dismiss();
//                        int status = response.body().getStatus();
//                        String msg = response.body().getMessage();
//                        if (status == 200) {
//                            LinearLayoutManager layoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//                            rv_venues.setLayoutManager(layoutManager3);
//                            searchVenues = new ArrayList<>();
//                            searchVenues = response.body().getVenuesData();
//                            if(searchVenues.size()== 0){
//                                rv_venues.setVisibility(View.GONE);
//                                tv_no_venues.setVisibility(View.VISIBLE);
//                            }else {
//                                searchVenueAdapter = new SearchVenueAdapter(context, searchVenues, fragmentManager, rl_fragments_header);
//                                rv_venues.setAdapter(searchVenueAdapter);
//                            }
//
//                            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//                            rv_events.setLayoutManager(layoutManager);
//                            upcomingEventData = new ArrayList<>();
//                            upcomingEventData = response.body().getEventsData();
//                            if(upcomingEventData.size()== 0){
//                                rv_events.setVisibility(View.GONE);
//                                tv_no_events.setVisibility(View.VISIBLE);
//                            }else {
//                                searchEventsAdapter = new SearchEventsAdapter(context, upcomingEventData, fragmentManager, rl_fragments_header);
//                                rv_events.setAdapter(searchEventsAdapter);
//                            }
//                        } else if (status == 422) {
//                            tv_no_venues.setVisibility(View.VISIBLE);
//                            tv_no_events.setVisibility(View.VISIBLE);
//                            rv_venues.setVisibility(View.GONE);
//                            rv_events.setVisibility(View.GONE);
//                            mProgressDialog.dismiss();
//                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        } else if (status == 404) {
//                            tv_no_venues.setVisibility(View.VISIBLE);
//                            tv_no_events.setVisibility(View.VISIBLE);
//                            rv_venues.setVisibility(View.GONE);
//                            rv_events.setVisibility(View.GONE);
//                            mProgressDialog.dismiss();
//                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        } else if (status == 401) {
//                            mProgressDialog.dismiss();
//                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        } else {
//                            mProgressDialog.dismiss();
//                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        mProgressDialog.dismiss();
//                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<Search> call, Throwable t) {
//                    mProgressDialog.dismiss();
//                }
//            });
//        } else {
//            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
//        }
//    }
//}

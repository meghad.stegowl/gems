package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.LoginActivity;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.CommonResponse;
import com.gems.Model.EliteMembership;
import com.gems.Model.GetProfile;
import com.gems.Model.GetProfileData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class MoreFragment extends Fragment implements View.OnClickListener {
    FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RelativeLayout rel_my_profile, rel_payments, rel_tickets, rel_notif, rel_share_app, rel_social_media,
            rel_term_codi;
    private LinearLayout ll_bottom_view;
    private LinearLayout ll_logout;
    private ImageView iv_loc,iv_setting;
    AppCompatDialog mProgressDialog;

    private CircleImageView iv_user_image;
    private TextView tv_user_name, tv_location, tv_logout_login;
    ArrayList<GetProfileData> getProfileData = new ArrayList<>();
    private Button btn_elite_member;


    public MoreFragment(Context context, RelativeLayout rl_fragments_header, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_more, container, false);
        CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        rl_fragments_header.setVisibility(View.VISIBLE);
        init();
        listner();
        getProfileDetail();
        getEliteMembership();
        return rootView;
    }

    private void init() {
        iv_setting = rootView.findViewById(R.id.iv_setting);
        iv_loc = rootView.findViewById(R.id.iv_loc);
        rel_my_profile = rootView.findViewById(R.id.rel_my_profile);
        rel_payments = rootView.findViewById(R.id.rel_payments);
        rel_tickets = rootView.findViewById(R.id.rel_tickets);
        rel_notif = rootView.findViewById(R.id.rel_notif);
        rel_share_app = rootView.findViewById(R.id.rel_share_app);
        rel_social_media = rootView.findViewById(R.id.rel_social_media);
        rel_term_codi = rootView.findViewById(R.id.rel_term_codi);
        ll_bottom_view = rootView.findViewById(R.id.ll_bottom_view);

        ll_logout = rootView.findViewById(R.id.ll_logout);
        iv_user_image = rootView.findViewById(R.id.iv_user_image);
        tv_user_name = rootView.findViewById(R.id.tv_user_name);
        tv_location = rootView.findViewById(R.id.tv_location);
        tv_logout_login = rootView.findViewById(R.id.tv_logout_login);
        btn_elite_member = rootView.findViewById(R.id.btn_elite_member);

        String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        if (skip_status.equals("0")) {
            tv_user_name.setText("Gems");
            tv_logout_login.setText("Login");
            rel_my_profile.setVisibility(View.GONE);
            rel_payments.setVisibility(View.GONE);
            rel_tickets.setVisibility(View.GONE);
            rel_notif.setVisibility(View.GONE);
            tv_location.setVisibility(View.GONE);
            iv_loc.setVisibility(View.GONE);
            ll_bottom_view.setVisibility(View.GONE);
            Glide.with(context)
                    .load(R.drawable.trans_logo)
                    .placeholder(R.drawable.trans_logo)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(iv_user_image);
        } else {
            tv_logout_login.setText("Logout");
            iv_setting.setVisibility(View.GONE);
        }
    }

    private void listner() {
        rel_my_profile.setOnClickListener(this);
        rel_payments.setOnClickListener(this);
        rel_tickets.setOnClickListener(this);
        rel_notif.setOnClickListener(this);
        rel_share_app.setOnClickListener(this);
        rel_social_media.setOnClickListener(this);
        rel_term_codi.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.rel_my_profile:
                pushInnerFragment(new ProfileFragment(context, rl_fragments_header, false, fragmentManager), "Notifications", true);
                break;

            case R.id.rel_payments:
                pushInnerFragment(new PaymentHistoryFragment(context, rl_fragments_header, false,fragmentManager), "Payments", true);
                break;

            case R.id.rel_tickets:
                pushInnerFragment(new InnerTicketFragment(context, rl_fragments_header, false, fragmentManager), "Tickets", true);
                break;

            case R.id.rel_notif:
                pushInnerFragment(new NotificationFragment(context, rl_fragments_header, false), "Notifications", true);
                break;

            case R.id.rel_share_app:
                pushInnerFragment(new ShareFragment(context, rl_fragments_header, false), "Share Our App", true);
                break;

            case R.id.rel_social_media:
                pushInnerFragment(new SocialFragment(context, rl_fragments_header, false), "Social Media", true);
                break;

            case R.id.rel_term_codi:
                pushInnerFragment(new TermsFragment(context, rl_fragments_header, false), "Term and Conditions", true);
                break;

            case R.id.ll_logout:
                String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                if (skip_status.equals("0")) {
                    String state_name = Prefs.getPrefInstance().getValue(context, Const.SIGN_UP_CITY_NAME, "");
//                    Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_NAME);
//                    Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_ID);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, state_name);
                    Log.d("mytag","here is value when sign in ________"+state_name);
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    logout();
                }
                break;
        }
    }


    private void logout() {
        if (CheckNetwork.isInternetAvailable(context)) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setTitle(getResources().getString(R.string.app_name));
            builder1.setMessage("Are you sure to want Logout?");
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Prefs.getPrefInstance().setValue(context, Const.USER_LOGIN_STATUS, "0");
                            String user_login_status = Prefs.getPrefInstance().getValue(context, Const.USER_LOGIN_STATUS, "");
                            Log.d("mytag", "status is in more fragment--------------+" + user_login_status);
                            getLogout();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getLogout() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommonResponse> getcall = apiInterface.getLogout(token, u_id);
            getcall.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, final Response<CommonResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "2");
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                            getActivity().finish();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "2");
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getProfileDetail() {
        if (CheckNetwork.isInternetAvailable(context)) {
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<GetProfile> getcall = apiInterface.getProfile("ProfileDetails", token, u_id);
            getcall.enqueue(new Callback<GetProfile>() {
                @Override
                public void onResponse(Call<GetProfile> getcall, Response<GetProfile> response) {
                    if (response != null && response.isSuccessful()) {
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            getProfileData = response.body().getData();
                            int user_id = getProfileData.get(0).getUserId();
                            String image = getProfileData.get(0).getImage();
                            String username = getProfileData.get(0).getUsername();
                            tv_user_name.setText(username);
                            String fcm_id = getProfileData.get(0).getFcmId();
                            String state_name = getProfileData.get(0).getState();
                            String city_name = getProfileData.get(0).getCity();
                            tv_location.setText(state_name );
                            String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                            if (skip_status.equals("1")) {
                                if (!image.equals("")) {
                                    Glide.with(context)
                                            .load(image)
                                            .placeholder(R.drawable.trans_logo)
                                            .fitCenter()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .centerCrop()
                                            .into(iv_user_image);
                                }
                            } else {
                                tv_user_name.setText("Gems");
                                Glide.with(context)
                                        .load(R.drawable.trans_logo)
                                        .placeholder(R.drawable.trans_logo)
                                        .fitCenter()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .into(iv_user_image);
                            }

                            Prefs.getPrefInstance().setValue(context, Const.ACCESS_TOKEN, token);
                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, String.valueOf(user_id));
                        } else if (status == 422) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            Glide.with(context)
                                    .load(R.drawable.trans_logo)
                                    .placeholder(R.drawable.trans_logo)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()
                                    .into(iv_user_image);

                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetProfile> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getEliteMembership() {
        if (CheckNetwork.isInternetAvailable(context)) {
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<EliteMembership> getcall = apiInterface.getEliteMembership(token, u_id);
            getcall.enqueue(new Callback<EliteMembership>() {
                @Override
                public void onResponse(Call<EliteMembership> getcall, Response<EliteMembership> response) {
                    if (response != null && response.isSuccessful()) {
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        String url = response.body().getUrl();
                        if (status == 200) {
                            btn_elite_member.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Log.d("mytag","more clicked");
                                    try {
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                        intent.setData(Uri.parse(url));
                                        context.startActivity(intent);
                                    } catch (ActivityNotFoundException e) {
                                    }
                                }
                            });
                        } else if (status == 422) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EliteMembership> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

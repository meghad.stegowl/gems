package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.VenueAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Customview.RecyclerViewPositionHelper;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.Venue;
import com.gems.Model.VenueData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class VenueFragment extends Fragment {

    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    RecyclerView.LayoutManager layoutManager;
    private ArrayList<VenueData> venueData = new ArrayList<>();
    RecyclerView rv_venue;
    private VenueAdapter venueAdapter;
    ProgressDialog pd;
    private boolean isLoading = false;
    public int page = 1, totalPage;
    private AppCompatDialog mProgressDialog;
    private TextView tv_nodata_found;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;
    FragmentManager fragmentManager;

    public VenueFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_venue, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);

        init();
        listener();
        getVenueData();
        return rootView;
    }

    private void init() {
        rv_venue = rootView.findViewById(R.id.rv_venue);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);
        layoutManager = new LinearLayoutManager(context);
        rv_venue.setLayoutManager(new GridLayoutManager(context, 2));
    }

    private void listener() {
    }

    private void getVenueData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            String state_name = Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "");
            Call<Venue> getcall;
            if (state_name.equals("All")) {
                Log.d("mytag", "here in if part of filtering");
                getcall = apiInterface.getVenueData("VenuesListing",  token, u_id/*, 50, page*/);
            } else if (state_name.equals("")) {
                Log.d("mytag", "here in if part of filtering");
                getcall = apiInterface.getVenueWithState("VenuesListing",state_name, token, u_id/*, 50, page*/);
            } else {
                getcall = apiInterface.getVenueData("VenuesListing",  token, u_id/*, 50, page*/);
            }
            getcall.enqueue(new Callback<Venue>() {
                @Override
                public void onResponse(Call<Venue> call, final Response<Venue> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            tv_nodata_found.setVisibility(View.GONE);
                            rv_venue.setVisibility(View.VISIBLE);
                            venueData = new ArrayList<>();
                            venueData = response.body().getData();
                            venueAdapter = new VenueAdapter(context, venueData, fragmentManager, rl_fragments_header);
                            rv_venue.setAdapter(venueAdapter);
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            tv_nodata_found.setVisibility(View.VISIBLE);
                            rv_venue.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Venue> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

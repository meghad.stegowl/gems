package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.ShareSocial;
import com.gems.Model.SocialmediaData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocialFragment extends Fragment {
    boolean isOuter;
    ProgressDialog pd;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private AppCompatDialog mProgressDialog;
    private ArrayList<SocialmediaData> socialmediaData = new ArrayList<SocialmediaData>();

    private RelativeLayout rl_insta,rl_fb,rl_twitter,rl_youTube;

    @SuppressLint("ValidFragment")
    public SocialFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_social, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        rl_fragments_header.setVisibility(View.VISIBLE);        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("Social Media");
        getSocialMedia();
        init();
        listener();
        return rootView;
    }


    private void init() {
        rl_insta = rootView.findViewById(R.id.rl_insta);
        rl_fb = rootView.findViewById(R.id.rl_fb);
        rl_twitter = rootView.findViewById(R.id.rl_twitter);
        rl_youTube = rootView.findViewById(R.id.rl_youTube);
    }

    private void listener() {
        rl_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new InstagramFragment(context, rl_fragments_header, false), "Instagram", true);
            }
        });

        rl_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FacebookFragment(context, rl_fragments_header, false), "Facebook", true);
            }
        });

        rl_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new TwitterFragment(context, rl_fragments_header, false), "Twitter", true);
            }
        });

        rl_youTube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new YoutubeFragment(context, rl_fragments_header, false), "Youtube", true);
            }
        });
    }

    private void getSocialMedia() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ShareSocial> getcall = apiInterface.getContactShare(token, u_id);
            getcall.enqueue(new Callback<ShareSocial>() {
                @Override
                public void onResponse(Call<ShareSocial> call, final Response<ShareSocial> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            socialmediaData = response.body().getSocialmediaData();
                            String facebook_Link = socialmediaData.get(0).getFacebookLink();
                            Prefs.getPrefInstance().setValue(context, Const.FB, facebook_Link);
                            String twitter_Link = socialmediaData.get(0).getTwitterLink();
                            Prefs.getPrefInstance().setValue(context, Const.TWIT, twitter_Link);
                            String instagram_Link = socialmediaData.get(0).getInstagramLink();
                            Prefs.getPrefInstance().setValue(context, Const.INSTA, instagram_Link);
                            String youtube_Link = socialmediaData.get(0).getYoutubeLink();
                            Prefs.getPrefInstance().setValue(context, Const.YOUTUBE, youtube_Link);
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ShareSocial> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

}
package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.AdapterJustForYou;
import com.gems.Customview.CustomHeader;
import com.gems.Model.UpcomingEventData;
import com.gems.R;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class JustForYouFragment extends Fragment {

    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView rv_venue;
    private AdapterJustForYou adapterJustForYou;
    FragmentManager fragmentManager;
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    private TextView tv_nodata_found;

    public JustForYouFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager
            , ArrayList<UpcomingEventData> upcomingEventData) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
        this.upcomingEventData = upcomingEventData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_just_for_you, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        init();
        return rootView;
    }

    private void init() {
        rv_venue = rootView.findViewById(R.id.rv_venue);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);

        layoutManager = new LinearLayoutManager(context);
        rv_venue.setLayoutManager(new GridLayoutManager(context, 2));

        if(upcomingEventData.size()== 0){
            tv_nodata_found.setVisibility(View.VISIBLE);
            rv_venue.setVisibility(View.GONE);
        }else {
            adapterJustForYou = new AdapterJustForYou(context, upcomingEventData, fragmentManager, rl_fragments_header);
            rv_venue.setAdapter(adapterJustForYou);
        }
    }
}
package com.gems.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.gems.Activity.LoginActivity;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.CommonResponse;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment {
    private View rootView;
    private Context context;
    private FragmentManager fm;
    private AppCompatDialog mProgressDialog;
    private RelativeLayout rl_fragments_header;
    boolean isOuter;
    FragmentManager fragmentManager;
    private String current_pwd, new_pwd, confirm_pwd;
    private EditText edt_current_pwd, edt_new_pwd, edt_confirm_pwd;
    Button btn_submit;
    private ImageView iv_current_pwd,iv_new_pwd,iv_confirm_pwd;

    public ChangePasswordFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_change_passwrd, container, false);
        if (isOuter) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        initViews();
        listners();
        return rootView;
    }


    private void initViews() {
        edt_current_pwd = rootView.findViewById(R.id.edt_current_pwd);
        edt_new_pwd = rootView.findViewById(R.id.edt_new_pwd);
        edt_confirm_pwd = rootView.findViewById(R.id.edt_confirm_pwd);
        iv_current_pwd = rootView.findViewById(R.id.iv_current_pwd);
        iv_new_pwd = rootView.findViewById(R.id.iv_new_pwd);
        iv_confirm_pwd = rootView.findViewById(R.id.iv_confirm_pwd);
        btn_submit = rootView.findViewById(R.id.btn_submit);
    }

    private void listners() {

        iv_current_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_current_pwd.isSelected()) {
                    iv_current_pwd.setSelected(false);
                    edt_current_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_current_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_current_pwd.setTypeface(type);
                } else {
                    iv_current_pwd.setSelected(true);
                    edt_current_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_current_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_current_pwd.setTypeface(type);
                }
            }
        });

        iv_new_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_new_pwd.isSelected()) {
                    iv_new_pwd.setSelected(false);
                    edt_new_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_new_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_new_pwd.setTypeface(type);
                } else {
                    iv_new_pwd.setSelected(true);
                    edt_new_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_new_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_new_pwd.setTypeface(type);
                }
            }
        });

        iv_confirm_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_confirm_pwd.isSelected()) {
                    iv_confirm_pwd.setSelected(false);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_confirm_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_confirm_pwd.setTypeface(type);
                } else {
                    iv_confirm_pwd.setSelected(true);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_confirm_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/century_gothic.ttf");
                    edt_confirm_pwd.setTypeface(type);
                }
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
    }

    private void validateData() {
        current_pwd = edt_current_pwd.getText().toString().trim();
        new_pwd = edt_new_pwd.getText().toString().trim();
        confirm_pwd = edt_confirm_pwd.getText().toString().trim();
        if (!current_pwd.isEmpty() && !new_pwd.isEmpty() && !confirm_pwd.isEmpty()) {
            if (CheckNetwork.isInternetAvailable(context)) {
                if (edt_new_pwd.getText().toString().equals(edt_confirm_pwd.getText().toString())) {
                        if (CheckNetwork.isInternetAvailable(context)) {
                            changePassword(current_pwd, new_pwd, confirm_pwd);
                        } else {
                            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                        }
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Password does not match");
                    builder1.setCancelable(true);

                    builder1.setNegativeButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            } else {
                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (current_pwd.isEmpty()) {
                edt_current_pwd.setError("Please enter current password");
                edt_current_pwd.requestFocus();
            } else if (new_pwd.isEmpty()) {
                edt_new_pwd.setError("Please enter new password");
                edt_new_pwd.requestFocus();
            } else if (edt_new_pwd.length() < 3){
                edt_new_pwd.setError("New password must be minimum 3 character");
                edt_new_pwd.requestFocus();
            }else if (confirm_pwd.isEmpty()) {
                edt_confirm_pwd.setError("Please enter confirm password");
                edt_confirm_pwd.requestFocus();
            }else if (edt_confirm_pwd.length() < 3){
                edt_confirm_pwd.setError("Confirm password must be minimum 3 character");
                edt_confirm_pwd.requestFocus();
            }
        }
    }

    private void clearAllEdditText() {
        edt_current_pwd.requestFocus();
        edt_current_pwd.getText().clear();
        edt_new_pwd.getText().clear();
        edt_confirm_pwd.getText().clear();
    }

    private void changePassword(String current_pwd, String new_pwd, String confirm_password) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("current_password", current_pwd);
                jsonObject.put("new_password", new_pwd);
                jsonObject.put("confirm_password", confirm_password);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommonResponse> getcall = apiInterface.getChangePassword(token, u_id, requestBody);
            getcall.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> getcall, Response<CommonResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            clearAllEdditText();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setMessage(msg);
                            builder1.setCancelable(true);
                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent loginIntent = new Intent(context, LoginActivity.class);
                                            startActivity(loginIntent);
                                            dialog.cancel();
                                            getActivity().finish();
                                        }
                                    });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}


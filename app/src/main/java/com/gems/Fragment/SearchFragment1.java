package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.SearchEventsAdapter;
import com.gems.Adapter.SearchVenueAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.Search;
import com.gems.Model.SearchVenue;
import com.gems.Model.UpcomingEventData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment1 extends Fragment {

    FragmentManager fragmentManager;
    private AppCompatDialog mProgressDialog;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private RelativeLayout ll_venue_event;
    boolean isOuter;
    private View rootView;
    private RecyclerView rv_venues, rv_events;
    SearchVenueAdapter searchVenueAdapter;
    SearchEventsAdapter searchEventsAdapter;

    private ArrayList<SearchVenue> searchVenues = new ArrayList<>();
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    private TextView tv_no_venues, tv_no_events;
    private EditText et_search;
    private ImageView iv_search;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager1;


    @SuppressLint("ValidFragment")
    public SearchFragment1(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        init();
        return rootView;
    }

    private void init() {
        ll_venue_event = rootView.findViewById(R.id.ll_venue_event);
        tv_no_venues = rootView.findViewById(R.id.tv_no_venues);
        tv_no_events = rootView.findViewById(R.id.tv_no_events);
        rv_venues = rootView.findViewById(R.id.rv_venues);
        rv_events = rootView.findViewById(R.id.rv_events);
        layoutManager = new LinearLayoutManager(context);
        rv_events.setLayoutManager(new GridLayoutManager(context, 2));
        layoutManager1 = new LinearLayoutManager(context);
        rv_venues.setLayoutManager(new GridLayoutManager(context, 2));
        et_search = rootView.findViewById(R.id.et_search);
        iv_search = rootView.findViewById(R.id.iv_search);
        Lisner();
    }

    private void Lisner() {
        ll_venue_event.setVisibility(View.GONE);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_search, InputMethodManager.SHOW_FORCED);
                if (CheckNetwork.isInternetAvailable(context)) {
                    String search_text = et_search.getText().toString();
                    if (!search_text.equals("") && !search_text.equals(null)) {
                        ll_venue_event.setVisibility(View.VISIBLE);
                        getVenueData();
                        et_search.getText().clear();
                    } else {
                        et_search.requestFocus();
                        et_search.setError("Please enter the text.");
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEARCH)) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(et_search, InputMethodManager.SHOW_FORCED);
                    if (CheckNetwork.isInternetAvailable(context)) {
                        String search_text = et_search.getText().toString();
                        if (!search_text.equals("") && !search_text.equals(null)) {
                            ll_venue_event.setVisibility(View.VISIBLE);
                            getVenueData();
                            et_search.getText().clear();
                        } else {
                            et_search.requestFocus();
                            et_search.setError("Please enter the text.");
                        }
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void getVenueData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("search", et_search.getText().toString());
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Search> getcall = apiInterface.getSearch(token, u_id, requestBody);
            getcall.enqueue(new Callback<Search>() {
                @Override
                public void onResponse(Call<Search> call, final Response<Search> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            searchVenues = new ArrayList<>();
                            searchVenues = response.body().getVenuesData();
                            if (searchVenues.size() == 0) {
                                rv_venues.setVisibility(View.GONE);
                                tv_no_venues.setVisibility(View.VISIBLE);
                                Log.d("mytag","here if venue not  available"+searchVenues.size());
                            } else {
                                Log.d("mytag","here else venue available"+searchVenues.size());
                                searchVenueAdapter = new SearchVenueAdapter(context, searchVenues, fragmentManager, rl_fragments_header);
                                rv_venues.setAdapter(searchVenueAdapter);
                            }

                            upcomingEventData = new ArrayList<>();
                            upcomingEventData = response.body().getEventsData();
                            if (upcomingEventData.size() == 0) {
                                rv_events.setVisibility(View.GONE);
                                tv_no_events.setVisibility(View.VISIBLE);
                                Log.d("mytag","here if event not  available"+upcomingEventData.size());
                            } else {
                                Log.d("mytag","here else event available"+upcomingEventData.size());
                                searchEventsAdapter = new SearchEventsAdapter(context, upcomingEventData, fragmentManager, rl_fragments_header);
                                rv_events.setAdapter(searchEventsAdapter);
                            }
                        } else if (status == 422) {
                            tv_no_venues.setVisibility(View.VISIBLE);
                            tv_no_events.setVisibility(View.VISIBLE);
                            rv_venues.setVisibility(View.GONE);
                            rv_events.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            tv_no_venues.setVisibility(View.VISIBLE);
                            tv_no_events.setVisibility(View.VISIBLE);
                            rv_venues.setVisibility(View.GONE);
                            rv_events.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Search> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;

import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.ShareSocial;
import com.gems.Model.ShareAppData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class ShareFragment extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    boolean isOuter;
    AppCompatDialog mProgressDialog;
    private ArrayList<ShareAppData> shareData = new ArrayList<ShareAppData>();

    private String android_link,ios_link;
    private String  shareBody;
    LinearLayout ll_share_btn;
    private Button btn_shareap;

    @SuppressLint("ValidFragment")
    public ShareFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_share, container, false);
        if (isOuter) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("Share Our App");
        init();
        listner();
        return rootView;
    }

    private void init() {
        ll_share_btn = rootView.findViewById(R.id.ll_share_btn);
        btn_shareap = rootView.findViewById(R.id.btn_shareap);
        getShareData();
    }

    private void listner() {

        btn_shareap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(shareIntent, "Share via"));
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    private void getShareData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ShareSocial> getcall = apiInterface.getContactShare(token, u_id);
            getcall.enqueue(new Callback<ShareSocial>() {
                @Override
                public void onResponse(Call<ShareSocial> call, final Response<ShareSocial> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            shareData = response.body().getShareourappData();
                            android_link = shareData.get(0).getAndroidLink();
                            ios_link = shareData.get(0).getIosLink();
                            shareBody = ("Check out the Gems application: " + "\n\n" + "Android Link : " + android_link +
                                    "\n\n" + "iOS Link : " + ios_link);
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ShareSocial> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.gems.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.InboxAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Customview.RecyclerViewPositionHelper;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.Notification;
import com.gems.Model.NotificationData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationFragment extends Fragment {
    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    RecyclerView.LayoutManager layoutManager;
    private ArrayList<NotificationData> inboxData = new ArrayList<>();
    private ArrayList<NotificationData> newinboxData = new ArrayList<>();
    RecyclerView rv_notification;
    private InboxAdapter inboxAdapter;
    ProgressDialog pd;
    private boolean isLoading = false;
    public int page = 1, totalPage;
    private AppCompatDialog mProgressDialog;
    private TextView tv_nodata_found;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;

    public NotificationFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("Notifications");
        init();
        listener();
        return rootView;
    }

    private void init() {
        rv_notification = rootView.findViewById(R.id.rv_notification);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);

        layoutManager = new LinearLayoutManager(context);
        rv_notification.setLayoutManager(layoutManager);
        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_notification);

        inboxData = new ArrayList<>();
        inboxAdapter= new InboxAdapter( context,inboxData);
        rv_notification.setAdapter(inboxAdapter);
        inboxAdapter.notifyDataSetChanged();
        getInbox(page);

    }

    private void listener() {
        rv_notification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) rv_notification.getLayoutManager();
                    int total = layoutManager.getItemCount();
                    int currentLastItem = recyclerViewPositionHelper.findLastCompletelyVisibleItemPosition() + 1;
                    if (currentLastItem == total) {
                        if (page <= totalPage) {
                            isLoading = true;
                            page++;
                            getInbox(page);
                        }
                    }
                }
            }
        });
    }

    private void getInbox(int page) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Notification> getcall = apiInterface.getNotification(token, u_id,50,page);
            getcall.enqueue(new Callback<Notification>() {
                @Override
                public void onResponse(Call<Notification> call, final Response<Notification> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        newinboxData = new ArrayList<>();
                        newinboxData = response.body().getData();
                        if (status == 200) {
                            tv_nodata_found.setVisibility(View.GONE);
                            rv_notification.setVisibility(View.VISIBLE);
                            inboxAdapter.add(newinboxData);
                            totalPage = response.body().getLastPage();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            tv_nodata_found.setVisibility(View.VISIBLE);
                            rv_notification.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Notification> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
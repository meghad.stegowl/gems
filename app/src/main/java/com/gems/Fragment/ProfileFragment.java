package com.gems.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.LoginActivity;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Helper.Utils;
import com.gems.Model.ContryList;
import com.gems.Model.GetProfile;
import com.gems.Model.GetProfileData;
import com.gems.Model.ImageUploadResponse;
import com.gems.Model.StateList;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;
import com.gems.Rest.WebInterface;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    boolean isOuter;
    private View rootView;
    private final Context context;
    private AppCompatDialog mProgressDialog;
    private final RelativeLayout rl_fragments_header;
    private final int GALLERY = 2;
    private final int CAMERA = 1;
    private Uri resultUri;
    private Bitmap bitmap;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    private String emailptrn = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private String showpicturedialog_title, selectfromgallerymsg, selectfromcameramsg, canceldialog;
    private Button btn_update_profile,btn_change_pwd;
    private CircleImageView profilePic;
    private EditText edt_first_name, edt_last_name, edt_user_name, edt_email, edt_zip, edt_phone;
    private FrameLayout fm_profile;
    ArrayList<GetProfileData> getProfileData = new ArrayList<>();
    private String lastChar = " ";

    private Spinner spn_city, spn_state;
    private ArrayList<ContryList> contryLists = new ArrayList<>();
    private ArrayList<StateList> stateLists = new ArrayList<>();
    private String[] items_country;
    private String[] items_state;
    private String items_country_id, items_city_name, items_state_id, items_state_name;
    String city, state;
    ArrayAdapter<String> countryAdapter;
    FragmentManager fragmentManager;

    public ProfileFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter,FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        requestMultiplePermissions();

        showpicturedialog_title = "Select the Action";
        selectfromgallerymsg = "Select photo from Gallery";
        selectfromcameramsg = "Capture photo from Camera";
        canceldialog = "Cancel";
        init();
        listener();
        getProfileDetail();
        return rootView;
    }

    private void init() {
        edt_first_name = rootView.findViewById(R.id.edt_first_name);
        edt_last_name = rootView.findViewById(R.id.edt_last_name);
        edt_user_name = rootView.findViewById(R.id.edt_user_name);
        edt_email = rootView.findViewById(R.id.edt_email);
        edt_zip = rootView.findViewById(R.id.edt_zip);
        edt_phone = rootView.findViewById(R.id.edt_phone);
        btn_update_profile = rootView.findViewById(R.id.btn_update_profile);
        btn_change_pwd = rootView.findViewById(R.id.btn_change_pwd);
        profilePic = rootView.findViewById(R.id.profilePic);
        fm_profile = rootView.findViewById(R.id.fm_profile);
        spn_state = rootView.findViewById(R.id.spn_state);
        spn_city = rootView.findViewById(R.id.spn_city);


        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = edt_phone.getText().toString().length();
                if (digits > 1)
                    lastChar = edt_phone.getText().toString().substring(digits - 1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = edt_phone.getText().toString().length();
                if (!lastChar.equals("-")) {
                    if (digits == 3 || digits == 7) {
                        edt_phone.append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void listener() {

        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                items_state_id = stateLists.get(pos).getId();
                items_state_name = stateLists.get(pos).getName();
                Log.d("mytag", "Select State id and name : " + items_state_id + "\n" + " -- " + items_state_name);
                getCityData(items_state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spn_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (contryLists != null) {
                    items_country_id = contryLists.get(pos).getId();
                    items_city_name = contryLists.get(pos).getName();
                    Log.d("okhttp", "Select Country id and city : " + items_country_id + contryLists.get(pos).getName());
                }else{
                    final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(context,
                            R.style.MyAlertDialogStyle);
                    alertDialogBuilder.setMessage("No Cities Available");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });

                    final androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    alertDialog.show();
                }            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        btn_change_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new ChangePasswordFragment(context, rl_fragments_header, false, fragmentManager), "Game Listing", true);
            }
        });

        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mytag", "save is clicked");
                validateData();
            }
        });

        fm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showPictureDialog_chooser();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getStateData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            stateLists = new ArrayList<StateList>();
            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        response = WebInterface.getInstance().doGet(Const.GET_STATES + "?type=" + "states");
                        Log.d("mytag", "response is---------" + jsonObject.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Utils.getInstance().d("Country Response : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    try {
                        int state_pos = 0;
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray.length() > 0) {
                            items_state = new String[jsonArray.length() + 1];
                            items_state[0] = "Preferred Province / State";
                            stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject country_data = jsonArray.getJSONObject(i);
                                String name = country_data.getString("name");
                                String state_id = country_data.getString("id");
                                if (name.equals(items_state_name)) {
                                    state_pos = i + 1;
                                    items_state_id = state_id;
                                }
                                String country_id = country_data.getString("country_id");
                                items_state[i + 1] = name;
                                stateLists.add(new StateList(state_id, name, country_id));
                            }
                        } else {
                            items_state = new String[1];
                            items_state[0] = "Preferred Province / State";
                            stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(context, R.layout.row_city, R.id.tv_state,
                                items_state);
                        spn_state.setAdapter(stateAdapter);
                        stateAdapter.notifyDataSetChanged();
                        spn_state.setSelection(state_pos, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getCityData(final String state_id) {
        if (CheckNetwork.isInternetAvailable(context)) {
            contryLists = new ArrayList<ContryList>();
            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        response = WebInterface.getInstance().doGet(Const.GET_CITIES + "?type=" + "cities" + "&state_id=" + state_id);
//                        response = WebInterface.getInstance().doPostRequest(Const.GET_CITIES + "/" + state_id, jsonObject.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Utils.getInstance().d("Country Response : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    try {
                        int city_pos = 0;
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        if (jsonArray.length() > 0) {
                            items_country = new String[jsonArray.length() + 1];
                            items_country[0] = "Preferred City";
                            contryLists.add(new ContryList("0", "Preferred City", "0"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject country_data = jsonArray.getJSONObject(i);
                                String name = country_data.getString("name");
                                String id = country_data.getString("id");
                                if (name.equals(items_city_name)) {
                                    city_pos = i + 1;
                                    items_country_id = id;
                                }
                                String state_id = country_data.getString("state_id");
                                items_country[i + 1] = name;
                                contryLists.add(new ContryList(id, name, state_id));
                            }
                        } else {
                            items_country = new String[1];
                            items_country[0] = "No Cities available";
                            contryLists.add(new ContryList("0", "No Cities available", "0"));
                        }
                       countryAdapter = new ArrayAdapter<String>(context, R.layout.row_city, R.id.tv_state, items_country);
                        spn_city.setAdapter(countryAdapter);
                        countryAdapter.notifyDataSetChanged();
                        spn_city.setSelection(city_pos, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getProfileDetail() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<GetProfile> getcall = apiInterface.getProfile("ProfileDetails", token, u_id);
            getcall.enqueue(new Callback<GetProfile>() {
                @Override
                public void onResponse(Call<GetProfile> getcall, Response<GetProfile> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            getProfileData = response.body().getData();
                            int user_id = getProfileData.get(0).getUserId();
                            String image = getProfileData.get(0).getImage();
                            Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, image);
                            String username = getProfileData.get(0).getUsername();
                            edt_user_name.setText(username);
                            String first_name = getProfileData.get(0).getFirstname();
                            edt_first_name.setText(first_name);
                            String last_name = getProfileData.get(0).getLastname();
                            edt_last_name.setText(last_name);
                            String email = getProfileData.get(0).getEmail();
                            edt_email.setText(email);
                            String desc = getProfileData.get(0).getZipcode();
                            edt_zip.setText(desc);
                            String phone = getProfileData.get(0).getPhone();
                            String contact1 = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6, 10);
                            edt_phone.setText(contact1);
                            items_state_name = getProfileData.get(0).getState();
                            items_city_name = getProfileData.get(0).getCity();
                            getStateData();
                            String fcm_id = getProfileData.get(0).getFcmId();

                            if (!image.equals("")) {
                                Glide.with(context)
                                        .load(image)
                                        .placeholder(R.drawable.trans_logo)
                                        .fitCenter()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .into(profilePic);
                            }

                            Prefs.getPrefInstance().setValue(context, Const.ACCESS_TOKEN, token);
                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, String.valueOf(user_id));
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            Glide.with(context)
                                    .load(R.drawable.trans_logo)
                                    .placeholder(R.drawable.trans_logo)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()
                                    .into(profilePic);

                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetProfile> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void validateData() {
        String phone = edt_phone.getText().toString().trim();
        Log.d("mytag", "validateData data");


        if (spn_state.getSelectedItem() == null) {
            return;
        } else {
            state = spn_state.getSelectedItem().toString();
            Log.d("mytag", "state is----" + items_state_name + "  -- " + state);
        }

        if (spn_city.getSelectedItem() == null) {
            return;
        } else {
            city = spn_city.getSelectedItem().toString();
            Log.d("mytag", "city is----" + items_city_name + "  -- " + city);
        }

        if (edt_first_name.getText().toString().length() == 0) {
            edt_first_name.requestFocus();
            edt_first_name.setError("Please Enter First Name.");
        } else if (edt_last_name.getText().toString().length() == 0) {
            edt_last_name.requestFocus();
            edt_last_name.setError("Please Enter Last Name.");
        } else if (edt_user_name.getText().toString().length() == 0) {
            edt_user_name.requestFocus();
            edt_user_name.setError("Please Enter UserName.");
        } else if (edt_email.getText().toString().trim().length() == 0 &&
                !edt_email.getText().toString().trim().matches(emailPattern) ||
                !edt_email.getText().toString().trim().matches(emailptrn)) {
            edt_email.requestFocus();
            edt_email.setError("Please Enter Valid  Email.");
        } /*else if (!state.equals("") && !state.equals("Preferred Province / State")) {
            Toast.makeText(context, "Please select Preferred State.", Toast.LENGTH_SHORT).show();
        } else if (!city.equals("") && !city.equals("Preferred City")) {
            Toast.makeText(context, "Please select Preferred City.", Toast.LENGTH_SHORT).show();
        } */else if (edt_zip.getText().toString().length() == 0) {
            edt_zip.requestFocus();
            edt_zip.setError("Please Enter Zip Code.");
        } else if (edt_phone.getText().toString().length() == 0) {
            edt_phone.requestFocus();
            edt_phone.setError("Please Enter Phone Number.");
        } else if (edt_phone.getText().toString().length() < 12) {
            edt_phone.requestFocus();
            edt_phone.setError("Please Enter valid Phone Number.");
        } else {
            if (CheckNetwork.isInternetAvailable(context)) {
                if (resultUri == null) {
                    submitProfileDetail(Prefs.getPrefInstance().getValue(context, Const.SET_PROFILE_IMAGE, ""));
                } else {
                    Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
                }
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void submitProfileDetail(final String url) {
        Log.d("mytag", "submit profile data" + url);
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String user_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
            String fcm = Prefs.getPrefInstance().getValue(context, Const.FCM, "");
            try {
                jsonObject.put("user_id", user_id);
                jsonObject.put("image", url);
                jsonObject.put("username", edt_user_name.getText().toString());
                jsonObject.put("firstname", edt_first_name.getText().toString());
                jsonObject.put("lastname", edt_last_name.getText().toString());
                jsonObject.put("email", edt_email.getText().toString());
                jsonObject.put("state_id", items_state_id);
                jsonObject.put("state", items_state_name);
                jsonObject.put("city_id", items_country_id);
                jsonObject.put("city", items_city_name);
                jsonObject.put("zipcode", edt_zip.getText().toString());
                jsonObject.put("phone", edt_phone.getText().toString().replace("-", ""));
                jsonObject.put("fcm_id", fcm);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

            Call<GetProfile> getcall = apiInterface.updateProfile("ProfileUpdate", token, u_id, requestBody);
            getcall.enqueue(new Callback<GetProfile>() {
                @Override
                public void onResponse(Call<GetProfile> getcall, Response<GetProfile> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, url);
                            getProfileData = response.body().getData();
                            int user_id = getProfileData.get(0).getUserId();
                            String image = getProfileData.get(0).getImage();
                            String firstname = getProfileData.get(0).getFirstname();
                            String lastname = getProfileData.get(0).getLastname();
                            String username = getProfileData.get(0).getUsername();
                            String email = getProfileData.get(0).getEmail();
                            String fcm_id = getProfileData.get(0).getFcmId();
//                            Prefs.getPrefInstance().setValue(context, Const.USERNAME, username);
                            Prefs.getPrefInstance().setValue(context, Const.ACCESS_TOKEN, token);
                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, String.valueOf(user_id));

                            androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(context);
                            builder1.setMessage(msg);
                            builder1.setCancelable(true);
                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            androidx.appcompat.app.AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetProfile> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(context, "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(context, "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showPictureDialog_chooser() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        pictureDialog.setTitle(showpicturedialog_title);
        String[] pictureDialogItems = {selectfromgallerymsg, selectfromcameramsg, canceldialog};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        startActivityForResult(intent, CAMERA);
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    CropImage(contentURI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    Log.d("mytag", "f=temp");
                    break;
                }
            }

            try {
                CropImage(Uri.fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleUCropResult(data);
        }
    }

    private void CropImage(Uri contentURI) throws IOException {
        Uri destinationUri = Uri.fromFile(new File(context.getCacheDir(), "temp.jpg"));
        UCrop.Options options = new UCrop.Options();
//      options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));

        UCrop.of(contentURI, destinationUri)
                .withOptions(options)
                .start(context, ProfileFragment.this);
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        try {
            resultUri = UCrop.getOutput(data);
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);
            Log.d("mytag", "my bitmap iss-" + bitmap);
//             Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            profilePic.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void Uploadimagetoserver(File file, final Uri resultUri) {
        Log.d("mytag", "Uploadimagetoserver profile data" + resultUri);
        if (CheckNetwork.isInternetAvailable(context)) {
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
            String fcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");
            Log.d("mytag", "access token" + token);
            Log.d("mytag", "user id" + u_id);
            Log.d("mytag", "fcm id" + fcm_id);

            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            final RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ImageUploadResponse> file_upload = apiInterface.UploadImage(fileToUpload);
            file_upload.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {

                    mProgressDialog.dismiss();
                    if (response.isSuccessful() && response != null) {
                        int status = response.body().getStatus();
                        if (status == 200) {
                            try {
                                profilePic.setImageBitmap(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),
                                        resultUri));
                                Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, response.body().getUrl());
                                submitProfileDetail(response.body().getUrl());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if (response.body().getStatus() == 404) {
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            if (resultUri == null) {
                                profilePic.setImageResource(R.drawable.trans_logo);
                            }
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });


        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(getActivity().RESULT_CANCELED, intent);
        getActivity().finish();
    }

    private void setResult(int resultCanceled, Intent intent) {

    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

}

package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.LoginActivity;
import com.gems.Adapter.MyAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.CommonResponse;
import com.gems.Model.UpcomingEventData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class UpcomingEventsDetailsFragment extends Fragment {

    boolean isOuter;
    private Context context;
    private View rootView;
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    FragmentManager fragmentManager;
    int position;
    RelativeLayout rl_fragments_header;
    final int max = 10, min = 1;

    private ImageView iv_club_image;
    private TextView date, tv_starttime, tv_endtime, tv_address, tv_mobile, tv_email, tv_desc, tv_quantity, tv_quantity_reserve, tv_quantity_total,
            tv_price, tv_total_price, tv_reserv_price;
    private String address = "";
    private Button btn_plus, btn_minus;
    private Button btn_plus_reserve, btn_minus_reserve;
    private Button btn_plus_total, btn_minus_total, btn_more, btn_conti_checkout;
    private String package_price, reservation_price, total_price;
    private LinearLayout ll_main_advance, ll_reserv_deposit, ll_total;
    private LinearLayout ll_plus_minus, ll_plus_minus_reserve;
    private Button btn_advance_sold_out, btn_reserve_sold_out;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private ImageView iv_advance, iv_reserve;
    private AppCompatDialog mProgressDialog;


    public UpcomingEventsDetailsFragment(Context context, boolean isOuter, FragmentManager fragmentManager, ArrayList<UpcomingEventData>
            upcomingEventData, int position, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.upcomingEventData = upcomingEventData;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
        this.position = position;
        this.rl_fragments_header = rl_fragments_header;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_event_details, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        Log.d("okhttp", "business_owner_id++++++++++++++" + upcomingEventData.get(position).getBusinessOwnerId());
        Log.d("okhttp", "venues_id__________________" + upcomingEventData.get(position).getVenuesId());
        Log.d("okhttp", "events_is++++++++++++++++++++++" + upcomingEventData.get(position).getEventsId());
        init();
        listener();
        return rootView;
    }

    private void init() {
        mPager = rootView.findViewById(R.id.pager);
        ll_main_advance = rootView.findViewById(R.id.ll_main_advance);
        ll_reserv_deposit = rootView.findViewById(R.id.ll_reserv_deposit);
        ll_total = rootView.findViewById(R.id.ll_total);
        iv_club_image = rootView.findViewById(R.id.iv_club_image);
        date = rootView.findViewById(R.id.date);
        tv_starttime = rootView.findViewById(R.id.tv_starttime);
        tv_endtime = rootView.findViewById(R.id.tv_endtime);
        tv_address = rootView.findViewById(R.id.tv_address);
        tv_mobile = rootView.findViewById(R.id.tv_mobile);
        tv_email = rootView.findViewById(R.id.tv_email);
        tv_desc = rootView.findViewById(R.id.tv_desc);
        tv_quantity = rootView.findViewById(R.id.tv_quantity);
        tv_quantity_reserve = rootView.findViewById(R.id.tv_quantity_reserve);
        tv_quantity_total = rootView.findViewById(R.id.tv_quantity_total);
        tv_price = rootView.findViewById(R.id.tv_price);
        tv_reserv_price = rootView.findViewById(R.id.tv_reserv_price);
        tv_total_price = rootView.findViewById(R.id.tv_total_price);

        btn_plus = rootView.findViewById(R.id.btn_plus);
        btn_minus = rootView.findViewById(R.id.btn_minus);
        btn_minus_reserve = rootView.findViewById(R.id.btn_minus_reserve);
        btn_plus_reserve = rootView.findViewById(R.id.btn_plus_reserve);
        btn_minus_total = rootView.findViewById(R.id.btn_minus_total);
        btn_plus_total = rootView.findViewById(R.id.btn_plus_total);
        btn_more = rootView.findViewById(R.id.btn_more);
        btn_conti_checkout = rootView.findViewById(R.id.btn_conti_checkout);
        iv_advance = rootView.findViewById(R.id.iv_advance);
        iv_reserve = rootView.findViewById(R.id.iv_reserve);
        btn_advance_sold_out = rootView.findViewById(R.id.btn_advance_sold_out);
        btn_reserve_sold_out = rootView.findViewById(R.id.btn_reserve_sold_out);
        ll_plus_minus = rootView.findViewById(R.id.ll_plus_minus);
        ll_plus_minus_reserve = rootView.findViewById(R.id.ll_plus_minus_reserve);
    }

    private void initSlider() {
        String image = upcomingEventData.get(position).getEventsImage();
        String image2 = upcomingEventData.get(position).getEventsImage2();
        String image3 = upcomingEventData.get(position).getEventsImage3();
        String image4 = upcomingEventData.get(position).getEventsImage4();
        String image5 = upcomingEventData.get(position).getEventsImage5();
        final String[] img = {image, image2, image3, image4, image5};

        ArrayList<String> ImgArray = new ArrayList<String>();

        for (int i = 0; i < img.length; i++)
            ImgArray.add(img[i]);

        mPager.setAdapter(new MyAdapter(context, ImgArray));
        CircleIndicator indicator = (CircleIndicator) rootView.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == img.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        //Auto start
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3500, 3500);
    }


    private void listener() {
        initSlider();

        Prefs.getPrefInstance().remove(context, Const.ADVANCE_AMOUNT);
        Prefs.getPrefInstance().remove(context, Const.RESERVE_AMOUNT);
        Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, "1");
        Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, "1");

        iv_advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_advance.setSelected(true);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                alertDialog.setMessage(upcomingEventData.get(position).getEventTicketInstruction());
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int which) {
                                d.dismiss();
                                iv_advance.setSelected(false);
                            }
                        });
                alertDialog.show();
            }
        });

        iv_reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_reserve.setSelected(true);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                alertDialog.setMessage(upcomingEventData.get(position).getEventReservationInstruction());
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int which) {
                                d.dismiss();
                                iv_reserve.setSelected(false);
                            }
                        });
                alertDialog.show();
            }
        });

        date.setText(upcomingEventData.get(position).getEventsDate());
        tv_starttime.setText(upcomingEventData.get(position).getEventsStartTime());
        tv_endtime.setText(upcomingEventData.get(position).getEventsEndTime());
        tv_address.setText(upcomingEventData.get(position).getEventsAddress());
        tv_mobile.setText(upcomingEventData.get(position).getEventsPhone());
        tv_email.setText(upcomingEventData.get(position).getEventsEmail());
        tv_desc.setText(upcomingEventData.get(position).getEventsName());
//        tv_desc.setText(Html.fromHtml("<font color='white'>" + upcomingEventData.get(position).getEventsDescription() + "</font>"));
        package_price = upcomingEventData.get(position).getEventTicketAmount();
        reservation_price = upcomingEventData.get(position).getEventReservationAmount();

        btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(upcomingEventData.get(position).getEventsMoreInfoUrl()));
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                }
            }
        });

        btn_conti_checkout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                if (skip_status.equals("0")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Please login to continue.");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface d, int which) {
                                    d.dismiss();
                                    Intent i = new Intent(context, LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Are you sure you want to subscribe to this Event?");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    ButtonClick();

                                    String total_ticket_amount = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_AMOUNT, "");
                                    String total_table_reservation_amount = Prefs.getPrefInstance().getValue(context, Const.RESERVE_AMOUNT, "");
                                    String total_table_reservation_buy = Prefs.getPrefInstance().getValue(context, Const.RESERVE_QUANTITY, "");
                                    String total_ticket_buy = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_QUANTITY, "");

                                    Log.d("mytag", "here is total_ticket_amount--------------------" + total_ticket_amount);
                                    Log.d("mytag", "here is getEventTicketAmount--------------------" + upcomingEventData.get(0).getEventTicketAmount());

                                    if (upcomingEventData.get(0).getEventFeeType().equals("Paid") &&
                                            upcomingEventData.get(0).getEventReservationFeeType().equals("Paid")) {
                                        if (upcomingEventData.get(0).getEventTotalTicket().equals("0")) {
                                            Log.d("mytag", "here is total_ticket_buy --------------------000000");
                                            Log.d("mytag", "here is total_table_reservation_buy --------------------000000" + total_ticket_buy);
                                        } else if (upcomingEventData.get(0).getEventTotalReservationTable().equals("0")) {
                                            Log.d("mytag", "here is total_table_reservation_buy  if else --------------------000000");
                                            Log.d("mytag", "here is total_ticket_buy if else--------------------000000" + total_ticket_buy);
                                        } else {
                                            Log.d("mytag", "here is total_table_reservation_buy else --------------------000000" + total_table_reservation_buy);
                                            Log.d("mytag", "here is total_ticket_buy else--------------------000000" + total_ticket_buy);
                                        }
                                    }else{
                                        Log.d("mytag", "here is total_table_reservation_buy main else --------------------000000" + total_table_reservation_buy);
                                        Log.d("mytag", "here is total_ticket_buy main else--------------------000000" + total_ticket_buy);

                                    }

                                    Log.d("mytag", "here is total_table_reservation_amount--------------------" + total_table_reservation_amount);
                                    Log.d("mytag", "here is getEventTicketAmount--------------------" + upcomingEventData.get(0).getEventTicketAmount());
                                    Log.d("mytag", "here is getEventReservationAmount--------------------" + upcomingEventData.get(0).getEventReservationAmount());
                                    if (upcomingEventData.get(0).getEventFeeType().equals("Free")) {
                                        Log.d("mytag", "here is ticket_type 000000--------------------");
                                    } else {
                                        Log.d("mytag", "here is ticket_type 111111--------------------");
                                    }

                                    if (upcomingEventData.get(0).getEventReservationFeeType().equals("Free")) {
                                        Log.d("mytag", "here is table_reservation_type 000000--------------------");
                                    } else {
                                        Log.d("mytag", "here is table_reservation_type 111111--------------------");
                                    }
                                    if (upcomingEventData.get(0).getEventFeeType().equals("Free") &&
                                            upcomingEventData.get(0).getEventReservationFeeType().equals("Free")) {
                                        Log.d("mytag", "here is event_type 000000--------------------");
                                    } else {
                                        Log.d("mytag", "here is event_type 111111--------------------");
                                    }
                                }
                            });
                    alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        if (upcomingEventData.get(position).getEventFeeType().equals("Paid")) {
            if (upcomingEventData.get(position).getEventTotalTicket().equals("0")) {
                if (upcomingEventData.get(position).getEventReservationFeeType().equals("Free") ||
                        upcomingEventData.get(position).getEventReservationAmount().equals("0")) {
                    btn_conti_checkout.setVisibility(View.GONE);
                    btn_advance_sold_out.setVisibility(View.VISIBLE);
                    ll_total.setVisibility(View.GONE);
                    ll_plus_minus.setVisibility(View.GONE);
                }
            } else {
                btn_conti_checkout.setVisibility(View.VISIBLE);
                btn_advance_sold_out.setVisibility(View.GONE);
                ll_plus_minus.setVisibility(View.VISIBLE);
            }
        } else if (upcomingEventData.get(position).getEventFeeType().equals("Free")
                || upcomingEventData.get(position).getEventTicketAmount().equals("0")) {
            ll_main_advance.setVisibility(View.GONE);
            Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, "0");
        }

        if (upcomingEventData.get(position).getEventReservationFeeType().equals("Paid")) {
            if (upcomingEventData.get(position).getEventTotalReservationTable().equals("0")) {
                if (upcomingEventData.get(position).getEventFeeType().equals("Free")
                        || upcomingEventData.get(position).getEventTicketAmount().equals("0")) {
                    btn_conti_checkout.setVisibility(View.GONE);
                    btn_reserve_sold_out.setVisibility(View.VISIBLE);
                    ll_plus_minus_reserve.setVisibility(View.GONE);
                    ll_total.setVisibility(View.GONE);
                }
            } else {
                btn_conti_checkout.setVisibility(View.VISIBLE);
                btn_reserve_sold_out.setVisibility(View.GONE);
                ll_plus_minus_reserve.setVisibility(View.VISIBLE);
            }
        } else if (upcomingEventData.get(position).getEventReservationFeeType().equals("Free") ||
                upcomingEventData.get(position).getEventReservationAmount().equals("0")) {
            ll_reserv_deposit.setVisibility(View.GONE);
            Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, "0");
        }

        if (upcomingEventData.get(position).getEventFeeType().equals("Paid") &&
                upcomingEventData.get(position).getEventReservationFeeType().equals("Paid")) {
            if (upcomingEventData.get(position).getEventTotalTicket().equals("0") &&
                    upcomingEventData.get(position).getEventTotalReservationTable().equals("0")) {
                btn_conti_checkout.setVisibility(View.GONE);
                btn_reserve_sold_out.setVisibility(View.VISIBLE);
                ll_plus_minus_reserve.setVisibility(View.GONE);
                ll_plus_minus.setVisibility(View.GONE);
                btn_advance_sold_out.setVisibility(View.VISIBLE);
                ll_total.setVisibility(View.GONE);
            } else {
                if (upcomingEventData.get(position).getEventTotalTicket().equals("0")) {
                    ll_plus_minus.setVisibility(View.GONE);
                    btn_advance_sold_out.setVisibility(View.VISIBLE);
                    Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, "0");
                } else if (upcomingEventData.get(position).getEventTotalReservationTable().equals("0")) {
                    btn_reserve_sold_out.setVisibility(View.VISIBLE);
                    ll_plus_minus_reserve.setVisibility(View.GONE);
                    Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, "0");
                }
            }
        }

//        if (upcomingEventData.get(position).getEventFeeType().equals("Paid") &&
//                upcomingEventData.get(position).getEventReservationFeeType().equals("Paid")) {
//            if(upcomingEventData.get(position).getEventTotalTicket().equals("0")){
//                btn_advance_sold_out.setVisibility(View.VISIBLE);
//            }else if(upcomingEventData.get(position).getEventTotalReservationTable().equals("0")){
//                btn_reserve_sold_out.setVisibility(View.VISIBLE);
//            }
//        }


        if (upcomingEventData.get(position).getEventFeeType().equals("Free") &&
                upcomingEventData.get(position).getEventReservationFeeType().equals("Free")) {
            ll_main_advance.setVisibility(View.GONE);
            ll_reserv_deposit.setVisibility(View.GONE);
            ll_total.setVisibility(View.GONE);
            btn_conti_checkout.setText("FREE ENTERY");
        }


        Prefs.getPrefInstance().setValue(context, Const.ADVANCE_AMOUNT, package_price);
        Prefs.getPrefInstance().setValue(context, Const.RESERVE_AMOUNT, reservation_price);

        tv_price.setText(String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(package_price)));
        tv_reserv_price.setText(String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(reservation_price)));
        float advanceAmount = Float.parseFloat(package_price);
        float reserveAmount = Float.parseFloat(reservation_price);

        DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");

        if (upcomingEventData.get(position).getEventFeeType().equals("Paid") &&
                upcomingEventData.get(position).getEventReservationFeeType().equals("Paid")) {
            if (upcomingEventData.get(position).getEventTotalTicket().equals("0")) {
                tv_total_price.setText(REAL_FORMATTER.format(0 + reserveAmount));
             Prefs.getPrefInstance().setValue(context, Const.ADVANCE_AMOUNT, "0");
                Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, "0");
            } else if (upcomingEventData.get(position).getEventTotalReservationTable().equals("0")) {
                tv_total_price.setText(REAL_FORMATTER.format(advanceAmount + 0));
                Prefs.getPrefInstance().setValue(context, Const.RESERVE_AMOUNT, "0");
                Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, "0");
            }else{
                tv_total_price.setText(REAL_FORMATTER.format(advanceAmount + reserveAmount));
            }
        } else {
            tv_total_price.setText(REAL_FORMATTER.format(advanceAmount + reserveAmount));
        }


        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int que = Integer.parseInt(tv_quantity.getText().toString());
                int max = Integer.parseInt(upcomingEventData.get(position).getEventTotalTicket());
                if (que < max) {
                    que++;
                    float final_amount = Float.parseFloat(package_price) * que;
                    Log.d("mytag", "Count : " + que);
                    tv_quantity.setText(String.valueOf(que));
                    Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, String.valueOf(que));
//                    tv_price.setText(String.format(Locale.ENGLISH, "%.2f", final_amount));
                    Log.d("mytag", "plus advance final_amount ----------------" + final_amount);
                    Prefs.getPrefInstance().setValue(context, Const.ADVANCE_AMOUNT, String.valueOf(final_amount));

                    String reservation_price = Prefs.getPrefInstance().getValue(context, Const.RESERVE_AMOUNT, "");
                    float reserveAmount = Float.parseFloat(reservation_price);
                    DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");
                    tv_total_price.setText(REAL_FORMATTER.format(final_amount + reserveAmount));
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Unfortunately you cannot add this amount. Try adding a lesser number and continue");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface d, int which) {
                                    d.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int que = Integer.parseInt(tv_quantity.getText().toString());
                float price = Float.parseFloat(tv_price.getText().toString());
                if (que > min) {
                    que--;
                    float final_amount =/* price - */(Float.parseFloat(package_price) * que);
                    Prefs.getPrefInstance().setValue(context, Const.ADVANCE_QUANTITY, String.valueOf(que));
                    tv_quantity.setText(String.valueOf(que));
//                    tv_price.setText(String.format(Locale.ENGLISH, "%.2f", final_amount));
                    Prefs.getPrefInstance().setValue(context, Const.ADVANCE_AMOUNT, String.valueOf(final_amount));
                    String reservation_price = Prefs.getPrefInstance().getValue(context, Const.RESERVE_AMOUNT, "");
                    Log.d("mytag", "minus advaance final_amount ----------------" + reservation_price);
                    float reserveAmount = Float.parseFloat(reservation_price);
                    DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");
                    tv_total_price.setText(REAL_FORMATTER.format(final_amount + reserveAmount));
                }
            }
        });

        btn_plus_reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int que = Integer.parseInt(tv_quantity_reserve.getText().toString());
                int max = Integer.parseInt(upcomingEventData.get(position).getEventTotalReservationTable());
                if (que < max) {
                    que++;
                    float final_amount = Float.parseFloat(reservation_price) * que;
                    tv_quantity_reserve.setText(String.valueOf(que));
                    Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, String.valueOf(que));
                    Log.d("mytag", "plus reserve final_amount ----------------" + final_amount);
                    Prefs.getPrefInstance().setValue(context, Const.RESERVE_AMOUNT, String.valueOf(final_amount));

                    String advance_price = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_AMOUNT, "");
                    float advanceAmount = Float.parseFloat(advance_price);
                    DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");
                    tv_total_price.setText(REAL_FORMATTER.format(final_amount + advanceAmount));
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Unfortunately you cannot add this amount. Try adding a lesser number and continue");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface d, int which) {
                                    d.dismiss();

                                }
                            });
                    alertDialog.show();
                }
            }
        });

        btn_minus_reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int que = Integer.parseInt(tv_quantity_reserve.getText().toString());
                float price = Float.parseFloat(tv_reserv_price.getText().toString());
                if (que > min) {
                    que--;
                    float final_amount =/* price - */(Float.parseFloat(reservation_price) * que);
                    Prefs.getPrefInstance().setValue(context, Const.RESERVE_QUANTITY, String.valueOf(que));
                    tv_quantity_reserve.setText(String.valueOf(que));
                    Log.d("mytag", "minus reserve final_amount ----------------" + final_amount);
                    Prefs.getPrefInstance().setValue(context, Const.RESERVE_AMOUNT, String.valueOf(final_amount));

                    String advance_price = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_AMOUNT, "");
                    float advanceAmount = Float.parseFloat(advance_price);
                    DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");
                    tv_total_price.setText(REAL_FORMATTER.format(final_amount + advanceAmount));
                }
            }
        });

        Glide.with(context)
                .load(upcomingEventData.get(position).getEventsImage())
                .placeholder(R.drawable.trans_logo)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(iv_club_image);
        address = upcomingEventData.get(position).getEventsAddress();

        tv_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!address.equals("")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://maps.google.co.in/maps?q=" + address));
                    if (intent.resolveActivity(context.getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });

        tv_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + upcomingEventData.get(position).getEventsPhone()));
                context.startActivity(intent);
            }
        });

        tv_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + upcomingEventData.get(position).getEventsEmail()));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                    intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                }
            }
        });
    }

    private void getBookingDone() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String total_ticket_amount = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_AMOUNT, "");
            String total_table_reservation_amount = Prefs.getPrefInstance().getValue(context, Const.RESERVE_AMOUNT, "");
            String total_table_reservation_buy = Prefs.getPrefInstance().getValue(context, Const.RESERVE_QUANTITY, "");
            String total_ticket_buy = Prefs.getPrefInstance().getValue(context, Const.ADVANCE_QUANTITY, "");
            try {
                jsonObject.put("business_owner_id", upcomingEventData.get(position).getBusinessOwnerId());
                jsonObject.put("venues_id", upcomingEventData.get(position).getVenuesId());
                jsonObject.put("events_id", upcomingEventData.get(position).getEventsId());
                jsonObject.put("total_ticket_amount", total_ticket_amount);
                jsonObject.put("per_ticket_amount", upcomingEventData.get(position).getEventTicketAmount());

                if (upcomingEventData.get(0).getEventFeeType().equals("Paid") &&
                        upcomingEventData.get(0).getEventReservationFeeType().equals("Paid")) {
                    if (upcomingEventData.get(0).getEventTotalTicket().equals("0")) {
                        jsonObject.put("total_ticket_buy", "0");
                        jsonObject.put("total_table_reservation_buy", total_table_reservation_buy);
                    } else if (upcomingEventData.get(0).getEventTotalReservationTable().equals("0")) {
                        jsonObject.put("total_table_reservation_buy", "0");
                        jsonObject.put("total_ticket_buy", total_ticket_buy);
                    } else {
                        jsonObject.put("total_table_reservation_buy", total_table_reservation_buy);
                        jsonObject.put("total_ticket_buy", total_ticket_buy);
                    }
                } else {
                    jsonObject.put("total_table_reservation_buy", total_table_reservation_buy);
                    jsonObject.put("total_ticket_buy", total_ticket_buy);
                }

                jsonObject.put("total_table_reservation_amount", total_table_reservation_amount);
                jsonObject.put("per_table_reservation_amount", upcomingEventData.get(position).getEventReservationAmount());
                if (upcomingEventData.get(position).getEventFeeType().equals("Free")) {
                    jsonObject.put("ticket_type", 0);
                } else {
                    jsonObject.put("ticket_type", 1);
                }
                if (upcomingEventData.get(position).getEventReservationFeeType().equals("Free")) {
                    jsonObject.put("table_reservation_type", 0);
                } else {
                    jsonObject.put("table_reservation_type", 1);
                }
                if (upcomingEventData.get(position).getEventFeeType().equals("Free") &&
                        upcomingEventData.get(position).getEventReservationFeeType().equals("Free")) {
                    jsonObject.put("event_type", 0);
                } else {
                    jsonObject.put("event_type", 1);
                }
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommonResponse> getcall = apiInterface.getBookTicketTable("Booking", token, u_id, requestBody);
            getcall.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, final Response<CommonResponse> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            String messge = "Sorry!! Booking for " + upcomingEventData.get(position).getEventsName() + " has already been closed";
                            Log.d("okhttp","here is log of message is-------------------"+messge);
//                            if (msg.equals("Sorry!! Booking for March Maddness has already been closed") ||
//                                    msg.equals("Sorry!! Booking for April Enjoyment has already been closed") ||
//                                    msg.equals("Sorry!! Booking for Party Event 3 has already been closed") ||
//                                    msg.equals("Sorry!! Booking for 11 Day Party has already been closed") ||
//                                    msg.equals("Sorry!! Booking for Party Event has already been closed")) {
                            if (msg.equals(messge)) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                                alertDialog.setMessage(msg);
                                alertDialog.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface d, int which) {
                                                d.dismiss();
                                            }
                                        });
                                alertDialog.show();
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                                alertDialog.setMessage(msg);
                                alertDialog.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface d, int which) {
                                                d.dismiss();
                                                pushInnerFragment(new InnerTicketFragment(context, rl_fragments_header, false, fragmentManager), "Tickets", true);
                                            }
                                        });
                                alertDialog.show();
                            }

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void ButtonClick() {
        String event_date = upcomingEventData.get(position).getEventsDate();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String current_Date = df.format(c);
        Log.d("mytag", "current date----------- " + current_Date);

        DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMMM dd['st']['nd']['rd']['th'], yyyy").withLocale(Locale.ENGLISH);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy").withLocale(Locale.ENGLISH);
        System.out.println(formatter.format(parser.parse(event_date))); // 26/05/2017
        Log.d("mytag", "formatter.format(parser.parse(input)) is----------- " + formatter.format(parser.parse(event_date)));

        String event_convert_date = formatter.format(parser.parse(event_date));

        String event_time = upcomingEventData.get(position).getEventsEndTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm aa");
        String current_time = dateFormat.format(new Date()).toString();
        Log.d("mytag", "current_time is----------- " + current_time);
        Log.d("mytag", "event_time is----------- " + event_time);

        if (event_convert_date.compareTo(current_Date) > 0) {
            Log.d("mytag", "Event date  is after current date");
            getBookingDone();
        } else if (event_convert_date.compareTo(current_Date) < 0) {
            Log.d("mytag", "Event date is before current date");
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle(context.getResources().getString(R.string.app_name));
            alertDialog.setMessage("This event is expired");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface d, int which) {
                            d.dismiss();
                        }
                    });
            alertDialog.show();
        } else if (event_convert_date.compareTo(current_Date) == 0) {
            Log.d("mytag", "Event date is equal to current date");
            DateFormat dfrmt = new SimpleDateFormat("hh:mm a");
            Date Dtime1 = new Date();
            Date Dtime2 = new Date();
            try {
                Dtime1 = dfrmt.parse(event_time);
                Dtime2 = dfrmt.parse(current_time);
            } catch (Exception e) {
            }

            if (Dtime1.compareTo(Dtime2) > 0) {
                Log.d("mytag", "Event time  is after current time");
                Log.d("mytag", "yooo" + dfrmt.format(Dtime1) + " > " + dfrmt.format(Dtime2));
                getBookingDone();
            } else if (Dtime1.compareTo(Dtime2) < 0) {
                Log.d("mytag", "Event time  is Before current time");
                Log.d("mytag", "yooo" + dfrmt.format(Dtime1) + " < " + dfrmt.format(Dtime2));
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                alertDialog.setMessage("This event is expired");
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int which) {
                                d.dismiss();
                            }
                        });
                alertDialog.show();
            } else {
                Log.d("mytag", "Event time  is equal current time");
                Log.d("mytag", "yooo" + dfrmt.format(Dtime1) + " == " + dfrmt.format(Dtime2));
                getBookingDone();
            }
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}
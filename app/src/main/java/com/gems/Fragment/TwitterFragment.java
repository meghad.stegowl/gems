package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gems.Customview.CustomHeader;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.R;

@SuppressLint("ValidFragment")
public class TwitterFragment extends Fragment {

    private View rootView;
    private Context context;
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private RelativeLayout rl_fragments_header;
    private boolean isOuter = false;

    @SuppressLint("ValidFragment")
    public TwitterFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        rl_fragments_header.setVisibility(View.VISIBLE);
        init();
        return rootView;
    }

    private void init() {
        webView = (WebView) rootView.findViewById(R.id.webView);
        Log.i("mytag", "Link : " + Prefs.getPrefInstance().getValue(context, Const.TWIT, ""));
        startWebView(Prefs.getPrefInstance().getValue(context, Const.TWIT, ""));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }        });
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
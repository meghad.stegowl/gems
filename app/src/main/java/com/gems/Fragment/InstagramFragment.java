package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.gems.Customview.CustomHeader;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.R;

@SuppressLint("ValidFragment")
public class InstagramFragment extends Fragment {

    private View rootView;
    private Context context;
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private RelativeLayout rl_fragments_header;
    private boolean isOuter = false;

    @SuppressLint("ValidFragment")
    public InstagramFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        rl_fragments_header.setVisibility(View.VISIBLE);
        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("Notifications");
        init();
        return rootView;
    }

    private void init() {
        webView = (WebView) rootView.findViewById(R.id.webView);
        Log.i("mytag", "Link : " + Prefs.getPrefInstance().getValue(context, Const.INSTA, ""));
        startWebView(Prefs.getPrefInstance().getValue(context, Const.INSTA, ""));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }        });
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}


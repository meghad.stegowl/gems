package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Activity.MainActivity;
import com.gems.Adapter.EventHistoryAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Customview.RecyclerViewPositionHelper;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.EventHistory;
import com.gems.Model.EventHistoryData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class InnerTicketFragment extends Fragment {
    private final Context context;
    private final RelativeLayout rl_fragments_header;
    private View rootView;
    private FragmentManager fragmentManager;
    private RecyclerView rv_data;
    private Button btn_explore;
    private AppCompatDialog mProgressDialog;
    RecyclerView.LayoutManager layoutManager;
    private ArrayList<EventHistoryData> eventHistoryData = new ArrayList<>();
    private ArrayList<EventHistoryData> neweventHistoryData = new ArrayList<>();

    private boolean isLoading = false;
    public int page = 1, totalPage;
    private ScrollView no_ticket_found;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;
    private EventHistoryAdapter eventHistoryAdapter;
    boolean isOuter;

    @SuppressLint("ValidFragment")
    public InnerTicketFragment(Context context, RelativeLayout rl_fragments_header,boolean isOuter,FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.fragmentManager = fragmentManager;
        this.isOuter = isOuter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reservation_tickets, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        rl_fragments_header.setVisibility(View.VISIBLE);
        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("My Ticket and RSVP");
        init();
        return rootView;
    }

    private void init() {
        btn_explore = rootView.findViewById(R.id.btn_explore);
        rv_data = rootView.findViewById(R.id.rv_data);
        rv_data.setLayoutManager(new LinearLayoutManager(context));
        no_ticket_found = rootView.findViewById(R.id.no_ticket_found);
        no_ticket_found.setVisibility(View.GONE);
        btn_explore.setVisibility(View.GONE);
        btn_explore = rootView.findViewById(R.id.btn_explore);
        btn_explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);

            }
        });

        layoutManager = new LinearLayoutManager(context);
        rv_data.setLayoutManager(layoutManager);
        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_data);

        eventHistoryData = new ArrayList<>();
        eventHistoryAdapter = new EventHistoryAdapter( context, eventHistoryData,fragmentManager,rl_fragments_header);
        rv_data.setAdapter(eventHistoryAdapter);
        eventHistoryAdapter.notifyDataSetChanged();
        getEvents(page);

    }

    private void getEvents(int page) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<EventHistory> getcall = apiInterface.getEventHistory("BookingHistory",token, u_id,100,page);
            getcall.enqueue(new Callback<EventHistory>() {
                @Override
                public void onResponse(Call<EventHistory> call, final Response<EventHistory> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        neweventHistoryData = new ArrayList<>();
                        neweventHistoryData = response.body().getData();
                        if (status == 200) {
                            no_ticket_found.setVisibility(View.GONE);
                            btn_explore.setVisibility(View.GONE);
                            rv_data.setVisibility(View.VISIBLE);
                            eventHistoryAdapter.add(neweventHistoryData);
                            totalPage = response.body().getLastPage();
                        } else if (status == 422) {
                            no_ticket_found.setVisibility(View.VISIBLE);
                            btn_explore.setVisibility(View.VISIBLE);
                            rv_data.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            no_ticket_found.setVisibility(View.VISIBLE);
                            btn_explore.setVisibility(View.VISIBLE);
                            rv_data.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EventHistory> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

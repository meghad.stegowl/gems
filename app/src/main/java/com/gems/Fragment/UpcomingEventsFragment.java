package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.HomeViewAllUpcomingEventsAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Model.HomeUpcomingEvents;
import com.gems.R;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class UpcomingEventsFragment extends Fragment {

    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    RecyclerView.LayoutManager layoutManager;
    private ArrayList<HomeUpcomingEvents> homeUpcomingEvents = new ArrayList<>();
    RecyclerView rv_events;
    private HomeViewAllUpcomingEventsAdapter homeViewAllUpcomingEventsAdapter;
    ProgressDialog pd;
    FragmentManager fragmentManager;
    private TextView tv_nodata_found,tv_upcoming_events;

    public UpcomingEventsFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager,
                                  ArrayList<HomeUpcomingEvents> homeUpcomingEvents) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
        this.homeUpcomingEvents = homeUpcomingEvents;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_upcoming_events, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);

        init();
        return rootView;
    }

    private void init() {
        rv_events = rootView.findViewById(R.id.rv_events);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);
        tv_upcoming_events = rootView.findViewById(R.id.tv_upcoming_events);
        tv_upcoming_events.setText("Upcoming Events & More");
        layoutManager = new LinearLayoutManager(context);
        rv_events.setLayoutManager(new GridLayoutManager(context, 2));

        if(homeUpcomingEvents.size()== 0){
            tv_nodata_found.setVisibility(View.VISIBLE);
            rv_events.setVisibility(View.GONE);
        }else {
            homeViewAllUpcomingEventsAdapter = new HomeViewAllUpcomingEventsAdapter(context, homeUpcomingEvents, fragmentManager, rl_fragments_header,false);
            rv_events.setAdapter(homeViewAllUpcomingEventsAdapter);
        }

    }
}
package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Adapter.PaymentHistoryAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Customview.RecyclerViewPositionHelper;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.PaymentHistory;
import com.gems.Model.PaymentHistoryData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class PaymentHistoryFragment extends Fragment {

    boolean isOuter;
    private View rootView;
    private Context context;
    private AppCompatDialog mProgressDialog;
    private RelativeLayout rl_fragments_header;
    FragmentManager fragmentManager;

    private RecyclerView rv_payment_history;
    private ArrayList<PaymentHistoryData> paymentHistoryData = new ArrayList<>();
    private ArrayList<PaymentHistoryData> newpaymentHistoryData = new ArrayList<>();
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private int page = 1, totalPage;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;
    private boolean isLoading = false;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tv_nodata_found;

    public PaymentHistoryFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager){
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_payment_history, container, false);
        if (isOuter) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
        init();
        listener();
        return rootView;
    }

    private void init() {
        rv_payment_history = rootView.findViewById(R.id.rv_payment_history);
        tv_nodata_found = rootView.findViewById(R.id.tv_nodata_found);
        layoutManager = new LinearLayoutManager(context);
        rv_payment_history.setLayoutManager(layoutManager);
        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_payment_history);

        paymentHistoryData = new ArrayList<>();
        paymentHistoryAdapter = new PaymentHistoryAdapter( context, paymentHistoryData);
        rv_payment_history.setAdapter(paymentHistoryAdapter);
        paymentHistoryAdapter.notifyDataSetChanged();
//        getInbox(page);
    }

    private void listener() {

        rv_payment_history.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState)
            {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) rv_payment_history.getLayoutManager();
                    int total = layoutManager.getItemCount();
                    int currentLastItem = recyclerViewPositionHelper.findLastCompletelyVisibleItemPosition() + 1;
                    if (currentLastItem == total) {
                        if (page <= totalPage) {
                            isLoading = true;
                            page++;
//                            getInbox(page);
                        }
                    }
                }
            }
        });

    }

    private void getInbox(int page) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PaymentHistory> getcall = apiInterface.getPaymentHistory("Booking",token, u_id,50,page);
            getcall.enqueue(new Callback<PaymentHistory>() {
                @Override
                public void onResponse(Call<PaymentHistory> call, final Response<PaymentHistory> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        newpaymentHistoryData = new ArrayList<>();
                        newpaymentHistoryData = response.body().getData();
                        if (status == 200) {
                            tv_nodata_found.setVisibility(View.GONE);
                            rv_payment_history.setVisibility(View.VISIBLE);
                            paymentHistoryAdapter.add(newpaymentHistoryData);
                            totalPage = response.body().getLastPage();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            tv_nodata_found.setVisibility(View.VISIBLE);
                            rv_payment_history.setVisibility(View.GONE);
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PaymentHistory> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

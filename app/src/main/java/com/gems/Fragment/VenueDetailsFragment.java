package com.gems.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Adapter.VenueUpcomingEventsAdapter;
import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.GPSTracker;
import com.gems.Helper.Prefs;
import com.gems.Helper.Utils;
import com.gems.Model.UpcomingEventData;
import com.gems.Model.VenueDetails;
import com.gems.Model.VenueDetailsData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressLint("ValidFragment")
public class VenueDetailsFragment extends Fragment implements OnMapReadyCallback {

    boolean isOuter;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;

    private RecyclerView rv_upcoming_events;
    private ArrayList<VenueDetailsData> venueDetailsData = new ArrayList<>();
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    VenueUpcomingEventsAdapter venueUpcomingEventsAdapter;
    private AppCompatDialog mProgressDialog;
    FragmentManager fragmentManager;
    int venue_id;
    private GoogleMap mMap;
    double Latitude = 0;
    double Longitude = 0;
    private ImageView iv_club_image;
    private TextView tv_venue_name, tv_venue_time_note, tv_open_time, tv_close_time, tv_venue_desc, tv_music_type, tv_crowd_type,
            tv_what_you_wear, tv_what_you_cant_wear, tv_address, tv_contact, tv_venue_email;
//    private String lat = "25.7617", long_t = "80.1918";
//    private TrackerSettings settings =
//            new TrackerSettings()
//                    .setUseGPS(true)
//                    .setUseNetwork(true)
//                    .setUsePassive(true)
//                    .setTimeBetweenUpdates(1 * 60 * 1000)
//                    .setMetersBetweenUpdates(100);

    private String lat = "", long_t = "";
    private TrackerSettings settings =
            new TrackerSettings()
                    .setUseGPS(true)
                    .setUseNetwork(true)
                    .setUsePassive(true)
                    .setTimeBetweenUpdates(1 * 60 * 1000)
                    .setMetersBetweenUpdates(100);
    private RelativeLayout rl_upcoming_events;
    private TextView tv_no_upcoming_events;

    public VenueDetailsFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fragmentManager,
                                int venue_id) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
        this.venue_id = venue_id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_venue_info, container, false);
        if (isOuter == true) {
            CustomHeader.setOuterFragment(getActivity(), rl_fragments_header);
        } else {
            CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        }
        rl_fragments_header.setVisibility(View.VISIBLE);
//        GPSTracker gpsTracker = new GPSTracker(context);
//        if (gpsTracker.getIsGPSTrackingEnabled()) {
//            loadLocation();
//        } else {
//            gpsTracker.showSettingsAlert();
//        }
        init();
        GPSTracker gpsTracker = new GPSTracker(context);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            if (CheckNetwork.isInternetAvailable(context)) {
                loadLocation();
                getVenueData();
            } else {
                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
        listener();
        return rootView;
    }

    private void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rl_upcoming_events = rootView.findViewById(R.id.rl_upcoming_events);
        tv_no_upcoming_events = rootView.findViewById(R.id.tv_no_upcoming_events);
        iv_club_image = rootView.findViewById(R.id.iv_club_image);
        tv_venue_name = rootView.findViewById(R.id.tv_venue_name);
        tv_venue_time_note = rootView.findViewById(R.id.tv_venue_time_note);
        tv_open_time = rootView.findViewById(R.id.tv_open_time);
        tv_close_time = rootView.findViewById(R.id.tv_close_time);
        tv_venue_desc = rootView.findViewById(R.id.tv_venue_desc);
        tv_music_type = rootView.findViewById(R.id.tv_music_type);
        tv_crowd_type = rootView.findViewById(R.id.tv_crowd_type);
        tv_what_you_wear = rootView.findViewById(R.id.tv_what_you_wear);
        tv_what_you_cant_wear = rootView.findViewById(R.id.tv_what_you_cant_wear);
        tv_address = rootView.findViewById(R.id.tv_address);
        tv_contact = rootView.findViewById(R.id.tv_contact);
        tv_venue_email = rootView.findViewById(R.id.tv_venue_email);
        rv_upcoming_events = rootView.findViewById(R.id.rv_upcoming_events);
    }

    private void listener() {
    }

    private void getVenueData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("venues_id", venue_id);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<VenueDetails> getcall = apiInterface.getVenueDetails("Details", token, u_id, requestBody);
            getcall.enqueue(new Callback<VenueDetails>() {
                @Override
                public void onResponse(Call<VenueDetails> call, final Response<VenueDetails> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            venueDetailsData = new ArrayList<>();
                            venueDetailsData = response.body().getData();
                            tv_venue_name.setText(venueDetailsData.get(0).getVenuesName());
                            Prefs.getPrefInstance().setValue(context, Const.VENUE_NAME, venueDetailsData.get(0).getVenuesName());
                            tv_venue_time_note.setText(venueDetailsData.get(0).getVenuesTimeNote());
                            tv_open_time.setText(venueDetailsData.get(0).getVenuesStartTime());
                            tv_close_time.setText(venueDetailsData.get(0).getVenuesEndTime());
                            tv_venue_desc.setText(Html.fromHtml(venueDetailsData.get(0).getVenuesDescription()));
                            tv_music_type.setText(venueDetailsData.get(0).getVenuesMusicType());
                            tv_crowd_type.setText(venueDetailsData.get(0).getVenuesCrowdType());
                            tv_what_you_wear.setText(venueDetailsData.get(0).getVenuesClothsCanWear());
                            tv_what_you_cant_wear.setText(venueDetailsData.get(0).getVenuesClothsCantWear());
                            tv_address.setText(venueDetailsData.get(0).getVenuesAddress());
                            tv_contact.setText(venueDetailsData.get(0).getVenuesPhone());
                            tv_venue_email.setText(venueDetailsData.get(0).getVenuesEmail());
                            Prefs.getPrefInstance().setValue(context, Const.VENUE_ADDRESS, venueDetailsData.get(0).getVenuesAddress());
                            tv_contact.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + venueDetailsData.get(0).getVenuesPhone()));
                                    context.startActivity(intent);
                                }
                            });

                            Glide.with(context)
                                    .load(venueDetailsData.get(0).getVenuesImage())
                                    .placeholder(R.drawable.trans_logo)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()
                                    .into(iv_club_image);

                            LinearLayoutManager layoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                            rv_upcoming_events.setLayoutManager(layoutManager3);
                            upcomingEventData = response.body().getData().get(0).getUpcomingEvents();
                            if (upcomingEventData.size() == 0) {
                                tv_no_upcoming_events.setVisibility(View.VISIBLE);
                                rl_upcoming_events.setVisibility(View.VISIBLE);
                                rv_upcoming_events.setVisibility(View.GONE);
                            } else {
                                rl_upcoming_events.setVisibility(View.VISIBLE);
                                tv_no_upcoming_events.setVisibility(View.GONE);
                                rv_upcoming_events.setVisibility(View.VISIBLE);
                                venueUpcomingEventsAdapter = new VenueUpcomingEventsAdapter(context, upcomingEventData, fragmentManager, rl_fragments_header);
                                rv_upcoming_events.setAdapter(venueUpcomingEventsAdapter);
                            }

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VenueDetails> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

//    private void loadLocation() {
//        LocationTracker tracker = null;
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            tracker = new LocationTracker(context, settings) {
//                @Override
//                public void onLocationFound(@NonNull Location location) {
//                    Latitude = location.getLatitude();
//                    Longitude = location.getLongitude();
//                    Log.d("mytag", "lat" + location.getLatitude() + " long" + location.getLongitude());
//                }
//
//                @Override
//                public void onTimeout() {
//                }
//            };
//            tracker.startListening();
//        } else {
//            Log.d("mytag", "Not");
//
//        }
//
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(long_t));
//        String venue_name = Prefs.getPrefInstance().getValue(context, Const.VENUE_NAME, "");
//        mMap.addMarker(new MarkerOptions().position(sydney).title(venue_name));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
//
//    }

    private void loadLocation() {
        LocationTracker tracker = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            tracker = new LocationTracker(context, settings) {
                @Override
                public void onLocationFound(@NonNull Location location) {
                    Latitude = location.getLatitude();
                    Longitude = location.getLongitude();
                    Log.d("mytag", "lat" + location.getLatitude() + " long" + location.getLongitude());
                }

                @Override
                public void onTimeout() {
                }
            };
            tracker.startListening();
        } else {
            Log.d("mytag", "Not");

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Log.d("mytag", "lat - " + lat + "long - " + long_t);
        if (CheckNetwork.isInternetAvailable(context)) {
            String venue_address = Prefs.getPrefInstance().getValue(context, Const.VENUE_ADDRESS, "");
            getLatLng(venue_address);
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }

        //        mMap = googleMap;
//        LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(long_t));
//        String venue_name = Prefs.getPrefInstance().getValue(context, Const.VENUE_NAME, "");
//        mMap.addMarker(new MarkerOptions().position(sydney).title(venue_name));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    private void getLatLng(String address1) {
        Utils.getInstance().d(" Inside getLatLng");
        mMap.clear();
        Geocoder gc = new Geocoder(context);
        if (Geocoder.isPresent()) {
            List<Address> list = null;
            try {
                list = gc.getFromLocationName(address1, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (list != null && list.size() > 0) {
                Address address = list.get(0);
                LatLng destination = new LatLng(address.getLatitude(), address.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(destination);
                markerOptions.title("Destination");
                mMap.addMarker(markerOptions);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(destination));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(destination)
                        .zoom(14)
                        .bearing(90)
                        .tilt(30)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        }
    }
}

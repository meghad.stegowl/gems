package com.gems.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gems.Customview.CustomHeader;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.ShareSocial;
import com.gems.Model.SocialmediaData;
import com.gems.Model.Terms;
import com.gems.Model.TermsData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsFragment extends Fragment {
    boolean isOuter;
    ProgressDialog pd;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private AppCompatDialog mProgressDialog;
    private ArrayList<TermsData> termsData = new ArrayList<TermsData>();
    int terms_id;
    String desc;
    private TextView tv_terms_condi;

    @SuppressLint("ValidFragment")
    public TermsFragment(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_terms, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        rl_fragments_header.setVisibility(View.VISIBLE);
        RelativeLayout rl_fragment_city = rl_fragments_header.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.GONE);
        ImageView iv_fragments_insta = rl_fragments_header.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.GONE);
        ImageView iv_fragments_search = rl_fragments_header.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.GONE);
        ImageView iv_fragments_noti = rl_fragments_header.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.GONE);
        TextView tv_fragment_title = rl_fragments_header.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.VISIBLE);
        tv_fragment_title.setText("Terms & Conditions");
        init();
        return rootView;
    }


    private void init() {
        tv_terms_condi = rootView.findViewById(R.id.tv_terms_condi);
        getTermsCondition();
    }

    private void getTermsCondition() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Terms> getcall = apiInterface.getTerms();
            getcall.enqueue(new Callback<Terms>() {
                @Override
                public void onResponse(Call<Terms> call, final Response<Terms> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            termsData = response.body().getData();
                            terms_id = termsData.get(0).getTermsandconditionsId();
                            desc = termsData.get(0).getDescription();

                            tv_terms_condi.setText(Html.fromHtml("<font color='white'>" + desc + "</font>"));

//                            webView.getSettings().setJavaScriptEnabled(true);
//                            webView.setClickable(true);
//                            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//                            webView.setBackgroundColor(context.getResources().getColor(R.color.black));
//                            webView.loadDataWithBaseURL(null,desc, "text/html", "utf-8", null);
//                            webView.setWebViewClient(new WebViewClient()
//                            {
//                                public void onPageFinished(WebView view, String url)
//                                {
//                                    view.loadUrl("javascript:document.body.style.setProperty(\"color\", \"white\");"
//                                    );
//                                }
//                            });

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Terms> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

}
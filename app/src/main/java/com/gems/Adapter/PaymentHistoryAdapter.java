package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gems.Model.PaymentHistoryData;
import com.gems.R;

import java.util.ArrayList;


public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder>{

    private ArrayList<PaymentHistoryData> paymentHistoryData;
    private Context context;

    public PaymentHistoryAdapter(Context context, ArrayList<PaymentHistoryData> paymentHistoryData){
        this.context = context;
        this.paymentHistoryData = paymentHistoryData;
    }

    @Override
    public PaymentHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payment_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentHistoryAdapter.ViewHolder holder, final int position) {
        holder.tv_event_name.setText(paymentHistoryData.get(position).getUsername());
        holder.tv_venue_name.setText(String.valueOf(paymentHistoryData.get(position).getTotalCoinBuy()));
        holder.tv_ticket_amount.setText(String.valueOf(paymentHistoryData.get(position).getPerCoinAmount()));
        holder.tv_table_amount.setText(String.valueOf(paymentHistoryData.get(position).getPerCoinAmount()));
        holder.tv_date.setText(String.valueOf(paymentHistoryData.get(position).getDate()));
        holder.tv_time.setText(String.valueOf(paymentHistoryData.get(position).getTime()));
        holder.tv_no_tickets.setText(String.valueOf(paymentHistoryData.get(position).getTime()));
        holder.tv_no_table.setText(String.valueOf(paymentHistoryData.get(position).getTime()));
    }

    @Override
    public int getItemCount() {
        return paymentHistoryData.size();
    }

    public void add(ArrayList<PaymentHistoryData> songs) {
        paymentHistoryData.addAll(songs);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_event_name,tv_venue_name,tv_no_tickets,tv_ticket_amount,tv_no_table,tv_table_amount,tv_date,tv_time;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_event_name = (TextView) itemView.findViewById(R.id.tv_event_name);
            tv_venue_name = (TextView) itemView.findViewById(R.id.tv_venue_name);
            tv_no_tickets = (TextView) itemView.findViewById(R.id.tv_no_tickets);
            tv_ticket_amount = (TextView) itemView.findViewById(R.id.tv_ticket_amount);
            tv_no_table = (TextView) itemView.findViewById(R.id.tv_no_table);
            tv_table_amount = (TextView) itemView.findViewById(R.id.tv_table_amount);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
        }
    }
}

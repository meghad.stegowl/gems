package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Helper.STClass;
import com.gems.Helper.Utils;
import com.gems.Model.ContryList;
import com.gems.R;

import java.util.ArrayList;

public class CitySelectAdapter extends RecyclerView.Adapter<CitySelectAdapter.ViewHolder> {

    public ImageView cancel;
    public TextView total_price;
    private ArrayList<ContryList> arrOffer;
    private Context context;
    private TextView tv_city,tv_fragments_city;
    private AppCompatDialog dialog;
    private Boolean check;

    public CitySelectAdapter(Context context, ArrayList<ContryList> arrOffer, TextView tv_city,TextView tv_fragments_city,
                             AppCompatDialog dialog, Boolean check) {
        this.context = context;
        this.arrOffer = arrOffer;
        this.tv_city = tv_city;
        this.tv_fragments_city = tv_fragments_city;
        this.dialog = dialog;
        this.check = check;
    }

       public void addItem(ContryList offer) {
        arrOffer.add(offer);
        notifyDataSetChanged();

    }

    public ArrayList<ContryList> get_Offers() {
        return arrOffer;
    }

    public void removeItem(int i) {
        if (arrOffer != null && i >= 0 && i <= arrOffer.size()) {
            arrOffer.remove(i);
            notifyDataSetChanged();

        }
    }

    @Override
    public CitySelectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_state, parent, false);
        return new CitySelectAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CitySelectAdapter.ViewHolder holder, final int position) {
        String name = arrOffer.get(position).getName();
        holder.tv_state.setText(name);
        holder.tv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check) {
                    String name = holder.tv_state.getText().toString();
                    String selected_id = arrOffer.get(position).getId();
                    Utils.getInstance().d("In City Adapter id : " + selected_id);
                    STClass.getInstance().cityid = Integer.parseInt(selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_ID, selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, name);
                    tv_city.setText(name);
                    tv_fragments_city.setText(name);
                } else {
                    String selected_id = arrOffer.get(position).getId();
                    String name = arrOffer.get(position).getName();
                    Utils.getInstance().d("In City Adapter id : " + selected_id);
                    STClass.getInstance().cityid = Integer.parseInt(selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_ID, selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, name);
                }
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrOffer.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_state;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_state = itemView.findViewById(R.id.tv_state);
        }
    }


}
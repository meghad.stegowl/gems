package com.gems.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.LoginActivity;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.IntroData;
import com.gems.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class IntroSliderAdapter extends PagerAdapter {

    ImageView sliderImage;
    LinearLayout skip;
    TextView sliderTitle;
    CircleIndicator indicator;
    TextView next;
    LinearLayout nextContainer;
    private Context context;
    private ArrayList<IntroData> data;
    ViewPager slider;

    public IntroSliderAdapter(Context context, ArrayList<IntroData> data, ViewPager slider) {
        this.context = context;
        this.data = data;
        this.slider = slider;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_intro_slider, container, false);

        sliderImage = view.findViewById(R.id.slider_image);
        skip = view.findViewById(R.id.skip);
        sliderTitle = view.findViewById(R.id.slider_title);
        indicator = view.findViewById(R.id.indicator);
        next = view.findViewById(R.id.next);
        nextContainer = view.findViewById(R.id.next_container);

        if (position == data.size() - 1) {
            next.setText(context.getResources().getString(R.string.enter));
        } else {
            next.setText(context.getResources().getString(R.string.next));
        }

        skip.setOnClickListener(view1 -> {
            Prefs.getPrefInstance().setValue(context, Const.APP_STATUS, "1");
            Log.d("mytag", "skip IntroSlider--------------+" );
            Prefs.getPrefInstance().setValue(context, Const.USER_LOGIN_STATUS, "1");
            context.startActivity(new Intent(context, LoginActivity.class).putExtra("isLogin", true));
            ((Activity) context).finish();
        });

        nextContainer.setOnClickListener(view12 -> {
            if (position < data.size() - 1) {
                slider.setCurrentItem(position + 1);
                Log.d("mytag", "nextContainer if in IntroSlider--------------+" );
            } else {
                Log.d("mytag", "nextContainer else in IntroSlider--------------+" );
                Prefs.getPrefInstance().setValue(context, Const.APP_STATUS, "1");
                Prefs.getPrefInstance().setValue(context, Const.USER_LOGIN_STATUS, "1");
                context.startActivity(new Intent(context, LoginActivity.class).putExtra("isLogin", true));
                ((Activity) context).finish();
            }
        });

        if (data.get(0).getImage() != null && !data.get(0).getImage().isEmpty()) {
            Glide
                    .with(context)
                    .load(data.get(position).getImage())
                    .fitCenter()
                    .transition(withCrossFade())
                    .error(R.drawable.trans_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(sliderImage);
        } else {
            sliderImage.setImageResource(R.drawable.trans_logo);
        }

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NotNull ViewGroup container, int position, @NotNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}

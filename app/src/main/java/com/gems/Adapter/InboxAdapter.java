package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gems.Model.NotificationData;
import com.gems.R;

import java.util.ArrayList;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder>{

    private ArrayList<NotificationData> inboxData;
    private Context context;

    public InboxAdapter(Context context, ArrayList<NotificationData> inboxData){
        this.context = context;
        this.inboxData = inboxData;
    }

    @Override
    public InboxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inbox, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InboxAdapter.ViewHolder holder, final int position) {
        holder.tv_msg.setText(inboxData.get(position).getMessage());
        holder.tv_title.setText(inboxData.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return inboxData.size();
    }

    public void add(ArrayList<NotificationData> songs) {
        inboxData.addAll(songs);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_msg,tv_title;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_msg = (TextView) itemView.findViewById(R.id.tv_msg);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}

package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.MainActivity;
import com.gems.Fragment.HomeSlidetandUpcomingEventsDetailsFragment;
import com.gems.Model.HomeUpcomingEvents;
import com.gems.R;

import java.util.ArrayList;

public class HomeUpcomingEventsAdapter extends RecyclerView.Adapter<HomeUpcomingEventsAdapter.ViewHolder>{

    private ArrayList<HomeUpcomingEvents> upcomingEventData;
    private Context context;
    FragmentManager fragmentManager;
    RelativeLayout rl_fragments_header;
    boolean isfromHome;

    public HomeUpcomingEventsAdapter(Context context, ArrayList<HomeUpcomingEvents> upcomingEventData, FragmentManager
            fragmentManager,RelativeLayout rl_fragments_header, boolean isfromHome){
        this.context = context;
        this.upcomingEventData = upcomingEventData;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;
        this.isfromHome = isfromHome;
    }

    @Override
    public HomeUpcomingEventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_upcoming_events, parent, false);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() / 2.5);
        view.setLayoutParams(layoutParams);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeUpcomingEventsAdapter.ViewHolder holder, final int position) {
        holder.tv_event_name.setText(upcomingEventData.get(position).getEventsName());
        holder.tv_event_address.setText(upcomingEventData.get(position).getEventsType());
        holder.tv_event_city.setText(upcomingEventData.get(position).getEventsCity());
        Glide.with(context)
                .load(upcomingEventData.get(position).getEventsImage())
                .placeholder(R.drawable.app_placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.event_img);

        holder.rel_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isfromHome){
                    MainActivity.ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                    pushInnerFragment(new HomeSlidetandUpcomingEventsDetailsFragment(context, true, fragmentManager,upcomingEventData.get(position).getEventsId(),rl_fragments_header), "artistAlbum", true);
                    MainActivity.rl_main_screen.setVisibility(View.GONE);
                    MainActivity.rl_fragment.setVisibility(View.VISIBLE);
                }else {
                    pushInnerFragment(new HomeSlidetandUpcomingEventsDetailsFragment(context, false, fragmentManager, upcomingEventData.get(position).getEventsId(),  rl_fragments_header), "artistAlbum", true);
                    MainActivity.rl_main_screen.setVisibility(View.GONE);
                    MainActivity.rl_fragment.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return upcomingEventData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_event_name,tv_event_address,tv_event_city;
        public ImageView event_img;
        private RelativeLayout rel_view;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_event_name = (TextView) itemView.findViewById(R.id.tv_event_name);
            tv_event_address = (TextView) itemView.findViewById(R.id.tv_event_address);
            tv_event_city = (TextView) itemView.findViewById(R.id.tv_event_city);
            event_img = (ImageView) itemView.findViewById(R.id.event_img);
            rel_view = (RelativeLayout) itemView.findViewById(R.id.rel_view);
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

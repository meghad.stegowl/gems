package com.gems.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.MainActivity;
import com.gems.Fragment.VenueDetailsFragment;
import com.gems.Helper.CheckNetwork;
import com.gems.Model.HomeJustForYou;
import com.gems.Model.HomeVenueData;
import com.gems.R;

import java.util.ArrayList;

public class HomeVenueAdapter extends RecyclerView.Adapter<HomeVenueAdapter.ViewHolder> {

    public ImageView cancel;
    public TextView total_price;
    private ArrayList<HomeVenueData> homeVenueData;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;


    public HomeVenueAdapter(Context context, ArrayList<HomeVenueData> homeVenueData, FragmentManager fragmentManager,
                            RelativeLayout rl_fragments_header) {
        this.context = context;
        this.homeVenueData = homeVenueData;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;

    }


    @Override
    public HomeVenueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_upcoming_events, parent, false);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() / 2.5);
        view.setLayoutParams(layoutParams);
        return new HomeVenueAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeVenueAdapter.ViewHolder holder, final int position) {

        holder.event_img.post(new Runnable() {
            @Override
            public void run() {
                int width = holder.event_img.getMeasuredWidth();
                holder.event_img.setLayoutParams(new LinearLayout.LayoutParams(width, width + (width/2)));
            }
        });

        Glide.with(context)
                .load( homeVenueData.get(position).getVenuesImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.app_placeholder)
                .fitCenter()
                .into(holder.event_img);

        holder.tv_event_name.setText(homeVenueData.get(position).getVenuesName());
        holder.tv_event_address.setText(homeVenueData.get(position).getVenuesType());
        holder.tv_event_city.setText(homeVenueData.get(position).getVenuesCity());
        int venue_id = homeVenueData.get(position).getVenuesId();

        holder.rel_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new VenueDetailsFragment(context, rl_fragments_header,true,fragmentManager, venue_id), "Songs", true);
                    MainActivity.rl_main_screen.setVisibility(View.GONE);
                    MainActivity.rl_fragment.setVisibility(View.VISIBLE);
                    MainActivity.ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeVenueData.size();
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = fragmentManager;
            Log.d("mytag","in if part of the fragment manager");
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment, tag);
            if (addToBackStack) {
                ft.addToBackStack(tag);
            }
            ft.commit();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView event_img;
        public TextView tv_event_name, tv_event_address, tv_event_city;
        private RelativeLayout rel_view;

        public ViewHolder(View itemView) {
            super(itemView);

            event_img = itemView.findViewById(R.id.event_img);
            tv_event_name = itemView.findViewById(R.id.tv_event_name);
            tv_event_address = itemView.findViewById(R.id.tv_event_address);
            tv_event_city = itemView.findViewById(R.id.tv_event_city);
            rel_view = itemView.findViewById(R.id.rel_view);
        }
    }

}



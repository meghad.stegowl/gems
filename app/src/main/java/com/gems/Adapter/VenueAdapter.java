package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Fragment.VenueDetailsFragment;
import com.gems.Model.VenueData;
import com.gems.R;

import java.util.ArrayList;

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.ViewHolder> {

    private ArrayList<VenueData> venueData;
    private Context context;
    FragmentManager fragmentManager;
    RelativeLayout rl_fragments_header;

    public VenueAdapter(Context context, ArrayList<VenueData> venueData, FragmentManager fragmentManager,
                        RelativeLayout rl_fragments_header) {
        this.context = context;
        this.venueData = venueData;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Override
    public VenueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_upcoming_events, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueAdapter.ViewHolder holder, final int position) {
        holder.tv_event_name.setText(venueData.get(position).getVenuesName());
        holder.tv_event_address.setText(venueData.get(position).getVenuesType());
        holder.tv_event_city.setText(venueData.get(position).getVenuesCity());
        Glide.with(context)
                .load(venueData.get(position).getVenuesImage())
                .placeholder(R.drawable.app_placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.event_img);

        int venue_id = venueData.get(position).getVenuesId();

        holder.rel_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new VenueDetailsFragment(context, rl_fragments_header,false,
                        fragmentManager, venue_id), "Songs", true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return venueData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_event_name, tv_event_address, tv_event_city;
        public ImageView event_img;
        private RelativeLayout rel_view;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_event_name = (TextView) itemView.findViewById(R.id.tv_event_name);
            tv_event_address = (TextView) itemView.findViewById(R.id.tv_event_address);
            tv_event_city = (TextView) itemView.findViewById(R.id.tv_event_city);
            event_img = (ImageView) itemView.findViewById(R.id.event_img);
            rel_view = (RelativeLayout) itemView.findViewById(R.id.rel_view);
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

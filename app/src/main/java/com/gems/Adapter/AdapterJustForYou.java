package com.gems.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Fragment.UpcomingEventsDetailsFragment;
import com.gems.Helper.CheckNetwork;
import com.gems.Model.UpcomingEventData;
import com.gems.R;

import java.util.ArrayList;

public class AdapterJustForYou extends RecyclerView.Adapter<AdapterJustForYou.ViewHolder> {

    public ImageView cancel;
    public TextView total_price;
    private ArrayList<UpcomingEventData> upcomingEventData;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;


    public AdapterJustForYou(Context context, ArrayList<UpcomingEventData> upcomingEventData, FragmentManager fragmentManager,
                             RelativeLayout rl_fragments_header) {
        this.context = context;
        this.upcomingEventData = upcomingEventData;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;

    }

    @Override
    public AdapterJustForYou.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_upcoming_events, parent, false);
//        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
//        layoutParams.width = (int) (parent.getWidth() / 3);
//        view.setLayoutParams(layoutParams);
        return new AdapterJustForYou.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterJustForYou.ViewHolder holder, final int position) {

        holder.event_img.post(new Runnable() {
            @Override
            public void run() {
                int width = holder.event_img.getMeasuredWidth();
                holder.event_img.setLayoutParams(new LinearLayout.LayoutParams(width, width + (width/2)));
            }
        });

        Glide.with(context)
                .load( upcomingEventData.get(position).getEventsImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.app_placeholder)
                .fitCenter()
                .into(holder.event_img);

        holder.tv_event_name.setText( upcomingEventData.get(position).getEventsName());
        holder.tv_event_address.setText( upcomingEventData.get(position).getEventsType());
        holder.tv_event_city.setText( upcomingEventData.get(position).getEventsCity() );

        holder.rel_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new UpcomingEventsDetailsFragment(context,false,fragmentManager, upcomingEventData,position,
                            rl_fragments_header), "Songs", true);
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return upcomingEventData.size();
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = fragmentManager;
            Log.d("mytag","in if part of the fragment manager");
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment, tag);
            if (addToBackStack) {
                ft.addToBackStack(tag);
            }
            ft.commit();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView event_img;
        public TextView tv_event_name, tv_event_address, tv_event_city;
        private RelativeLayout rel_view;

        public ViewHolder(View itemView) {
            super(itemView);

            event_img = itemView.findViewById(R.id.event_img);
            tv_event_name = itemView.findViewById(R.id.tv_event_name);
            tv_event_address = itemView.findViewById(R.id.tv_event_address);
            tv_event_city = itemView.findViewById(R.id.tv_event_city);
            rel_view = itemView.findViewById(R.id.rel_view);
        }
    }

}



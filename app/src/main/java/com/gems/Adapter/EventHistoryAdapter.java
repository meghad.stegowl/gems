package com.gems.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Fragment.UpcomingEventsDetailsFragment;
import com.gems.Model.EventHistoryData;
import com.gems.Model.UpcomingEventData;
import com.gems.R;

import java.util.ArrayList;

public class EventHistoryAdapter extends RecyclerView.Adapter<EventHistoryAdapter.ViewHolder> {

    private ArrayList<EventHistoryData> eventHistoryData;
    private Context context;
    FragmentManager fragmentManager;
    RelativeLayout rl_fragments_header;

    public EventHistoryAdapter(Context context, ArrayList<EventHistoryData> eventHistoryData, FragmentManager fragmentManager,
                               RelativeLayout rl_fragments_header) {
        this.context = context;
        this.eventHistoryData = eventHistoryData;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Override
    public EventHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ticket_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventHistoryAdapter.ViewHolder holder, final int position) {
        holder.tv_event_name.setText(eventHistoryData.get(position).getEventsName());
        holder.tv_venue_name.setText(eventHistoryData.get(position).getEventsCity());
        holder.tv_location.setText(eventHistoryData.get(position).getEventsAddress());
        holder.tv_phone.setText(eventHistoryData.get(position).getEventsPhone());
        holder.tv_email.setText(eventHistoryData.get(position).getEventsEmail());
        if (eventHistoryData.get(position).getPerTicketAmount()== 0) {
            holder.tv_tickets.setText("Free");
        } else {
            holder.tv_tickets.setText(String.valueOf(eventHistoryData.get(position).getTotalTicketBuy()));
        }

        if (eventHistoryData.get(position).getPerTableReservationAmount()== 0) {
            holder.tv_tables.setText("Free");
        } else {
            holder.tv_tables.setText(String.valueOf(eventHistoryData.get(position).getTotalTableReservationBuy()));
        }

        holder.tv_date.setText(eventHistoryData.get(position).getEventsDate());
        holder.tv_start_time.setText(eventHistoryData.get(position).getEventsStartTime());
        holder.tv_end_time.setText(eventHistoryData.get(position).getEventsEndTime());
    }

    @Override
    public int getItemCount() {
        return eventHistoryData.size();
    }

    public void add(ArrayList<EventHistoryData> data) {
        eventHistoryData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_event_name, tv_venue_name, tv_location, tv_phone, tv_email, tv_tickets, tv_date,
                tv_start_time, tv_end_time, tv_tables;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_event_name = (TextView) itemView.findViewById(R.id.tv_event_name);
            tv_venue_name = (TextView) itemView.findViewById(R.id.tv_venue_name);
            tv_location = (TextView) itemView.findViewById(R.id.tv_location);
            tv_phone = (TextView) itemView.findViewById(R.id.tv_phone);
            tv_email = (TextView) itemView.findViewById(R.id.tv_email);
            tv_tickets = (TextView) itemView.findViewById(R.id.tv_tickets);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_start_time = (TextView) itemView.findViewById(R.id.tv_start_time);
            tv_end_time = (TextView) itemView.findViewById(R.id.tv_end_time);
            tv_tables = (TextView) itemView.findViewById(R.id.tv_tables);
        }
    }
}

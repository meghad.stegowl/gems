package com.gems.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gems.Activity.MainActivity;
import com.gems.Fragment.HomeSlidetandUpcomingEventsDetailsFragment;
import com.gems.Helper.CheckNetwork;
import com.gems.Model.HomeTopEventSlider;
import com.gems.R;

import java.util.ArrayList;

import static com.gems.Activity.MainActivity.rl_fragment;
import static com.gems.Activity.MainActivity.rl_main_screen;

public class AdapterMainviewPager extends PagerAdapter {
    public static ProgressDialog pd;
    static Context context;
    FragmentManager fm;
    View itemView;
    ArrayList<HomeTopEventSlider> imageArraylist = new ArrayList<>();
    FragmentManager fragmentManager;
    RelativeLayout rl_fragments_header;

    public AdapterMainviewPager(ArrayList<HomeTopEventSlider> imageArraylist, Context context, RelativeLayout rl_fragments_header,
                                FragmentManager fragmentManager) {
        this.context = context;
        this.imageArraylist = imageArraylist;
        this.rl_fragments_header = rl_fragments_header;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return imageArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        itemView = layoutInflater.inflate(R.layout.row_image_slider1, container, false);
        final ImageView iv_image = itemView.findViewById(R.id.iv_image);
        final TextView tv_title = itemView.findViewById(R.id.tv_title);
        final TextView tv_name = itemView.findViewById(R.id.tv_name);
        final LinearLayout ll_play = itemView.findViewById(R.id.ll_play);

        Glide.with(context)
                .load(imageArraylist.get(position).getEventsImage())
                .placeholder(R.drawable.app_placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(iv_image);

        tv_title.setText(imageArraylist.get(position).getEventsName());
//        tv_name.setMaxLines(2);
        tv_name.setText(Html.fromHtml("<font color='white'>" + imageArraylist.get(position).getEventsDescription() + "</font>"));

        ll_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageArraylist.size() > 0) {
                    if (CheckNetwork.isInternetAvailable(context)) {
                        MainActivity.ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                        pushInnerFragment(new HomeSlidetandUpcomingEventsDetailsFragment(context, true, fragmentManager,
                                imageArraylist.get(position).getEventsId(),
                                 rl_fragments_header), "Songs", true);
                        rl_main_screen.setVisibility(View.GONE);
                        rl_fragment.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(itemView, 0);
        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

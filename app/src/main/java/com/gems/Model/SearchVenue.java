package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchVenue {

    @SerializedName("venues_id")
    @Expose
    private Integer venuesId;
    @SerializedName("venues_image")
    @Expose
    private String venuesImage;
    @SerializedName("venues_name")
    @Expose
    private String venuesName;
    @SerializedName("venues_type")
    @Expose
    private String venuesType;
    @SerializedName("venues_time_note")
    @Expose
    private String venuesTimeNote;
    @SerializedName("venues_start_time")
    @Expose
    private String venuesStartTime;
    @SerializedName("venues_end_time")
    @Expose
    private String venuesEndTime;
    @SerializedName("venues_description")
    @Expose
    private String venuesDescription;
    @SerializedName("venues_music_type")
    @Expose
    private String venuesMusicType;
    @SerializedName("venues_crowd_type")
    @Expose
    private String venuesCrowdType;
    @SerializedName("venues_cloths_can_wear")
    @Expose
    private String venuesClothsCanWear;
    @SerializedName("venues_cloths_cant_wear")
    @Expose
    private String venuesClothsCantWear;
    @SerializedName("venues_address")
    @Expose
    private String venuesAddress;
    @SerializedName("venues_country")
    @Expose
    private String venuesCountry;
    @SerializedName("venues_state")
    @Expose
    private String venuesState;
    @SerializedName("venues_city")
    @Expose
    private String venuesCity;
    @SerializedName("venues_phone")
    @Expose
    private String venuesPhone;
    @SerializedName("venues_email")
    @Expose
    private String venuesEmail;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("upcoming_events")
    @Expose
    private ArrayList<UpcomingEventData> upcomingEvents = null;

    public Integer getVenuesId() {
        return venuesId;
    }

    public void setVenuesId(Integer venuesId) {
        this.venuesId = venuesId;
    }

    public String getVenuesImage() {
        return venuesImage;
    }

    public void setVenuesImage(String venuesImage) {
        this.venuesImage = venuesImage;
    }

    public String getVenuesName() {
        return venuesName;
    }

    public void setVenuesName(String venuesName) {
        this.venuesName = venuesName;
    }

    public String getVenuesType() {
        return venuesType;
    }

    public void setVenuesType(String venuesType) {
        this.venuesType = venuesType;
    }

    public String getVenuesTimeNote() {
        return venuesTimeNote;
    }

    public void setVenuesTimeNote(String venuesTimeNote) {
        this.venuesTimeNote = venuesTimeNote;
    }

    public String getVenuesStartTime() {
        return venuesStartTime;
    }

    public void setVenuesStartTime(String venuesStartTime) {
        this.venuesStartTime = venuesStartTime;
    }

    public String getVenuesEndTime() {
        return venuesEndTime;
    }

    public void setVenuesEndTime(String venuesEndTime) {
        this.venuesEndTime = venuesEndTime;
    }

    public String getVenuesDescription() {
        return venuesDescription;
    }

    public void setVenuesDescription(String venuesDescription) {
        this.venuesDescription = venuesDescription;
    }

    public String getVenuesMusicType() {
        return venuesMusicType;
    }

    public void setVenuesMusicType(String venuesMusicType) {
        this.venuesMusicType = venuesMusicType;
    }

    public String getVenuesCrowdType() {
        return venuesCrowdType;
    }

    public void setVenuesCrowdType(String venuesCrowdType) {
        this.venuesCrowdType = venuesCrowdType;
    }

    public String getVenuesClothsCanWear() {
        return venuesClothsCanWear;
    }

    public void setVenuesClothsCanWear(String venuesClothsCanWear) {
        this.venuesClothsCanWear = venuesClothsCanWear;
    }

    public String getVenuesClothsCantWear() {
        return venuesClothsCantWear;
    }

    public void setVenuesClothsCantWear(String venuesClothsCantWear) {
        this.venuesClothsCantWear = venuesClothsCantWear;
    }

    public String getVenuesAddress() {
        return venuesAddress;
    }

    public void setVenuesAddress(String venuesAddress) {
        this.venuesAddress = venuesAddress;
    }

    public String getVenuesCountry() {
        return venuesCountry;
    }

    public void setVenuesCountry(String venuesCountry) {
        this.venuesCountry = venuesCountry;
    }

    public String getVenuesState() {
        return venuesState;
    }

    public void setVenuesState(String venuesState) {
        this.venuesState = venuesState;
    }

    public String getVenuesCity() {
        return venuesCity;
    }

    public void setVenuesCity(String venuesCity) {
        this.venuesCity = venuesCity;
    }

    public String getVenuesPhone() {
        return venuesPhone;
    }

    public void setVenuesPhone(String venuesPhone) {
        this.venuesPhone = venuesPhone;
    }

    public String getVenuesEmail() {
        return venuesEmail;
    }

    public void setVenuesEmail(String venuesEmail) {
        this.venuesEmail = venuesEmail;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public ArrayList<UpcomingEventData> getUpcomingEvents() {
        return upcomingEvents;
    }

    public void setUpcomingEvents(ArrayList<UpcomingEventData> upcomingEvents) {
        this.upcomingEvents = upcomingEvents;
    }

}

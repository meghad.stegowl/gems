package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SerachEvent {

    @SerializedName("events_id")
    @Expose
    private Integer eventsId;
    @SerializedName("venues_id")
    @Expose
    private Integer venuesId;
    @SerializedName("venues_name")
    @Expose
    private String venuesName;
    @SerializedName("events_image")
    @Expose
    private String eventsImage;
    @SerializedName("events_image2")
    @Expose
    private String eventsImage2;
    @SerializedName("events_image3")
    @Expose
    private String eventsImage3;
    @SerializedName("events_image4")
    @Expose
    private String eventsImage4;
    @SerializedName("events_image5")
    @Expose
    private String eventsImage5;
    @SerializedName("events_name")
    @Expose
    private String eventsName;
    @SerializedName("events_type")
    @Expose
    private String eventsType;
    @SerializedName("events_title")
    @Expose
    private String eventsTitle;
    @SerializedName("events_description")
    @Expose
    private String eventsDescription;
    @SerializedName("events_date")
    @Expose
    private String eventsDate;
    @SerializedName("events_start_time")
    @Expose
    private String eventsStartTime;
    @SerializedName("events_end_time")
    @Expose
    private String eventsEndTime;
    @SerializedName("events_address")
    @Expose
    private String eventsAddress;
    @SerializedName("events_country")
    @Expose
    private String eventsCountry;
    @SerializedName("events_state")
    @Expose
    private String eventsState;
    @SerializedName("events_city")
    @Expose
    private String eventsCity;
    @SerializedName("events_zipcode")
    @Expose
    private String eventsZipcode;
    @SerializedName("events_phone")
    @Expose
    private String eventsPhone;
    @SerializedName("events_email")
    @Expose
    private String eventsEmail;
    @SerializedName("events_more_info_url")
    @Expose
    private String eventsMoreInfoUrl;
    @SerializedName("event_fee_type")
    @Expose
    private String eventFeeType;
    @SerializedName("event_ticket_amount")
    @Expose
    private String eventTicketAmount;
    @SerializedName("event_total_ticket")
    @Expose
    private String eventTotalTicket;
    @SerializedName("event_ticket_instruction")
    @Expose
    private String eventTicketInstruction;
    @SerializedName("event_reservation_fee_type")
    @Expose
    private String eventReservationFeeType;
    @SerializedName("event_reservation_amount")
    @Expose
    private String eventReservationAmount;
    @SerializedName("event_total_reservation_table")
    @Expose
    private String eventTotalReservationTable;
    @SerializedName("event_reservation_instruction")
    @Expose
    private String eventReservationInstruction;
    @SerializedName("event_spotlight_status")
    @Expose
    private Boolean eventSpotlightStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getEventsId() {
        return eventsId;
    }

    public void setEventsId(Integer eventsId) {
        this.eventsId = eventsId;
    }

    public Integer getVenuesId() {
        return venuesId;
    }

    public void setVenuesId(Integer venuesId) {
        this.venuesId = venuesId;
    }

    public String getVenuesName() {
        return venuesName;
    }

    public void setVenuesName(String venuesName) {
        this.venuesName = venuesName;
    }

    public String getEventsImage() {
        return eventsImage;
    }

    public void setEventsImage(String eventsImage) {
        this.eventsImage = eventsImage;
    }

    public String getEventsImage2() {
        return eventsImage2;
    }

    public void setEventsImage2(String eventsImage2) {
        this.eventsImage2 = eventsImage2;
    }

    public String getEventsImage3() {
        return eventsImage3;
    }

    public void setEventsImage3(String eventsImage3) {
        this.eventsImage3 = eventsImage3;
    }

    public String getEventsImage4() {
        return eventsImage4;
    }

    public void setEventsImage4(String eventsImage4) {
        this.eventsImage4 = eventsImage4;
    }

    public String getEventsImage5() {
        return eventsImage5;
    }

    public void setEventsImage5(String eventsImage5) {
        this.eventsImage5 = eventsImage5;
    }

    public String getEventsName() {
        return eventsName;
    }

    public void setEventsName(String eventsName) {
        this.eventsName = eventsName;
    }

    public String getEventsType() {
        return eventsType;
    }

    public void setEventsType(String eventsType) {
        this.eventsType = eventsType;
    }

    public String getEventsTitle() {
        return eventsTitle;
    }

    public void setEventsTitle(String eventsTitle) {
        this.eventsTitle = eventsTitle;
    }

    public String getEventsDescription() {
        return eventsDescription;
    }

    public void setEventsDescription(String eventsDescription) {
        this.eventsDescription = eventsDescription;
    }

    public String getEventsDate() {
        return eventsDate;
    }

    public void setEventsDate(String eventsDate) {
        this.eventsDate = eventsDate;
    }

    public String getEventsStartTime() {
        return eventsStartTime;
    }

    public void setEventsStartTime(String eventsStartTime) {
        this.eventsStartTime = eventsStartTime;
    }

    public String getEventsEndTime() {
        return eventsEndTime;
    }

    public void setEventsEndTime(String eventsEndTime) {
        this.eventsEndTime = eventsEndTime;
    }

    public String getEventsAddress() {
        return eventsAddress;
    }

    public void setEventsAddress(String eventsAddress) {
        this.eventsAddress = eventsAddress;
    }

    public String getEventsCountry() {
        return eventsCountry;
    }

    public void setEventsCountry(String eventsCountry) {
        this.eventsCountry = eventsCountry;
    }

    public String getEventsState() {
        return eventsState;
    }

    public void setEventsState(String eventsState) {
        this.eventsState = eventsState;
    }

    public String getEventsCity() {
        return eventsCity;
    }

    public void setEventsCity(String eventsCity) {
        this.eventsCity = eventsCity;
    }

    public String getEventsZipcode() {
        return eventsZipcode;
    }

    public void setEventsZipcode(String eventsZipcode) {
        this.eventsZipcode = eventsZipcode;
    }

    public String getEventsPhone() {
        return eventsPhone;
    }

    public void setEventsPhone(String eventsPhone) {
        this.eventsPhone = eventsPhone;
    }

    public String getEventsEmail() {
        return eventsEmail;
    }

    public void setEventsEmail(String eventsEmail) {
        this.eventsEmail = eventsEmail;
    }

    public String getEventsMoreInfoUrl() {
        return eventsMoreInfoUrl;
    }

    public void setEventsMoreInfoUrl(String eventsMoreInfoUrl) {
        this.eventsMoreInfoUrl = eventsMoreInfoUrl;
    }

    public String getEventFeeType() {
        return eventFeeType;
    }

    public void setEventFeeType(String eventFeeType) {
        this.eventFeeType = eventFeeType;
    }

    public String getEventTicketAmount() {
        return eventTicketAmount;
    }

    public void setEventTicketAmount(String eventTicketAmount) {
        this.eventTicketAmount = eventTicketAmount;
    }

    public String getEventTotalTicket() {
        return eventTotalTicket;
    }

    public void setEventTotalTicket(String eventTotalTicket) {
        this.eventTotalTicket = eventTotalTicket;
    }

    public String getEventTicketInstruction() {
        return eventTicketInstruction;
    }

    public void setEventTicketInstruction(String eventTicketInstruction) {
        this.eventTicketInstruction = eventTicketInstruction;
    }

    public String getEventReservationFeeType() {
        return eventReservationFeeType;
    }

    public void setEventReservationFeeType(String eventReservationFeeType) {
        this.eventReservationFeeType = eventReservationFeeType;
    }

    public String getEventReservationAmount() {
        return eventReservationAmount;
    }

    public void setEventReservationAmount(String eventReservationAmount) {
        this.eventReservationAmount = eventReservationAmount;
    }

    public String getEventTotalReservationTable() {
        return eventTotalReservationTable;
    }

    public void setEventTotalReservationTable(String eventTotalReservationTable) {
        this.eventTotalReservationTable = eventTotalReservationTable;
    }

    public String getEventReservationInstruction() {
        return eventReservationInstruction;
    }

    public void setEventReservationInstruction(String eventReservationInstruction) {
        this.eventReservationInstruction = eventReservationInstruction;
    }

    public Boolean getEventSpotlightStatus() {
        return eventSpotlightStatus;
    }

    public void setEventSpotlightStatus(Boolean eventSpotlightStatus) {
        this.eventSpotlightStatus = eventSpotlightStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}

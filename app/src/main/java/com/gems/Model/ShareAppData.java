package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShareAppData {

    @SerializedName("shareourapp_id")
    @Expose
    private Integer shareourappId;
    @SerializedName("android_link")
    @Expose
    private String androidLink;
    @SerializedName("ios_link")
    @Expose
    private String iosLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getShareourappId() {
        return shareourappId;
    }

    public void setShareourappId(Integer shareourappId) {
        this.shareourappId = shareourappId;
    }

    public String getAndroidLink() {
        return androidLink;
    }

    public void setAndroidLink(String androidLink) {
        this.androidLink = androidLink;
    }

    public String getIosLink() {
        return iosLink;
    }

    public void setIosLink(String iosLink) {
        this.iosLink = iosLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}

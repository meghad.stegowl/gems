package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentHistoryData {
    @SerializedName("paymentinfo_id")
    @Expose
    private Integer paymentinfoId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("total_coin_buy")
    @Expose
    private Integer totalCoinBuy;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("per_coin_amount")
    @Expose
    private String perCoinAmount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    public Integer getPaymentinfoId() {
        return paymentinfoId;
    }

    public void setPaymentinfoId(Integer paymentinfoId) {
        this.paymentinfoId = paymentinfoId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getTotalCoinBuy() {
        return totalCoinBuy;
    }

    public void setTotalCoinBuy(Integer totalCoinBuy) {
        this.totalCoinBuy = totalCoinBuy;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPerCoinAmount() {
        return perCoinAmount;
    }

    public void setPerCoinAmount(String perCoinAmount) {
        this.perCoinAmount = perCoinAmount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

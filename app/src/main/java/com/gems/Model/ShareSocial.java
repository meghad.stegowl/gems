package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ShareSocial {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("socialmedia_data")
    @Expose
    private ArrayList<SocialmediaData> socialmediaData = null;
    @SerializedName("shareourapp_data")
    @Expose
    private ArrayList<ShareAppData> shareourappData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SocialmediaData> getSocialmediaData() {
        return socialmediaData;
    }

    public void setSocialmediaData(ArrayList<SocialmediaData> socialmediaData) {
        this.socialmediaData = socialmediaData;
    }

    public ArrayList<ShareAppData> getShareourappData() {
        return shareourappData;
    }

    public void setShareourappData(ArrayList<ShareAppData> shareourappData) {
        this.shareourappData = shareourappData;
    }
}

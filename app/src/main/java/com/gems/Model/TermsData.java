package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsData {

    @SerializedName("termsandconditions_id")
    @Expose
    private Integer termsandconditionsId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getTermsandconditionsId() {
        return termsandconditionsId;
    }

    public void setTermsandconditionsId(Integer termsandconditionsId) {
        this.termsandconditionsId = termsandconditionsId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}

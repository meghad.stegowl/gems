package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Home {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("top_events")
    @Expose
    private ArrayList<HomeTopEventSlider> topEvents = null;
    @SerializedName("upcoming_events")
    @Expose
    private ArrayList<HomeUpcomingEvents> upcomingEvents = null;
    @SerializedName("venues_data")
    @Expose
    private ArrayList<HomeVenueData> venuesData = null;
    @SerializedName("just_for_you")
    @Expose
    private ArrayList<UpcomingEventData> justForYou = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<HomeTopEventSlider> getTopEvents() {
        return topEvents;
    }

    public void setTopEvents(ArrayList<HomeTopEventSlider> topEvents) {
        this.topEvents = topEvents;
    }

    public ArrayList<HomeUpcomingEvents> getUpcomingEvents() {
        return upcomingEvents;
    }

    public void setUpcomingEvents(ArrayList<HomeUpcomingEvents> upcomingEvents) {
        this.upcomingEvents = upcomingEvents;
    }

    public ArrayList<HomeVenueData> getVenuesData() {
        return venuesData;
    }

    public void setVenuesData(ArrayList<HomeVenueData> venuesData) {
        this.venuesData = venuesData;
    }

    public ArrayList<UpcomingEventData> getJustForYou() {
        return justForYou;
    }

    public void setJustForYou(ArrayList<UpcomingEventData> justForYou) {
        this.justForYou = justForYou;
    }
}

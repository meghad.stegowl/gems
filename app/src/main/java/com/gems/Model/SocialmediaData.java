package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialmediaData {

    @SerializedName("socialmedia_id")
    @Expose
    private Integer socialmediaId;
    @SerializedName("facebook_link")
    @Expose
    private String facebookLink;
    @SerializedName("twitter_link")
    @Expose
    private String twitterLink;
    @SerializedName("instagram_link")
    @Expose
    private String instagramLink;
    @SerializedName("youtube_link")
    @Expose
    private String youtubeLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getSocialmediaId() {
        return socialmediaId;
    }

    public void setSocialmediaId(Integer socialmediaId) {
        this.socialmediaId = socialmediaId;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}

package com.gems.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Search {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("events_data")
    @Expose
    private ArrayList<UpcomingEventData> eventsData = null;
    @SerializedName("venues_data")
    @Expose
    private ArrayList<SearchVenue> venuesData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<UpcomingEventData> getEventsData() {
        return eventsData;
    }

    public void setEventsData(ArrayList<UpcomingEventData> eventsData) {
        this.eventsData = eventsData;
    }

    public ArrayList<SearchVenue> getVenuesData() {
        return venuesData;
    }

    public void setVenuesData(ArrayList<SearchVenue> venuesData) {
        this.venuesData = venuesData;
    }
}

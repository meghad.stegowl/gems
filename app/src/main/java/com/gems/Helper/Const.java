package com.gems.Helper;


public class Const {
    public static final String APP_STATUS = "appstatus";
    public static final String PREFS_FILENAME = "Mansa";
    public static final String DEVICE_TYPE = "Android";
    public static final String FCM = "fcm";
    public static final String ANDROID_ID = "android_id";
    public static final String ACCESS_TOKEN = "token";
    public static final String SKIP_USER_ID = "skip_user_id";
    public static final String USER_ID = "user_id";
    public static final String USER_LOGIN_STATUS = "user_login_status";
    public static final String SET_PROFILE_IMAGE = "profile_image";
    public static final String SKIP_STATUS = "skip_status";
    public static final String VENUE_NAME = "venue_name";
    public static final String VENUE_ADDRESS = "venue_address";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String KEEP_USER_LOGGED_IN = "keepUserLoggedIn";
    public static final String ADVANCE_AMOUNT = "advance_amount";
    public static final String RESERVE_AMOUNT = "reserve_amount";
    public static final String TOTAL_AMOUNT = "total_amount";
    public static final String ADVANCE_QUANTITY = "advance_qua";
    public static final String RESERVE_QUANTITY = "reserve_qua";
    public static final String NEW_APP_STATUS = "newappstatus";

    public static final String FIRST_TIME_LOGIN_CITY_ID = "first_city_id";
    public static final String FIRST_TIME_LOGIN_CITY_NAME = "first_city_name";
    public static final String SIGN_UP_CITY_NAME = "signup_State_name";

    public static final String FIRST_TIME_SKIP_CITY_ID = "first_login_city_id";
    public static final String FIRST_TIME_SKIP_CITY_NAME = "first_skip_city_name";

    public static final String FB = "facebook_Link";
    public static final String INSTA = "instagram_Link";
    public static final String TWIT = "twitter_Link";
    public static final String YOUTUBE = "youtube_Link";


    public static final String HOST_MANSA = "http://54.227.185.135/gems/api/";
    public static final String UPLOAD_IMAGE = HOST_MANSA+"uploads";
    public static final String GET_STATES = HOST_MANSA + "statesandcities";
    public static final String GET_CITIES = HOST_MANSA + "statesandcities";
}

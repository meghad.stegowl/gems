package com.gems.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.gems.Gems;
import com.gems.R;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AppUtil {


    public static void Go_To_This(Context context, String social_handle, String name, String id) {
        String package_Name;
        String app_Link;
        String app_URL;

    }

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null) {
            return false;
        } else {
            if (info.isConnected()) {
                return true;
            } else {
                return true;
            }
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public static void setup_Fragment(FragmentManager fragmentManager, Fragment fragment, String path, String type, String title, String id, String tag, String backStack, boolean isAdd) {
        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        bundle.putString("type", type);
        bundle.putString("title", title);
        bundle.putString("id", id);
        fragment.setArguments(bundle);
        //change
//        if (isAdd)
//            fragmentManager.beginTransaction().add(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
//        else
//            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
    }


    public static void setup_Fragment_with_bundle(FragmentManager fragmentManager, Fragment fragment, String tag, String backStack, boolean isAdd) {
        //change
//        if (isAdd)
//            fragmentManager.beginTransaction().add(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
//        else
//            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
    }

    public static int getDisplayWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return (displayMetrics.widthPixels > displayMetrics.heightPixels) ? displayMetrics.heightPixels : displayMetrics.widthPixels;
    }

    public static File getAppDirectory(Context context) {
        /* Checks if external storage is available for read and write */
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state)) {
//            Log.e(TAG, "Application Directory is in External Drive");
//        File appFolder = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.app_name));
        File appFolder = new File(Gems.applicationContext.getFilesDir(), context.getResources().getString(R.string.app_name));
        if (!appFolder.mkdirs()) {
            appFolder.mkdirs();
        }
        return appFolder;
//        }
//        else {
//            Log.e(TAG, "Application Directory is in Internal Drive");
//            File appFolder = new File(context.getFilesDir(), context.getResources().getString(R.string.app_name));
//            if (!appFolder.mkdirs()) {
//                Log.e(TAG, "Application Directory is not created");
//                appFolder.mkdirs();
//            }
//            return appFolder;
//        }
    }

    public static int getDisplayHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static boolean checkIsTablet(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        float widthInches = metrics.widthPixels / metrics.xdpi;
        float heightInches = metrics.heightPixels / metrics.ydpi;
        double diagonalInches = Math.sqrt(Math.pow(widthInches, 2) + Math.pow(heightInches, 2));
        return diagonalInches >= 7.0;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(int px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = px / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public boolean isConnected(Context context) {
        //
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static void showHashKey(Context context, String packageName) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo( packageName
                    , PackageManager.GET_SIGNATURES); //Your package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    //                            // Check HomeScreen is foreground activity or not.
//                            if(((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity.getShortClassName().equals(".activities.HomeScreen"))
}
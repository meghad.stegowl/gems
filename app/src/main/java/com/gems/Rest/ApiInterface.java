package com.gems.Rest;

import com.gems.Model.CommonResponse;
import com.gems.Model.EliteMembership;
import com.gems.Model.EventDetails;
import com.gems.Model.EventHistory;
import com.gems.Model.GetProfile;
import com.gems.Model.Home;
import com.gems.Model.ImageUploadResponse;
import com.gems.Model.Intro;
import com.gems.Model.PaymentHistory;
import com.gems.Model.PaymentHistoryData;
import com.gems.Model.Search;
import com.gems.Model.ShareSocial;
import com.gems.Model.Login;
import com.gems.Model.Notification;
import com.gems.Model.Skip;
import com.gems.Model.State;
import com.gems.Model.Terms;
import com.gems.Model.UpcomingEvent;
import com.gems.Model.Venue;
import com.gems.Model.VenueDetails;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("login")
    Call<Login> getLogin(@Body RequestBody requestBody);

    @POST("skip")
    Call<Skip> getSkip(@Body RequestBody requestBody);

    @GET("notifications")
    Call<Notification> getNotification(@Header("authorization") String token, @Header("userid") String uid, @Query("limit") int limit,
                                       @Query("page") int page);

    @GET("statesandcities")
    Call<State> getStateCity(@Query("type") String Requestor);

    @POST("register")
    Call<CommonResponse> getSignUp(@Body RequestBody requestBody);

    @GET("termsandconditions")
    Call<Terms> getTerms();

    @POST("forgotpassword")
    Call<CommonResponse> getForgotPassword(@Body RequestBody requestBody);

    @POST("changepassword")
    Call<CommonResponse> getChangePassword(@Header("authorization") String token, @Header("userid") String uid, @Body RequestBody requestBody);

    @GET("cms")
    Call<ShareSocial> getContactShare(@Header("authorization") String token, @Header("userid") String uid);

    @GET("logout")
    Call<CommonResponse> getLogout(@Header("authorization") String token, @Header("userid") String uid);

    @POST("profile")
    Call<GetProfile> getProfile(@Query("type") String Requestor, @Header("authorization") String token, @Header("userid") String uid);

    @POST("profile")
    Call<GetProfile> updateProfile(@Query("type") String Requestor,@Header("authorization") String token, @Header("userid") String uid,
                                   @Body RequestBody requestBody );

    @POST("uploads")
    @Multipart
    Call<ImageUploadResponse> UploadImage(@Part MultipartBody.Part fileToUpload);

    @POST("events")
    Call<UpcomingEvent> getEventData(@Query("type") String Requestor, @Header("authorization") String token, @Header("userid") String uid
            , @Query("limit") int limit,@Query("page") int page);

    @POST("events")
    Call<EventDetails> getEventDetails(@Query("type") String Requestor, @Header("authorization") String token,
                                       @Header("userid") String uid, @Body RequestBody requestBody );

    @POST("homepage")
    Call<Home> getHomeallData(@Header("authorization") String token, @Header("userid") String uid);

    @POST("homepage")
    Call<Home> getHomeData(@Query("state") String Requestor, @Header("authorization") String token, @Header("userid") String uid);

    @POST("venues")
    Call<Venue> getVenueData(@Query("type") String Requestor, @Header("authorization") String token, @Header("userid") String uid
           /* , @Query("limit") int limit, @Query("page") int page*/);

    @POST("venues")
    Call<Venue> getVenueWithState(@Query("type") String Requestor,@Query("state") String state, @Header("authorization") String token, @Header("userid") String uid
            /* , @Query("limit") int limit, @Query("page") int page*/);

    @POST("venues")
    Call<VenueDetails> getVenueDetails(@Query("type") String Requestor, @Header("authorization") String token, @Header("userid") String uid,
                                       @Body RequestBody requestBody);

    @POST("search")
    Call<Search> getSearch(@Header("authorization") String token, @Header("userid") String uid,@Body RequestBody requestBody);

    @GET("elitemembership")
    Call<EliteMembership> getEliteMembership(@Header("authorization") String token, @Header("userid") String uid);

    @GET("introductionslider")
    Call<Intro> getIntroSlider();

    @POST("bookings")
    Call<CommonResponse> getBookTicketTable(@Query("type") String Requestor,@Header("authorization") String token, @Header("userid") String uid,
                                            @Body RequestBody requestBody);

    @POST("bookings")
    Call<EventHistory> getEventHistory(@Query("type") String Requestor, @Header("authorization") String token,
                                       @Header("userid") String uid, @Query("limit") int limit, @Query("page") int page);

    @POST("bookings")
    Call<PaymentHistory> getPaymentHistory(@Query("type") String Requestor, @Header("authorization") String token,
                                           @Header("userid") String uid, @Query("limit") int limit, @Query("page") int page);


}

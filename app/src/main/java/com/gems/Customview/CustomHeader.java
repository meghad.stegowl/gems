package com.gems.Customview;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gems.Activity.MainActivity;
import com.gems.R;

import static com.gems.Activity.MainActivity.homeSelectedothernonSelected;
import static com.gems.Activity.MainActivity.ll_bottom_menu_buttons;
import static com.gems.Activity.MainActivity.rl_main_screen;
import static com.gems.Activity.MainActivity.withTitleHeader;

public class CustomHeader {

    public static void setOuterFragment(final Activity activity, RelativeLayout relativeLayout) {
        ImageView back_btn = relativeLayout.findViewById(R.id.iv_fragments_menu);
        back_btn.setVisibility(View.VISIBLE);
        relativeLayout.setTag("Outer");
//        MainActivity.ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
        back_btn.setImageResource(R.drawable.selector_back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                withTitleHeader(relativeLayout);
                homeSelectedothernonSelected();
                activity.onBackPressed();
                ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                if (rl_main_screen.getVisibility() == View.VISIBLE) {
                    homeSelectedothernonSelected();
                }
            }
        });
    }

    public static void setInnerFragment(final Activity activity, final RelativeLayout relativeLayout) {
        final ImageView back_btn = relativeLayout.findViewById(R.id.iv_fragments_menu);
        back_btn.setImageResource(R.drawable.selector_back);
        back_btn.setVisibility(View.VISIBLE);
        relativeLayout.setTag("Inner");
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (relativeLayout.getTag().equals("Inner")) {
                    withTitleHeader(relativeLayout);
                    activity.onBackPressed();
                    if (rl_main_screen.getVisibility() == View.VISIBLE) {
                        homeSelectedothernonSelected();
                    }
                } else {
                    back_btn.setBackgroundResource(R.drawable.selector_back);
                }
            }
        });
    }
}

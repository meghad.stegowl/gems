package com.gems.FCM;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.gems.Activity.MainActivity;
import com.gems.Activity.SplashActivity;
import com.gems.Gems;
import com.gems.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String NOTIFICATION_CHANNEL_ID = "10011";
    private static final String TAG = "mytag";
    String notiType;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getData());

            notiType = remoteMessage.getData().get("notiType");
            if (notiType != null && notiType.equals("2")) {
                Fragment fragment = MainActivity.fm.findFragmentById(R.id.fragment);
//                if (fragment instanceof FragmentChatRoom) {
//                    if (FragmentChatRoom.req_id.equals(remoteMessage.getData().get("other_id"))) {
//                        FragmentChatRoom.updateChat();
//                    } else {
//                        showNotification(remoteMessage.getData().get("body"), remoteMessage.getData().get("name"));
//                    }
//                } else if (fragment instanceof FragmentContact) {
//                    FragmentContact.updateContact();
//                    showNotification(remoteMessage.getData().get("body"), remoteMessage.getData().get("name"));
//
//                } else {
                    showNotification(remoteMessage.getData().get("body"), remoteMessage.getData().get("name"));
//                }
            } else {
                showNotification(remoteMessage.getData().get("body"), Gems.applicationContext.getResources().getString(R.string.app_name));
            }
        }
    }

    @SuppressLint("WrongConstant")
    private void showNotification(String message, String title) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.setFlags(Notification.FLAG_AUTO_CANCEL);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, 0);
        mBuilder.setSmallIcon(R.drawable.trans_logo);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(message);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.trans_logo);
        mBuilder.setLargeIcon(icon);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, Gems.applicationContext.getResources().getString(R.string.app_name), importance);
            notificationChannel.setDescription(Gems.applicationContext.getResources().getString(R.string.app_name));
            notificationChannel.setName("Android");

            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(1 /* Request Code */, mBuilder.build());
    }
}

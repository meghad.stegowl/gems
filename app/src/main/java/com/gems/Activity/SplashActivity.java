package com.gems.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    private String token, android_id;
    public static final String device = "Android";
    Context context;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    token = Objects.requireNonNull(task.getResult()).getToken();
                    Log.d("mytag", "Refreshed token: " + token);
                    Prefs.getPrefInstance().setValue(this, Const.FCM, token);
                    Prefs.getPrefInstance().setValue(this, Const.DEVICE_TYPE, device);
                    android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    Prefs.getPrefInstance().setValue(this, Const.ANDROID_ID, android_id);
                    Log.d("mytag", "u id: " + android_id);
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (Prefs.getPrefInstance().getValue(context, Const.APP_STATUS, "").equals("")) {
                        Intent i = new Intent(context, IntroSlider.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    } else {
                        String skip_status = Prefs.getPrefInstance().getValue(SplashActivity.this, Const.SKIP_STATUS, "");
                        if (skip_status.equals("1")) {
                            Intent i = new Intent(context, MainActivity.class);
                            startActivity(i);
                            finish();
                        } else if (skip_status.equals("0")) {
                            Intent i = new Intent(context, MainActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("No Internet Available");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        }, 3000);
    }
}
package com.gems.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;

import com.gems.Helper.CheckNetwork;
import com.gems.Model.Terms;
import com.gems.Model.TermsData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsActivity extends AppCompatActivity {

    WebView webView;
    LinearLayout btn_back;
    AppCompatDialog mProgressDialog;
    Context context;
    private ArrayList<TermsData> termsData = new ArrayList<TermsData>();
    int terms_id;
    String desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        context = TermsActivity.this;
        init();
        listener();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    private void init() {
        webView = findViewById(R.id.webView);
        btn_back = findViewById(R.id.btn_back);
        getTermsCondition();
    }

    private void listener() {

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TermsActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getTermsCondition() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Terms> getcall = apiInterface.getTerms();
            getcall.enqueue(new Callback<Terms>() {
                @Override
                public void onResponse(Call<Terms> call, final Response<Terms> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            termsData = response.body().getData();
                            terms_id = termsData.get(0).getTermsandconditionsId();
                            desc = termsData.get(0).getDescription();

                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.setClickable(true);
                            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                            webView.setBackgroundColor(context.getResources().getColor(R.color.black));
                            webView.loadDataWithBaseURL(null,desc, "text/html", "utf-8", null);
                            webView.setWebViewClient(new WebViewClient()
                            {
                                public void onPageFinished(WebView view, String url)
                                {
                                    view.loadUrl("javascript:document.body.style.setProperty(\"color\", \"white\");"
                                    );
                                }
                            });

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Terms> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

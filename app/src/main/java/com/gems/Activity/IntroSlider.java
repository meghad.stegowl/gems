package com.gems.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import com.gems.Adapter.IntroSliderAdapter;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.Intro;
import com.gems.Model.IntroData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gems.Helper.AppUtil.setWindowFlag;

public class IntroSlider extends AppCompatActivity {

    ViewPager slider;
    CircleIndicator indicator;
    private ArrayList<IntroData> introData = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    void setWatchNow() {
        Prefs.getPrefInstance().setValue(IntroSlider.this, Const.APP_STATUS, "1");
        Log.d("mytag", "IntroSlider is in IntroSlider--------------+" );
        startActivity(new Intent(IntroSlider.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_slider);
        getIntroSlider();

        // Make Full Screen - Hide StatusBar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Make UI Full Screen
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        // Set StatusBar Color Transparent
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        slider = findViewById(R.id.slider);
        indicator = findViewById(R.id.indicator);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewCompat.setOnApplyWindowInsetsListener(getWindow().getDecorView(), (view, insets) ->
                ViewCompat.onApplyWindowInsets(getWindow().getDecorView(),
                        insets.replaceSystemWindowInsets(insets.getSystemWindowInsetLeft(), 0,
                                insets.getSystemWindowInsetRight(), insets.getSystemWindowInsetBottom()))
        );
    }

    private void getIntroSlider() {
        if (CheckNetwork.isInternetAvailable(IntroSlider.this)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Intro> getcall = apiInterface.getIntroSlider();
            getcall.enqueue(new Callback<Intro>() {
                @Override
                public void onResponse(Call<Intro> call, final Response<Intro> response) {
                    if (response != null && response.isSuccessful()) {
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        introData = new ArrayList<>();
                        introData = response.body().getData();
                        if (status == 200) {
                            if(response.body().getData() != null && !response.body().getData().isEmpty()) {
                                slider.setAdapter(new IntroSliderAdapter(IntroSlider.this, response.body().getData(), slider));
                                indicator.setViewPager(slider);
                            } else {
                                setWatchNow();
                            }
                        } else if (status == 422) {
                            setWatchNow();
                            Toast.makeText(IntroSlider.this, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            setWatchNow();
                            Toast.makeText(IntroSlider.this, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            setWatchNow();
                            Toast.makeText(IntroSlider.this, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            setWatchNow();
                            Toast.makeText(IntroSlider.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(IntroSlider.this, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Intro> call, Throwable t) {
                    t.printStackTrace();
                    setWatchNow();
                }
            });
        } else {
            Toast.makeText(IntroSlider.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.gems.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.Login;
import com.gems.Model.LoginData;
import com.gems.Model.Skip;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;
import com.google.android.material.checkbox.MaterialCheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView tv_privacy, tv_terms, tv_skip;
    private LinearLayout btn_back;
    private TextView tv_forgot_pwd;
    private Button btn_login, btn_create_pro;
    AppCompatDialog mProgressDialog;
    Context context;
    ArrayList<LoginData> loginData = new ArrayList<>();
    private EditText edt_user_name, edt_pwd;
    String userName, password;
    private ImageView iv_pwd;
    MaterialCheckBox remember_me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        init();
        listener();
    }

    private void init() {
        tv_forgot_pwd = findViewById(R.id.tv_forgot_pwd);
        btn_back = findViewById(R.id.btn_back);
        tv_privacy = findViewById(R.id.tv_privacy);
        tv_skip = findViewById(R.id.tv_skip);
        tv_terms = findViewById(R.id.tv_terms);
        btn_login = findViewById(R.id.btn_login);
        btn_create_pro = findViewById(R.id.btn_create_pro);
        edt_user_name = findViewById(R.id.edt_user_name);
        edt_pwd = findViewById(R.id.edt_pwd);
        iv_pwd = findViewById(R.id.iv_pwd);
        remember_me = findViewById(R.id.remember_me);

        if (Prefs.getPrefInstance().getValue(LoginActivity.this, Const.KEEP_USER_LOGGED_IN, "").equals("1")) {
            edt_user_name.setText(Prefs.getPrefInstance().getValue(LoginActivity.this, Const.USERNAME, ""));
            edt_pwd.setText(Prefs.getPrefInstance().getValue(LoginActivity.this, Const.PASSWORD, ""));
            remember_me.setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        alertDialog.setMessage("Are you sure want to exit?");
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void validateData() {
        userName = edt_user_name.getText().toString().trim();
        password = edt_pwd.getText().toString().trim();

        if (userName.trim().length() != 0) {
            if (password.trim().length() != 0) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    Login(userName, password);
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            } else {
                edt_pwd.setError("Please Enter Password");
                edt_pwd.requestFocus();
            }
        } else {
            edt_user_name.setError("Please Enter Username");
            edt_user_name.requestFocus();
        }
    }

    private void listener() {
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKip();
            }
        });

        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, PrivacyPolicyActivity.class);
                startActivity(i);
                finish();
            }
        });

        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, TermsActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SplashActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });

        tv_forgot_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_create_pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                finish();
            }
        });

        iv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_pwd.isSelected()) {
                    iv_pwd.setSelected(false);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic_bold.ttf");
                    edt_pwd.setTypeface(type);
                } else {
                    iv_pwd.setSelected(true);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic_bold.ttf");
                    edt_pwd.setTypeface(type);
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void Login(String username, String password) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String gcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");

            try {
                jsonObject.put("login_id", username);
                jsonObject.put("password", password);
                jsonObject.put("fcm_id", gcm_id);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Login> getcall = apiInterface.getLogin(requestBody);
            getcall.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> getcall, Response<Login> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        String token = response.body().getAuthtoken();
                        if (status == 200) {
                            if (remember_me.isChecked()) {
                                Prefs.getPrefInstance().setValue(LoginActivity.this, Const.PASSWORD, password);
                                Prefs.getPrefInstance().setValue(LoginActivity.this, Const.KEEP_USER_LOGGED_IN, "1");
                            } else {
                                Prefs.getPrefInstance().setValue(LoginActivity.this, Const.PASSWORD, "");
                                Prefs.getPrefInstance().setValue(LoginActivity.this, Const.KEEP_USER_LOGGED_IN, "0");
                            }

                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.USER_LOGIN_STATUS, "1");
                            loginData = response.body().getData();
                            int user_id = loginData.get(0).getUserId();
                            String image = loginData.get(0).getImage();
                            String username = loginData.get(0).getUsername();
                            String name = loginData.get(0).getName();
                            String email = loginData.get(0).getEmail();
                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.USERNAME, username);
                            String state = loginData.get(0).getState();
                            String city = loginData.get(0).getCity();
                            String zipcode = loginData.get(0).getZipcode();
                            String phone = loginData.get(0).getPhone();
                            String fcmId = loginData.get(0).getFcmId();
                            Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, state);
                            Prefs.getPrefInstance().setValue(context, Const.ACCESS_TOKEN, token);
                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, String.valueOf(user_id));
                            clearAllEdditText();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setMessage(msg);
                            builder1.setCancelable(true);
                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.SKIP_STATUS, "1");
                                            Intent i = new Intent(context, MainActivity.class);
                                            startActivity(i);
                                            finish();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Login> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void SKip() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String gcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");
            String android_id = Prefs.getPrefInstance().getValue(context, Const.ANDROID_ID, "");

            try {
                jsonObject.put("unique_id", android_id);
                jsonObject.put("fcm_id", gcm_id);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Skip> getcall = apiInterface.getSkip(requestBody);
            getcall.enqueue(new Callback<Skip>() {
                @Override
                public void onResponse(Call<Skip> getcall, Response<Skip> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        String token = response.body().getAuthtoken();
                        if (status == 200) {
                            int skip_user_id = response.body().getUserId();
                            String fcmId = response.body().getFcmId();
                            Intent i = new Intent(context, MainActivity.class);
                            startActivity(i);
                            finish();
                            Prefs.getPrefInstance().setValue(context, Const.ACCESS_TOKEN, token);
                            Prefs.getPrefInstance().setValue(context, Const.USER_ID, String.valueOf(skip_user_id));
                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.SKIP_STATUS, "0");
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Skip> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearAllEdditText() {
        edt_user_name.requestFocus();
        edt_user_name.getText().clear();
        edt_pwd.getText().clear();
    }
}
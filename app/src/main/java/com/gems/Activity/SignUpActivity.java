package com.gems.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.CommonResponse;
import com.gems.Model.ContryList;
import com.gems.Model.ImageUploadResponse;
import com.gems.Model.State;
import com.gems.Model.StateData;
import com.gems.Model.StateList;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;
import com.gems.Rest.WebInterface;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    LinearLayout btn_back;
    AppCompatDialog mProgressDialog;
    Context context;
    private EditText edt_first_name, edt_last_name, edt_user_name, edt_email, edt_phone, edt_pwd, edt_confirm_pwd, edt_zip;
    private Spinner spn_state, spn_city;
    private String items_country_id, items_city_name, items_state_id, items_state_name;
    String city, state;
    String firstName, lastName, userName, email, password, confirm_password, phone, zipcode;
    private ArrayList<StateList> stateLists = new ArrayList<>();
    private ArrayList<ContryList> contryLists = new ArrayList<>();
    private ArrayList<StateData> stateData = new ArrayList<StateData>();
    private String[] items_country;
    private String[] items_state;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    String emailptrn = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private String showpicturedialog_title, selectfromgallerymsg, selectfromcameramsg, canceldialog;
    private final int GALLERY = 2;
    private final int CAMERA = 1;
    private Uri resultUri;
    private Bitmap bitmap;
    private CircleImageView iv_profileiimage;
    private RelativeLayout rl_city;
    private Button btn_login;
    private FrameLayout fm_profile;
    private String lastChar = " ";

    private ImageView iv_pwd, iv_confirm_pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        context = SignUpActivity.this;
        requestMultiplePermissions();
        showpicturedialog_title = "Select the Action";
        selectfromgallerymsg = "Select photo from Gallery";
        selectfromcameramsg = "Capture photo from Camera";
        canceldialog = "Cancel";
        init();
        listener();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void init() {
        btn_back = findViewById(R.id.btn_back);
        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_user_name = findViewById(R.id.edt_user_name);
        edt_email = findViewById(R.id.edt_email);
        edt_phone = findViewById(R.id.edt_phone);
        edt_pwd = findViewById(R.id.edt_pwd);
        edt_confirm_pwd = findViewById(R.id.edt_confirm_pwd);
        edt_zip = findViewById(R.id.edt_zip);
        spn_state = findViewById(R.id.spn_state);
        spn_city = findViewById(R.id.spn_city);
        iv_profileiimage = findViewById(R.id.iv_profileiimage);
        rl_city = findViewById(R.id.rl_city);
        btn_login = findViewById(R.id.btn_login);
        fm_profile = findViewById(R.id.fm_profile);
        iv_pwd = findViewById(R.id.iv_pwd);
        iv_confirm_pwd = findViewById(R.id.iv_confirm_pwd);
        getStateData();
        ArrayAdapter<String> spanishAdapter = new ArrayAdapter<String>(SignUpActivity.this,
                R.layout.row_city_signup, R.id.tv_state);
    }

    private void listener() {

        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = edt_phone.getText().toString().length();
                if (digits > 1)
                    lastChar = edt_phone.getText().toString().substring(digits - 1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = edt_phone.getText().toString().length();
                if (!lastChar.equals("-")) {
                    if (digits == 3 || digits == 7) {
                        edt_phone.append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        iv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_pwd.isSelected()) {
                    iv_pwd.setSelected(false);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic.ttf");
                    edt_pwd.setTypeface(type);
                } else {
                    iv_pwd.setSelected(true);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic.ttf");
                    edt_pwd.setTypeface(type);
                }
            }
        });

        iv_confirm_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_confirm_pwd.isSelected()) {
                    iv_confirm_pwd.setSelected(false);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_confirm_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic.ttf");
                    edt_confirm_pwd.setTypeface(type);
                } else {
                    iv_confirm_pwd.setSelected(true);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_confirm_pwd.setTextSize(15);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/century_gothic.ttf");
                    edt_confirm_pwd.setTypeface(type);
                }
            }
        });

        fm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showPictureDialog_chooser();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                items_state_id = stateLists.get(pos).getId();
                items_state_name = stateLists.get(pos).getName();
                getCityData(items_state_id);
                Log.d("okhttp", "Select state id and city : " + items_state_id + stateLists.get(pos).getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spn_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (contryLists != null) {
                    items_country_id = contryLists.get(pos).getId();
                    items_city_name = contryLists.get(pos).getName();
                    Log.d("okhttp", "Select Country id and city : " + items_country_id + contryLists.get(pos).getName());
                } else {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this,
                            R.style.MyAlertDialogStyle);
                    alertDialogBuilder.setMessage("No Cities Available");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    alertDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }


    private void validateData() {
        firstName = edt_first_name.getText().toString().trim();
        lastName = edt_last_name.getText().toString().trim();
        userName = edt_user_name.getText().toString().trim();
//        phone = edt_phone.getText().toString().trim();
        phone = edt_phone.getText().toString().replace("-", "");
        password = edt_pwd.getText().toString().trim();
        confirm_password = edt_confirm_pwd.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        zipcode = edt_zip.getText().toString().trim();
        try {
            city = spn_city.getSelectedItem().toString();
            state = spn_state.getSelectedItem().toString();
        } catch (Exception e) {

        }

        if (firstName.trim().length() != 0) {
            if (lastName.trim().length() != 0) {
                if (userName.trim().length() != 0) {
                    if (email.trim().length() != 0) {
                        if (email.trim().matches(emailPattern) || email.trim().matches(emailptrn)) {
                            if (password.trim().length() != 0) {
                                if (confirm_password.trim().length() != 0) {
                                    if (CheckNetwork.isInternetAvailable(context)) {
                                        if (edt_pwd.getText().toString().equals(edt_confirm_pwd.getText().toString())) {
//                                            if (!state.equals("") && !state.toUpperCase().equals("PREFERRED STATE / PROVINCE")) {
//                                                if (!city.equals("") && !city.toUpperCase().equals("PREFERRED CITY")) {
                                                    if (zipcode.trim().length() != 0) {
                                                        if (phone.trim().length() != 0) {
                                                            if (isValidofficephone(phone)) {
                                                                if (resultUri == null) {
                                                                    registerUser(Prefs.getPrefInstance().getValue(context, Const.SET_PROFILE_IMAGE, ""));
                                                                } else {
                                                                    Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, (Activity) context)), resultUri);
                                                                }
                                                            } else {
                                                                edt_phone.setError("Please Enter Valid Phone");
                                                                edt_phone.requestFocus();
                                                            }
                                                        } else {
                                                            edt_phone.setError("Please Enter Phone Number");
                                                            edt_phone.requestFocus();
                                                        }
                                                    } else {
                                                        edt_zip.setError("Please Enter ZipCode");
                                                        edt_zip.requestFocus();
                                                    }
//                                                } else {
//                                                    Toast.makeText(context, "Please select Preferred City.", Toast.LENGTH_SHORT).show();
//                                                }
//                                            } else {
//                                                Toast.makeText(context, "Please select Preferred State.", Toast.LENGTH_SHORT).show();
//                                            }
                                        } else {
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                                            builder1.setMessage("Password does not match");
                                            builder1.setCancelable(true);

                                            builder1.setNegativeButton(
                                                    "Ok",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });

                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    } else {
                                        Toast.makeText(context, "Sin conexión a Internet", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    edt_confirm_pwd.setError("Please Enter Confirm Password");
                                    edt_confirm_pwd.requestFocus();
                                }
                            } else {
                                edt_pwd.setError("Please Enter Password");
                                edt_pwd.requestFocus();
                            }
                        } else {
                            edt_email.setError("Please Enter Valid E-mail");
                            edt_email.requestFocus();
                        }
                    } else {
                        edt_email.setError("Please Enter E-mail");
                        edt_email.requestFocus();
                    }
                } else {
                    edt_user_name.setError("Please Enter UserName");
                    edt_user_name.requestFocus();
                }
            } else {
                edt_last_name.setError("Please Enter LastName");
                edt_last_name.requestFocus();
            }
        } else {
            edt_first_name.setError("Please Enter FirstName");
            edt_first_name.requestFocus();
        }
    }

    private boolean isValidofficephone(String bphone) {
        return TextUtils.isEmpty(bphone) || (!TextUtils.isEmpty(bphone) && bphone.length() == 10 &&
                TextUtils.isDigitsOnly(bphone));
    }

    private void registerUser(String url) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String gcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");
            try {
                jsonObject.put("email", edt_email.getText().toString());
                jsonObject.put("username", edt_user_name.getText().toString());
                jsonObject.put("password", edt_pwd.getText().toString());
                jsonObject.put("confirm_password", edt_confirm_pwd.getText().toString());
                jsonObject.put("firstname", edt_first_name.getText().toString());
                jsonObject.put("lastname", edt_last_name.getText().toString());
                jsonObject.put("image", url);
                jsonObject.put("state", items_state_name);
                jsonObject.put("city", spn_city.getSelectedItem().toString());
                jsonObject.put("state_id", items_state_id);
                jsonObject.put("city_id", items_country_id);
                jsonObject.put("zipcode", edt_zip.getText().toString());
                jsonObject.put("phone", edt_phone.getText().toString());
                jsonObject.put("fcm_id", gcm_id);
                Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_ID, items_state_id);
                Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, items_state_name);
                Prefs.getPrefInstance().setValue(context, Const.SIGN_UP_CITY_NAME, items_state_name);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommonResponse> getcall = apiInterface.getSignUp(requestBody);
            getcall.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> getcall, Response<CommonResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            clearAllEdditText();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setMessage(msg);
                            builder1.setCancelable(true);
                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent loginIntent = new Intent(context, LoginActivity.class);
                                            startActivity(loginIntent);
                                            finish();
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearAllEdditText() {
        edt_first_name.requestFocus();
        edt_first_name.getText().clear();
        edt_last_name.getText().clear();
        edt_user_name.getText().clear();
        edt_pwd.getText().clear();
        edt_confirm_pwd.getText().clear();
        edt_zip.getText().clear();
    }

    private void getStateData1() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<State> getcall = apiInterface.getStateCity("states");
            getcall.enqueue(new Callback<State>() {
                @Override
                public void onResponse(Call<State> call, final Response<State> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            stateData = response.body().getData();
                            if (stateData.size() > 0) {
                                items_state = new String[stateData.size() + 1];
                                items_state[0] = "Preferred Province / State";
                                stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                                for (int i = 0; i < stateData.size(); i++) {
                                    String name = stateData.get(i).getName();
                                    items_state[i + 1] = name;
                                }
                            } else {
                                items_state = new String[1];
                                items_state[0] = "Preferred Province / State";
                                stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                            }
                            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.row_city, R.id.tv_state,
                                    items_state);
                            spn_state.setAdapter(stateAdapter);
                            stateAdapter.notifyDataSetChanged();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<State> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getStateData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            stateLists = new ArrayList<StateList>();
            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        response = WebInterface.getInstance().doGet(Const.GET_STATES + "?type=" + "states");
                        Log.d("okhttp", "response is---------" + jsonObject.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("okhttp", "State Response : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
//                        JSONObject data = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray.length() > 0) {
                            items_state = new String[jsonArray.length() + 1];
                            items_state[0] = "Preferred Province / State";
                            stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject country_data = jsonArray.getJSONObject(i);
                                String name = country_data.getString("name");
                                String state_id = country_data.getString("id");
                                String country_id = country_data.getString("country_id");
                                items_state[i + 1] = name;
                                stateLists.add(new StateList(state_id, name, country_id));
                            }
                        } else {
                            items_state = new String[1];
                            items_state[0] = "Preferred Province / State";
                            stateLists.add(new StateList("0", "Preferred Province / State", "0"));
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.row_city, R.id.tv_state,
                                items_state);
                        spn_state.setAdapter(stateAdapter);
                        stateAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(SignUpActivity.this, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getCityData(final String state_id) {
        if (CheckNetwork.isInternetAvailable(context)) {
            contryLists = new ArrayList<ContryList>();
            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        response = WebInterface.getInstance().doGet(Const.GET_CITIES + "?type=" + "cities" + "&state_id=" + state_id);
//                        response = WebInterface.getInstance().doPostRequest(Const.GET_CITIES + "/" + state_id, jsonObject.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("okhttp", "City Response : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        if (jsonArray.length() > 0) {
                            items_country = new String[jsonArray.length() + 1];
                            items_country[0] = "Preferred City";
                            contryLists.add(new ContryList("0", "Preferred City", "0"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject country_data = jsonArray.getJSONObject(i);
                                String name = country_data.getString("name");
                                String id = country_data.getString("id");
                                String state_id = country_data.getString("state_id");
                                items_country[i + 1] = name;
                                contryLists.add(new ContryList(id, name, state_id));
                            }
                        } else {
                            items_country = new String[1];
                            items_country[0] = "No Cities available";
                            contryLists.add(new ContryList("0", "No Cities available", "0"));
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(context, R.layout.row_city, R.id.tv_state, items_country);
                        spn_city.setAdapter(countryAdapter);
                        countryAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity((Activity) context)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(context, "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(context, "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showPictureDialog_chooser() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(context);
        pictureDialog.setTitle(showpicturedialog_title);
        String[] pictureDialogItems = {selectfromgallerymsg, selectfromcameramsg, canceldialog};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        startActivityForResult(intent, CAMERA);
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    CropImage(contentURI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    Log.d("mytag", "f=temp");
                    break;
                }
            }

            try {
                CropImage(Uri.fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleUCropResult(data);
        }
    }

    private void CropImage(Uri contentURI) throws IOException {
        Uri destinationUri = Uri.fromFile(new File(context.getCacheDir(), "temp.jpg"));
        UCrop.Options options = new UCrop.Options();
//      options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));

        UCrop.of(contentURI, destinationUri)
                .withOptions(options)
                .start(this);
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        try {
            resultUri = UCrop.getOutput(data);
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);
            Log.d("mytag", "my bitmap iss-" + bitmap);
//             Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            iv_profileiimage.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void Uploadimagetoserver(File file, final Uri resultUri) {
        Log.d("mytag", "Uploadimagetoserver profile data" + resultUri);
        if (CheckNetwork.isInternetAvailable(context)) {
            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
            String fcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");
            Log.d("mytag", "access token" + token);
            Log.d("mytag", "user id" + u_id);
            Log.d("mytag", "fcm id" + fcm_id);

            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            final RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ImageUploadResponse> file_upload = apiInterface.UploadImage(fileToUpload);
            file_upload.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {

                    mProgressDialog.dismiss();
                    if (response.isSuccessful() && response != null) {
                        int status = response.body().getStatus();
                        if (status == 200) {
                            try {
                                iv_profileiimage.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(),
                                        resultUri));
                                Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, response.body().getUrl());
                                registerUser(response.body().getUrl());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if (response.body().getStatus() == 404) {
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            if (resultUri == null) {
                                iv_profileiimage.setImageResource(R.drawable.trans_logo);
                            }
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });


        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }


}
package com.gems.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gems.Adapter.AdapterMainviewPager;
import com.gems.Adapter.CitySelectAdapter;
import com.gems.Adapter.HomeAdapterJustForYou;
import com.gems.Adapter.HomeUpcomingEventsAdapter;
import com.gems.Adapter.HomeVenueAdapter;
import com.gems.Customview.ClickableViewPager;
import com.gems.Fragment.HomeBottomEventsFragment;
import com.gems.Fragment.InstagramFragment;
import com.gems.Fragment.JustForYouFragment;
import com.gems.Fragment.MoreFragment;
import com.gems.Fragment.NotificationFragment;
import com.gems.Fragment.ProfileFragment;
import com.gems.Fragment.SearchFragment1;
import com.gems.Fragment.UpcomingEventsFragment;
import com.gems.Fragment.VenueFragment;
import com.gems.Gems;
import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.DividerItemDecorator;
import com.gems.Helper.Prefs;
import com.gems.Helper.STClass;
import com.gems.Helper.Utils;
import com.gems.Model.ContryList;
import com.gems.Model.Home;
import com.gems.Model.HomeTopEventSlider;
import com.gems.Model.HomeUpcomingEvents;
import com.gems.Model.HomeVenueData;
import com.gems.Model.StateList;
import com.gems.Model.UpcomingEventData;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;
import com.gems.Rest.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "mytag";
    public boolean fromHome = false;
    public static FragmentManager fm;
    private boolean isFragmentLoaded = false;
    static Context context;
    private static final Object progressTimerSync = new Object();
    private static final Object sync = new Object();
    private static Timer progressTimer = null;

    private ImageView iv_menu, iv_fragments_menu, iv_insta, iv_fragments_insta, iv_noti, iv_fragments_noti, iv_search, iv_fragments_search;
    public static RelativeLayout rl_main_screen, rl_fragment, rl_fragments_header;

    private LinearLayout ll_home, ll_events, ll_profile;
    private static ImageView iv_home, iv_events, iv_profile;
    private static TextView tv_home, tv_events, tv_profile, tv_fragment_title;

    private TextView tv_viewall_upcoming_events, tv_viewall_venue, tv_viewall_just_for_you;
    private TextView tv_no_upcoming_events, tv_no_venue, tv_no_just_for_you;
    private RelativeLayout rl_upcoming_events, rl_venue, rl_just_for_you, rl_viewPager;
    private RecyclerView rv_upcoming_events, rv_listof_venue, rv_just_for_you;
    HomeAdapterJustForYou adapterJustForYou;
    HomeUpcomingEventsAdapter homeUpcomingEventsAdapter;
    HomeVenueAdapter homeVenueAdapter;

    private AppCompatDialog mProgressDialog;
    private ArrayList<HomeTopEventSlider> homeTopEventSliders = new ArrayList<>();
    private ArrayList<HomeUpcomingEvents> homeUpcomingEvents = new ArrayList<>();
    private ArrayList<HomeVenueData> homeVenueData = new ArrayList<>();
    private ArrayList<UpcomingEventData> upcomingEventData = new ArrayList<>();
    private static ClickableViewPager clickableViewPager;
    private static TextView tv_city, tv_fragments_city;
    private RelativeLayout rl_city, rl_fragment_city, rl_event_venue_slider, rl_no_all_data_found;

    static AdapterMainviewPager adapter;
    CircleIndicator indicator;

    private String[] items_state;
    private ArrayList<StateList> stateLists = new ArrayList<>();

    private static ArrayList<ContryList> contryLists = new ArrayList<>();
    public static RelativeLayout ll_bottom_menu_buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        init();
        listener();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= 23) {
            getPermission();
        }
        rl_event_venue_slider = findViewById(R.id.rl_event_venue_slider);
        rl_no_all_data_found = findViewById(R.id.rl_no_all_data_found);
        ll_bottom_menu_buttons = findViewById(R.id.ll_bottom_menu_buttons);
        tv_city = findViewById(R.id.tv_city);
        tv_fragments_city = findViewById(R.id.tv_fragments_city);
        rl_city = findViewById(R.id.rl_city);
        rl_fragment_city = findViewById(R.id.rl_fragment_city);

        ll_home = findViewById(R.id.ll_home);
        ll_events = findViewById(R.id.ll_events);
        ll_profile = findViewById(R.id.ll_profile);
        iv_home = findViewById(R.id.iv_home);
        iv_events = findViewById(R.id.iv_events);
        iv_profile = findViewById(R.id.iv_profile);
        tv_home = findViewById(R.id.tv_home);
        tv_events = findViewById(R.id.tv_events);
        tv_profile = findViewById(R.id.tv_profile);
        tv_fragment_title = findViewById(R.id.tv_fragment_title);
        clickableViewPager = findViewById(R.id.clickableViewPager);
        indicator = findViewById(R.id.indicator);

        tv_viewall_upcoming_events = findViewById(R.id.tv_viewall_upcoming_events);
        tv_viewall_venue = findViewById(R.id.tv_viewall_venue);
        tv_viewall_just_for_you = findViewById(R.id.tv_viewall_just_for_you);

        rl_viewPager = findViewById(R.id.rl_viewPager);
        rl_upcoming_events = findViewById(R.id.rl_upcoming_events);
        rl_venue = findViewById(R.id.rl_venue);
        rl_just_for_you = findViewById(R.id.rl_just_for_you);
        tv_no_upcoming_events = findViewById(R.id.tv_no_upcoming_events);
        tv_no_venue = findViewById(R.id.tv_no_venue);
        tv_no_just_for_you = findViewById(R.id.tv_no_just_for_you);

        rv_upcoming_events = findViewById(R.id.rv_upcoming_events);
        rv_listof_venue = findViewById(R.id.rv_listof_venue);
        rv_just_for_you = findViewById(R.id.rv_just_for_you);

        iv_menu = findViewById(R.id.iv_menu);
        iv_fragments_menu = findViewById(R.id.iv_fragments_menu);
        iv_insta = findViewById(R.id.iv_insta);
        iv_fragments_insta = findViewById(R.id.iv_fragments_insta);
        iv_noti = findViewById(R.id.iv_noti);
        iv_fragments_noti = findViewById(R.id.iv_fragments_noti);
        iv_search = findViewById(R.id.iv_search);
        iv_fragments_search = findViewById(R.id.iv_fragments_search);
        rl_main_screen = findViewById(R.id.rl_main_screen);
        rl_fragment = findViewById(R.id.rl_fragment);
        rl_fragments_header = findViewById(R.id.rl_fragments_header);
        if (rl_main_screen.getVisibility() == View.VISIBLE) {
            ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
            homeSelectedothernonSelected();
        }
    }

    private void listener() {
        ll_home.setOnClickListener(this::onClick);
        ll_events.setOnClickListener(this::onClick);
        ll_profile.setOnClickListener(this::onClick);
        iv_menu.setOnClickListener(this::onClick);
        iv_fragments_menu.setOnClickListener(this::onClick);
        iv_insta.setOnClickListener(this::onClick);
        iv_fragments_insta.setOnClickListener(this::onClick);
        iv_noti.setOnClickListener(this::onClick);
        iv_fragments_noti.setOnClickListener(this::onClick);
        iv_search.setOnClickListener(this::onClick);
        iv_fragments_search.setOnClickListener(this::onClick);
        tv_viewall_venue.setOnClickListener(this::onClick);
        homeSelectedothernonSelected();

        String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        if (skip_status.equals("0")) {
//            Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_NAME);
//            Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_ID);
            Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "All");
            tv_city.setText("Select City");
            tv_fragments_city.setText("Select City");
        }


        Utils.getInstance().d("Fisrt time city name : " + Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, ""));
        Utils.getInstance().d("Fisrt time city Id : " + Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_ID, ""));

        String state_name = Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "");
        Log.d("mytag", "here is state name===========" + state_name);
        if (state_name.equals("All")) {
            getHomeallData();
        } else if (state_name.equals("")) {
            Log.d("mytag", "here is in if else part===========" + state_name);
            getHomeallData();
        } else {
            Log.d("mytag", "here is in if part===========" + state_name);
            getHomeData(Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, ""));
        }

        if (Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "").equals("")) {
            Log.d("mytag", "here is iinifff name===========" + Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "").equals(""));
            tv_city.setText("Select City");
            tv_fragments_city.setText("Select City");
        } else {
            Log.d("mytag", "here is iinielseeeeee name===========" + Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "").equals(""));
            tv_city.setText(Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, ""));
            tv_fragments_city.setText(Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, ""));
        }


        rl_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    ShowCityPopup();
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rl_fragment_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    ShowCityPopup();
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public void ShowCityPopup() {
        if (CheckNetwork.isInternetAvailable(context)) {
            final AppCompatDialog dialog;
            dialog = new AppCompatDialog(context, R.style.dialogFullScreen);
            dialog.setContentView(R.layout.custom_city_popup);
            dialog.show();

            final TextView tv_city_all = dialog.findViewById(R.id.tv_city_all);
            final RecyclerView rv_citylist = dialog.findViewById(R.id.rv_citylist);
            rv_citylist.setLayoutManager(new LinearLayoutManager(context));
            contryLists = new ArrayList<ContryList>();

            ImageView iv_close_comment = dialog.findViewById(R.id.del);
            iv_close_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            tv_city_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String selected_id = "0";
                    String name = tv_city_all.getText().toString();
                    Utils.getInstance().d("In City Adapter id : " + selected_id);
                    STClass.getInstance().cityid = Integer.parseInt(selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_ID, selected_id);
                    Prefs.getPrefInstance().setValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, name);
                    tv_city.setText(name);
                    tv_fragments_city.setText(name);
                    getHomeallData();
                    dialog.dismiss();
                }
            });

            if (CheckNetwork.isInternetAvailable(context)) {
                new AsyncTask<Void, Void, String>() {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(Void... voids) {
                        String response = null;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            response = WebInterface.getInstance().doGet(Const.GET_STATES + "?type=" + "states");
                            Log.d("okhttp", "response is---------" + jsonObject.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("okhttp", "State Response : " + response);
                        return response;
                    }

                    @Override
                    protected void onPostExecute(String response) {
                        super.onPostExecute(response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            items_state = new String[jsonArray.length()];
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject country_data = jsonArray.getJSONObject(i);
                                String name = country_data.getString("name");
                                String id = country_data.getString("id");
                                items_state[i] = name;
                                contryLists.add(new ContryList(id, name));
                            }
                            CitySelectAdapter cityAdapter = new CitySelectAdapter(context, contryLists, tv_city, tv_fragments_city, dialog, true);
                            rv_citylist.setAdapter(cityAdapter);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.execute();
            } else {
                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
            }

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    homeTopEventSliders = new ArrayList<>();
                    homeUpcomingEvents = new ArrayList<>();
                    homeVenueData = new ArrayList<>();
                    upcomingEventData = new ArrayList<>();
                    Utils.getInstance().d("in dialog dismiss listener id : " + STClass.getInstance().cityid);
                    Utils.getInstance().d("in dialog dismiss listener cityname : " + STClass.getInstance().cityname);
                    String state_name = Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, "");
                    if (state_name.equals("All")) {
//                        getHomeallData();
                    } else {
                        getHomeData(Prefs.getPrefInstance().getValue(context, Const.FIRST_TIME_LOGIN_CITY_NAME, ""));
                    }

                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_menu:
                fromHome = true;
                ll_bottom_menu_buttons.setVisibility(View.GONE);
                pushFragment(new MoreFragment(context, rl_fragments_header, getSupportFragmentManager()), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;


            case R.id.iv_fragments_menu:
                rl_main_screen.setVisibility(View.VISIBLE);
                rl_fragment.setVisibility(View.GONE);
                break;

            case R.id.iv_insta:
                fromHome = true;
                pushFragment(new InstagramFragment(context, rl_fragments_header, true), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_fragments_insta:
                fromHome = true;
                pushFragment(new InstagramFragment(context, rl_fragments_header, true), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_noti:
                fromHome = true;
                pushFragment(new NotificationFragment(context, rl_fragments_header, true), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_fragments_noti:
                fromHome = true;
                pushFragment(new NotificationFragment(context, rl_fragments_header, true), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_search:
                fromHome = true;
                pushFragment(new SearchFragment1(context, rl_fragments_header, true, getSupportFragmentManager()), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_fragments_search:
                fromHome = true;
                pushFragment(new SearchFragment1(context, rl_fragments_header, true, getSupportFragmentManager()), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;

            case R.id.ll_home:
                fromHome = true;
                iv_home.setSelected(true);
                iv_events.setSelected(false);
                iv_profile.setSelected(false);
                tv_home.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_events.setTextColor(getResources().getColor(R.color.white));
                tv_profile.setTextColor(getResources().getColor(R.color.white));
                rl_main_screen.setVisibility(View.VISIBLE);
                rl_fragment.setVisibility(View.GONE);
                ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                break;

            case R.id.ll_events:
                fromHome = true;
                rl_fragment_city.setVisibility(View.VISIBLE);
                iv_fragments_insta.setVisibility(View.VISIBLE);
                iv_fragments_search.setVisibility(View.VISIBLE);
                iv_fragments_noti.setVisibility(View.VISIBLE);
                tv_fragment_title.setVisibility(View.GONE);
                iv_home.setSelected(false);
                iv_events.setSelected(true);
                iv_profile.setSelected(false);
                tv_home.setTextColor(getResources().getColor(R.color.white));
                tv_events.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_profile.setTextColor(getResources().getColor(R.color.white));
                pushFragment(new HomeBottomEventsFragment(context, rl_fragments_header, true, getSupportFragmentManager()), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                break;

            case R.id.ll_profile:
                fromHome = true;
                iv_home.setSelected(false);
                iv_events.setSelected(false);
                iv_profile.setSelected(true);
                tv_home.setTextColor(getResources().getColor(R.color.white));
                tv_events.setTextColor(getResources().getColor(R.color.white));
                tv_profile.setTextColor(getResources().getColor(R.color.colorPrimary));
                String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                if (skip_status.equals("0")) {
//                    Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_NAME);
//                    Prefs.getPrefInstance().remove(context, Const.FIRST_TIME_LOGIN_CITY_ID);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Please login to continue.");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface d, int which) {
                                    d.dismiss();
                                    Intent i = new Intent(context, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    pushFragment(new ProfileFragment(context, rl_fragments_header, true, getSupportFragmentManager()), "more", false);
                    rl_main_screen.setVisibility(View.GONE);
                    rl_fragment.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.tv_viewall_venue:
                fromHome = true;
                pushFragment(new VenueFragment(context, rl_fragments_header, true, getSupportFragmentManager()), "more", false);
                rl_main_screen.setVisibility(View.GONE);
                rl_fragment.setVisibility(View.VISIBLE);
                break;
        }
    }

    public static void homeSelectedothernonSelected() {
        if (rl_main_screen.getVisibility() == View.VISIBLE) {
            ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
        }
        iv_home.setSelected(true);
        iv_events.setSelected(false);
        iv_profile.setSelected(false);
        tv_home.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        tv_events.setTextColor(context.getResources().getColor(R.color.white));
        tv_profile.setTextColor(context.getResources().getColor(R.color.white));
    }

    public static void withTitleHeader(RelativeLayout relativeLayout) {
        RelativeLayout rl_fragment_city = relativeLayout.findViewById(R.id.rl_fragment_city);
        rl_fragment_city.setVisibility(View.VISIBLE);
        ImageView iv_fragments_insta = relativeLayout.findViewById(R.id.iv_fragments_insta);
        iv_fragments_insta.setVisibility(View.VISIBLE);
        ImageView iv_fragments_search = relativeLayout.findViewById(R.id.iv_fragments_search);
        iv_fragments_search.setVisibility(View.VISIBLE);
        ImageView iv_fragments_noti = relativeLayout.findViewById(R.id.iv_fragments_noti);
        iv_fragments_noti.setVisibility(View.VISIBLE);
        TextView tv_fragment_title = relativeLayout.findViewById(R.id.tv_fragment_title);
        tv_fragment_title.setVisibility(View.GONE);
    }

    public void getPermission() {
        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int permissionCheckLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((permissionCheckCamera == -1) || (permissionCheckRead == -1) || (permissionCheckWrite == -1) || (permissionCheckLocation == -1)) {
                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 2909);
                }
            }
        }
    }

    private void pushFragment(Fragment fragment, String tag, boolean addToBackStack) {
        isFragmentLoaded = true;
        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (rl_main_screen.getVisibility() == View.VISIBLE) {
            ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
            androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
            alertDialog.setTitle(getResources().getString(R.string.app_name));
            alertDialog.setMessage("Are you sure want to exit?");
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Prefs.getPrefInstance().setValue(MainActivity.this, Const.USER_LOGIN_STATUS, "0");
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        } else if (rl_fragments_header.getTag().equals("Outer")) {
            ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
            rl_main_screen.setVisibility(View.VISIBLE);
            rl_fragment.setVisibility(View.GONE);
        } else if (rl_fragments_header.getTag().equals("Inner")) {
            super.onBackPressed();
        }
    }

    private void getHomeallData() {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Home> getcall = apiInterface.getHomeallData(token, u_id);
            getcall.enqueue(new Callback<Home>() {
                @Override
                public void onResponse(Call<Home> call, final Response<Home> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();

                        if (status == 200) {
                            homeTopEventSliders = new ArrayList<>();
                            homeVenueData = new ArrayList<>();
                            upcomingEventData = new ArrayList<>();
                            homeUpcomingEvents = new ArrayList<>();
                            homeTopEventSliders = response.body().getTopEvents();
                            homeVenueData = response.body().getVenuesData();
                            upcomingEventData = response.body().getJustForYou();
                            homeUpcomingEvents = response.body().getUpcomingEvents();

                            if (homeTopEventSliders.size() == 0 && homeVenueData.size() == 0 &&
                                    upcomingEventData.size() == 0 && homeUpcomingEvents.size() == 0) {
                                rl_event_venue_slider.setVisibility(View.GONE);
                                rl_no_all_data_found.setVisibility(View.VISIBLE);
                            } else {
                                rl_event_venue_slider.setVisibility(View.VISIBLE);
                                rl_no_all_data_found.setVisibility(View.GONE);
                            }

                            if (homeTopEventSliders.size() == 0) {
                                rl_viewPager.setVisibility(View.GONE);
                            } else {
                                rl_viewPager.setVisibility(View.VISIBLE);
                                adapter = new AdapterMainviewPager(homeTopEventSliders, context, rl_fragments_header, getSupportFragmentManager());
                                clickableViewPager.setAdapter(adapter);
                                indicator.setViewPager(clickableViewPager);
                            }


                            DividerItemDecorator dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(context, R.drawable.divider), RecyclerView.HORIZONTAL);


                            if (homeVenueData.size() == 0) {
                                rv_listof_venue.setVisibility(View.GONE);
                                tv_no_venue.setVisibility(View.VISIBLE);
                                rl_venue.setVisibility(View.GONE);
                            } else {
                                tv_no_venue.setVisibility(View.GONE);
                                rl_venue.setVisibility(View.VISIBLE);
                                rv_listof_venue.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_listof_venue.setLayoutManager(layoutManager3);
                                homeVenueAdapter = new HomeVenueAdapter(context, homeVenueData, getSupportFragmentManager(), rl_fragments_header);
                                rv_listof_venue.setAdapter(homeVenueAdapter);
                            }


                            if (upcomingEventData.size() == 0) {
                                rv_just_for_you.setVisibility(View.GONE);
                                tv_no_just_for_you.setVisibility(View.VISIBLE);
                                rl_just_for_you.setVisibility(View.GONE);
                            } else {
                                rv_just_for_you.setVisibility(View.VISIBLE);
                                tv_no_just_for_you.setVisibility(View.GONE);
                                rl_just_for_you.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_just_for_you.setLayoutManager(layoutManager1);
                                adapterJustForYou = new HomeAdapterJustForYou(context, upcomingEventData, getSupportFragmentManager(), rl_fragments_header);
                                rv_just_for_you.setAdapter(adapterJustForYou);
                            }


                            if (homeUpcomingEvents.size() == 0) {
                                Log.d("mytag", "there is no upcoming events-------------" + homeUpcomingEvents.size());
                                rv_upcoming_events.setVisibility(View.GONE);
                                tv_no_upcoming_events.setVisibility(View.VISIBLE);
                                rl_upcoming_events.setVisibility(View.GONE);
                            } else {
                                rv_upcoming_events.setVisibility(View.VISIBLE);
                                tv_no_upcoming_events.setVisibility(View.GONE);
                                rl_upcoming_events.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_upcoming_events.setLayoutManager(layoutManager2);
                                homeUpcomingEventsAdapter = new HomeUpcomingEventsAdapter(context, homeUpcomingEvents, getSupportFragmentManager(), rl_fragments_header, true);
                                rv_upcoming_events.setAdapter(homeUpcomingEventsAdapter);
                            }

                            tv_viewall_upcoming_events.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fromHome = true;
                                    ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                                    pushFragment(new UpcomingEventsFragment(context, rl_fragments_header,
                                            true, getSupportFragmentManager(), homeUpcomingEvents), "more", false);
                                    rl_main_screen.setVisibility(View.GONE);
                                    rl_fragment.setVisibility(View.VISIBLE);
                                }
                            });

                            tv_viewall_just_for_you.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fromHome = true;
                                    ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                                    pushFragment(new JustForYouFragment(context, rl_fragments_header, true, getSupportFragmentManager(), upcomingEventData), "more", false);
                                    rl_main_screen.setVisibility(View.GONE);
                                    rl_fragment.setVisibility(View.VISIBLE);
                                }
                            });

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Home> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getHomeData(String state_name) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();

            String token = Prefs.getPrefInstance().getValue(context, Const.ACCESS_TOKEN, "");
            String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Home> getcall = apiInterface.getHomeData(state_name, token, u_id);
            getcall.enqueue(new Callback<Home>() {
                @Override
                public void onResponse(Call<Home> call, final Response<Home> response) {
                    if (response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();

                        if (status == 200) {
                            homeTopEventSliders = new ArrayList<>();
                            homeVenueData = new ArrayList<>();
                            upcomingEventData = new ArrayList<>();
                            homeUpcomingEvents = new ArrayList<>();
                            homeTopEventSliders = response.body().getTopEvents();
                            homeVenueData = response.body().getVenuesData();
                            upcomingEventData = response.body().getJustForYou();
                            homeUpcomingEvents = response.body().getUpcomingEvents();

                            if (homeTopEventSliders.size() == 0 && homeVenueData.size() == 0 &&
                                    upcomingEventData.size() == 0 && homeUpcomingEvents.size() == 0) {
                                rl_event_venue_slider.setVisibility(View.GONE);
                                rl_no_all_data_found.setVisibility(View.VISIBLE);
                            } else {
                                rl_event_venue_slider.setVisibility(View.VISIBLE);
                                rl_no_all_data_found.setVisibility(View.GONE);
                            }

                            if (homeTopEventSliders.size() == 0) {
                                rl_viewPager.setVisibility(View.GONE);
                            } else {
                                rl_viewPager.setVisibility(View.VISIBLE);
                                adapter = new AdapterMainviewPager(homeTopEventSliders, context, rl_fragments_header, getSupportFragmentManager());
                                clickableViewPager.setAdapter(adapter);
                                indicator.setViewPager(clickableViewPager);
                            }
                            DividerItemDecorator dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(context, R.drawable.divider), RecyclerView.HORIZONTAL);

                            if (homeVenueData.size() == 0) {
                                rv_listof_venue.setVisibility(View.GONE);
                                tv_no_venue.setVisibility(View.VISIBLE);
                                rl_venue.setVisibility(View.GONE);
                            } else {
                                tv_no_venue.setVisibility(View.GONE);
                                rl_venue.setVisibility(View.VISIBLE);
                                rv_listof_venue.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_listof_venue.setLayoutManager(layoutManager3);
                                homeVenueAdapter = new HomeVenueAdapter(context, homeVenueData, getSupportFragmentManager(), rl_fragments_header);
                                rv_listof_venue.setAdapter(homeVenueAdapter);
                            }
                            if (upcomingEventData.size() == 0) {
                                rv_just_for_you.setVisibility(View.GONE);
                                tv_no_just_for_you.setVisibility(View.VISIBLE);
                                rl_just_for_you.setVisibility(View.GONE);
                            } else {
                                rv_just_for_you.setVisibility(View.VISIBLE);
                                tv_no_just_for_you.setVisibility(View.GONE);
                                rl_just_for_you.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_just_for_you.setLayoutManager(layoutManager1);
                                adapterJustForYou = new HomeAdapterJustForYou(context, upcomingEventData, getSupportFragmentManager(), rl_fragments_header);
                                rv_just_for_you.setAdapter(adapterJustForYou);
                            }
                            if (homeUpcomingEvents.size() == 0) {
                                Log.d("mytag", "there is no upcoming events-------------" + homeUpcomingEvents.size());
                                rv_upcoming_events.setVisibility(View.GONE);
                                tv_no_upcoming_events.setVisibility(View.VISIBLE);
                                rl_upcoming_events.setVisibility(View.GONE);
                            } else {
                                rv_upcoming_events.setVisibility(View.VISIBLE);
                                tv_no_upcoming_events.setVisibility(View.GONE);
                                rl_upcoming_events.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                rv_upcoming_events.setLayoutManager(layoutManager2);
                                homeUpcomingEventsAdapter = new HomeUpcomingEventsAdapter(context, homeUpcomingEvents, getSupportFragmentManager(), rl_fragments_header, true);
                                rv_upcoming_events.setAdapter(homeUpcomingEventsAdapter);
                            }

                            tv_viewall_upcoming_events.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fromHome = true;
                                    ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                                    pushFragment(new UpcomingEventsFragment(context, rl_fragments_header, true, getSupportFragmentManager(), homeUpcomingEvents), "more", false);
                                    rl_main_screen.setVisibility(View.GONE);
                                    rl_fragment.setVisibility(View.VISIBLE);
                                }
                            });

                            tv_viewall_just_for_you.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fromHome = true;
                                    ll_bottom_menu_buttons.setVisibility(View.VISIBLE);
                                    pushFragment(new JustForYouFragment(context, rl_fragments_header, true, getSupportFragmentManager(), upcomingEventData), "more", false);
                                    rl_main_screen.setVisibility(View.GONE);
                                    rl_fragment.setVisibility(View.VISIBLE);
                                }
                            });

                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Home> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public static void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                int currentItem = clickableViewPager.getCurrentItem() + 1;
                                int totalItem = adapter.getCount();
                                if (currentItem == totalItem) {
                                    clickableViewPager.setCurrentItem(0);
                                } else {
                                    clickableViewPager.setCurrentItem(currentItem);
                                }

                            }
                        });
                    }
                }
            }, 0, 3000);
        }
    }

    public static void runOnUIThread(Runnable runnable) {

        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            Gems.applicationHandler.post(runnable);
        } else {
            Gems.applicationHandler.postDelayed(runnable, delay);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE)
                && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
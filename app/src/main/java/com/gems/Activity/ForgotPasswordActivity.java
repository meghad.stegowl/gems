package com.gems.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;

import com.gems.Helper.CheckNetwork;
import com.gems.Helper.Const;
import com.gems.Helper.Prefs;
import com.gems.Model.CommonResponse;
import com.gems.R;
import com.gems.Rest.ApiClient;
import com.gems.Rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener  {

    private EditText edt_email;
    private String email;
    private Button btn_submit;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    String emailptrn = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    AppCompatDialog mProgressDialog;
    Context context;
    LinearLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = ForgotPasswordActivity.this;
        initViews();
        listners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initViews() {
        edt_email = findViewById(R.id.edt_email);
        btn_submit = findViewById(R.id.btn_submit);
        btn_back = findViewById(R.id.btn_back);
    }

    private void listners() {

        edt_email.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                validateData();
                break;

            case R.id.btn_back:
                Intent registerIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                startActivity(registerIntent);
                break;

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent(ForgotPasswordActivity.this,LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.exit_anim,R.anim.entry_anim);
        finish();

    }

    private void validateData() {

        email = edt_email.getText().toString().trim();

        if (email.trim().length() != 0) {
            if (email.trim().matches(emailPattern) || email.trim().matches(emailptrn)) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    forgotPassword(email);
                } else {
                    Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            } else {
                edt_email.setError("Please Enter Valid E-mail");
                edt_email.requestFocus();
            }
        } else {
            edt_email.setError("Please Enter E-mail");
            edt_email.requestFocus();
        }
    }

    private void forgotPassword(String email) {
        if (CheckNetwork.isInternetAvailable(context)) {
            mProgressDialog = new AppCompatDialog(context);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progress_dialog);
            mProgressDialog.show();
            JSONObject jsonObject = new JSONObject();
            String gcm_id = Prefs.getPrefInstance().getValue(context, Const.FCM, "");

            try {
                jsonObject.put("email", email);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonObject.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonString);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommonResponse> getcall = apiInterface.getForgotPassword(requestBody);
            getcall.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> getcall, Response<CommonResponse> response) {
                    if (response != null && response.isSuccessful()) {
                        mProgressDialog.dismiss();
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        if (status == 200) {
                            clearAllEdditText();

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setMessage(msg);
                            builder1.setCancelable(true);
                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent loginIntent = new Intent(context, LoginActivity.class);
                                            startActivity(loginIntent);
                                            finish();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else if (status == 422) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (status == 404) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else if (status == 401) {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "Response not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> getcall, Throwable t) {
                    Toast.makeText(context, "Response Failure", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearAllEdditText() {
        edt_email.requestFocus();
        edt_email.getText().clear();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        super.onDestroy();
    }

}

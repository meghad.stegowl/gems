package com.gems.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import com.gems.R;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class PrivacyPolicyActivity extends AppCompatActivity {

    WebView webView;
    LinearLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        init();
        listener();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    private void init() {
        webView = findViewById(R.id.webView);
        btn_back = findViewById(R.id.btn_back);
    }

    private void listener() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PrivacyPolicyActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
